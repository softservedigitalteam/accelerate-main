
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsGroupPhases
/// </summary>
public class clsGroupPhases
{
    #region MEMBER VARIABLES

        private int m_iGroupPhaseID;
        private String m_strTitle;
        private int m_iGroupID;
        private int m_iPhaseID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iGroupPhaseID
        {
            get
            {
                return m_iGroupPhaseID;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public int iGroupID
        {
            get
            {
                return m_iGroupID;
            }
            set
            {
                m_iGroupID = value;
            }
        }

        public int iPhaseID
        {
            get
            {
                return m_iPhaseID;
            }
            set
            {
                m_iPhaseID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsGroupPhases()
    {
        m_iGroupPhaseID = 0;
    }

    public clsGroupPhases(int iGroupPhaseID)
    {
        m_iGroupPhaseID = iGroupPhaseID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iGroupPhaseID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@iGroupID", m_iGroupID),
                        new SqlParameter("@iPhaseID", m_iPhaseID)                  
                  };

                  //### Add
                  m_iGroupPhaseID = (int)clsDataAccess.ExecuteScalar("spGroupPhasesInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iGroupPhaseID", m_iGroupPhaseID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@iGroupID", m_iGroupID),
                         new SqlParameter("@iPhaseID", m_iPhaseID)
                    };
                    //### Update
                    clsDataAccess.Execute("spGroupPhasesUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iGroupPhaseID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iGroupPhaseID", iGroupPhaseID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spGroupPhasesDelete", sqlParameter);
        }

        public static DataTable GetGroupPhasesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spGroupPhasesList", EmptySqlParameter);
        }
        public static DataTable GetGroupPhasesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvGroupPhasesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvGroupPhasesList = clsDataAccess.GetDataView("spGroupPhasesList", EmptySqlParameter);
            dvGroupPhasesList.RowFilter = strFilterExpression;
            dvGroupPhasesList.Sort = strSortExpression;

            return dvGroupPhasesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iGroupPhaseID", m_iGroupPhaseID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spGroupPhasesGetRecord", sqlParameter);

                m_strTitle = drRecord["strTitle"].ToString();
                m_iGroupID = Convert.ToInt32(drRecord["iGroupID"]);
                m_iPhaseID = Convert.ToInt32(drRecord["iPhaseID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}