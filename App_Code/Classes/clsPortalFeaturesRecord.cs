
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsPortalFeatures
/// </summary>
public class clsPortalFeatures
{
    #region MEMBER VARIABLES

        private int m_iPortalFeatureID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iGroupID;
        private String m_strTitle;
        private String m_strDescription;
        private String m_strYouTubeLink;
        private String m_strPathToImages;
        private String m_strMasterImage;
        private String m_strPathToDocument;
        private String m_strMasterDocument;
        private bool m_bIsNormal;
        private bool m_bIsChecklist;
        private bool m_bIsMultiple;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iPortalFeatureID
        {
            get
            {
                return m_iPortalFeatureID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iGroupID
        {
            get
            {
                return m_iGroupID;
            }
            set
            {
                m_iGroupID = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strDescription
        {
            get
            {
                return m_strDescription;
            }
            set
            {
                m_strDescription = value;
            }
        }

        public String strYouTubeLink
        {
            get
            {
                return m_strYouTubeLink;
            }
            set
            {
                m_strYouTubeLink = value;
            }
        }

        public String strPathToImages
        {
            get
            {
                return m_strPathToImages;
            }
            set
            {
                m_strPathToImages = value;
            }
        }

        public String strMasterImage
        {
            get
            {
                return m_strMasterImage;
            }
            set
            {
                m_strMasterImage = value;
            }
        }

        public String strPathToDocument
        {
            get
            {
                return m_strPathToDocument;
            }
            set
            {
                m_strPathToDocument = value;
            }
        }

        public String strMasterDocument
        {

            get
            {
                return m_strMasterDocument;
            }
            set
            {
                m_strMasterDocument = value;
            }
        }

        public bool bIsNormal
        {
            get
            {
                return m_bIsNormal;
            }
            set
            {
                m_bIsNormal = value;
            }
        }

        public bool bIsChecklist
        {
            get
            {
                return m_bIsChecklist;
            }
            set
            {
                m_bIsChecklist = value;
            }
        }

        public bool bIsMultiple
        {
            get
            {
                return m_bIsMultiple;
            }
            set
            {
                m_bIsMultiple = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsPortalFeatures()
    {
        m_iPortalFeatureID = 0;
    }

    public clsPortalFeatures(int iPortalFeatureID)
    {
        m_iPortalFeatureID = iPortalFeatureID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iPortalFeatureID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iGroupID", m_iGroupID),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@strYouTubeLink", m_strYouTubeLink),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage) ,
                        new SqlParameter("@strPathToDocument", m_strPathToDocument),
                        new SqlParameter("@strMasterDocument", m_strMasterDocument),
                        new SqlParameter("@bIsNormal", m_bIsNormal),
                        new SqlParameter("@bIsChecklist", m_bIsChecklist),
                        new SqlParameter("@bIsMultiple", m_bIsMultiple)
                  };

                  //### Add
                  m_iPortalFeatureID = (int)clsDataAccess.ExecuteScalar("spPortalFeaturesInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iPortalFeatureID", m_iPortalFeatureID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iGroupID", m_iGroupID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@strYouTubeLink", m_strYouTubeLink),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage),
                         new SqlParameter("@strPathToDocument", m_strPathToDocument),
                         new SqlParameter("@strMasterDocument", m_strMasterDocument),
                         new SqlParameter("@bIsNormal", m_bIsNormal),
                         new SqlParameter("@bIsChecklist", m_bIsChecklist),
                         new SqlParameter("@bIsMultiple", m_bIsMultiple)
                    };
                    //### Update
                    clsDataAccess.Execute("spPortalFeaturesUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iPortalFeatureID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iPortalFeatureID", iPortalFeatureID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spPortalFeaturesDelete", sqlParameter);
        }

        public static DataTable GetPortalFeaturesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spPortalFeaturesList", EmptySqlParameter);
        }

        public static DataTable GetPortalFeaturesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvPortalFeaturesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvPortalFeaturesList = clsDataAccess.GetDataView("spPortalFeaturesList", EmptySqlParameter);
            dvPortalFeaturesList.RowFilter = strFilterExpression;
            dvPortalFeaturesList.Sort = strSortExpression;

            return dvPortalFeaturesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iPortalFeatureID", m_iPortalFeatureID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spPortalFeaturesGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iGroupID = Convert.ToInt32(drRecord["iGroupID"]);
                m_strTitle = drRecord["strTitle"].ToString();
                m_strDescription = drRecord["strDescription"].ToString();
                m_strYouTubeLink = drRecord["strYouTubeLink"].ToString();
                m_strPathToImages = drRecord["strPathToImages"].ToString();
                m_strMasterImage = drRecord["strMasterImage"].ToString();
                m_strPathToDocument = drRecord["strPathToDocument"].ToString();
                m_strMasterDocument = drRecord["strMasterDocument"].ToString();
                m_bIsNormal = Convert.ToBoolean(drRecord["bIsNormal"]);
                m_bIsChecklist = Convert.ToBoolean(drRecord["bIsChecklist"]);
                m_bIsMultiple = Convert.ToBoolean(drRecord["bIsMultiple"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}