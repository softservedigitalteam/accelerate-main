
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsPhases
/// </summary>
public class clsPhases
{
    #region MEMBER VARIABLES

        private int m_iPhaseID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strTitle;
        private String m_strTag;
        private DateTime m_dtStartDate;
        private DateTime m_dtEndDate;
        private String m_strPathToImages;
        private String m_strMasterImage;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iPhaseID
        {
            get
            {
                return m_iPhaseID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strTag
        {
            get
            {
                return m_strTag;
            }
            set
            {
                m_strTag = value;
            }
        }

        public DateTime dtStartDate
        {
            get
            {
                return m_dtStartDate;
            }
            set
            {
                m_dtStartDate = value;
            }
        }

        public DateTime dtEndDate
        {
            get
            {
                return m_dtEndDate;
            }
            set
            {
                m_dtEndDate = value;
            }
        }

        public String strPathToImages
        {
            get
            {
                return m_strPathToImages;
            }
            set
            {
                m_strPathToImages = value;
            }
        }

        public String strMasterImage
        {
            get
            {
                return m_strMasterImage;
            }
            set
            {
                m_strMasterImage = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsPhases()
    {
        m_iPhaseID = 0;
    }

    public clsPhases(int iPhaseID)
    {
        m_iPhaseID = iPhaseID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iPhaseID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strTag", m_strTag),
                        new SqlParameter("@dtStartDate", m_dtStartDate),
                        new SqlParameter("@dtEndDate", m_dtEndDate),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage)                  
                  };

                  //### Add
                  m_iPhaseID = (int)clsDataAccess.ExecuteScalar("spPhasesInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iPhaseID", m_iPhaseID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strTag", m_strTag),
                         new SqlParameter("@dtStartDate", m_dtStartDate),
                         new SqlParameter("@dtEndDate", m_dtEndDate),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage)
                    };
                    //### Update
                    clsDataAccess.Execute("spPhasesUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iPhaseID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iPhaseID", iPhaseID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spPhasesDelete", sqlParameter);
        }

        public static DataTable GetPhasesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spPhasesList", EmptySqlParameter);
        }
        public static DataTable GetPhasesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvPhasesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvPhasesList = clsDataAccess.GetDataView("spPhasesList", EmptySqlParameter);
            dvPhasesList.RowFilter = strFilterExpression;
            dvPhasesList.Sort = strSortExpression;

            return dvPhasesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                //### Populate
                SqlParameter[] sqlParameter = new SqlParameter[] 
                {
                    new SqlParameter("@iPhaseID", m_iPhaseID)
                };
                DataRow drRecord = clsDataAccess.GetRecord("spPhasesGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

                if (drRecord["iEditedBy"] != DBNull.Value)
                    m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strTitle = drRecord["strTitle"].ToString();
                m_dtStartDate = Convert.ToDateTime(drRecord["dtStartDate"]);
                m_dtEndDate = Convert.ToDateTime(drRecord["dtEndDate"]);
                m_strTag = drRecord["strTag"].ToString();
                m_strPathToImages = drRecord["strPathToImages"].ToString();
                m_strMasterImage = drRecord["strMasterImage"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}