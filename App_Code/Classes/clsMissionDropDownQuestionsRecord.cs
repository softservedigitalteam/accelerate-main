
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMissionQuestions
/// </summary>
public class clsMissionDropDownQuestions
{
    #region MEMBER VARIABLES

    private int m_iMissionDropDownQuestionID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private int m_iMissionID;
    private String m_strQuestion;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iMissionDropDownQuestionID
    {
        get
        {
            return m_iMissionDropDownQuestionID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public int iMissionID
    {
        get
        {
            return m_iMissionID;
        }
        set
        {
            m_iMissionID = value;
        }
    }

    public String strQuestion
    {
        get
        {
            return m_strQuestion;
        }
        set
        {
            m_strQuestion = value;
        }

    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsMissionDropDownQuestions()
    {
        m_iMissionDropDownQuestionID = 0;
    }

    public clsMissionDropDownQuestions(int iMissionDropDownQuestionID)
    {
        m_iMissionDropDownQuestionID = iMissionDropDownQuestionID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iMissionDropDownQuestionID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iMissionID", m_iMissionID),
                        new SqlParameter("@strQuestion", m_strQuestion)
                  };

                //### Add
                m_iMissionDropDownQuestionID = (int)clsDataAccess.ExecuteScalar("spMissionDropDownQuestionsInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMissionDropDownQuestionID", m_iMissionDropDownQuestionID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionID", m_iMissionID),
                         new SqlParameter("@strQuestion", m_strQuestion)
                    };
                //### Update
                clsDataAccess.Execute("spMissionDropDownQuestionsUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iMissionDropDownQuestionID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMissionDropDownQuestionID", iMissionDropDownQuestionID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMissionDropDownQuestionsDelete", sqlParameter);
    }

    public static DataTable GetMissionQuestionsList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spMissionDropDownQuestionsList", EmptySqlParameter);
    }
    public static DataTable GetMissionQuestionsList(string strFilterExpression, string strSortExpression)
    {
        DataView dvMissionDropDownQuestionsList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvMissionDropDownQuestionsList = clsDataAccess.GetDataView("spMissionDropDownQuestionsList", EmptySqlParameter);
        dvMissionDropDownQuestionsList.RowFilter = strFilterExpression;
        dvMissionDropDownQuestionsList.Sort = strSortExpression;

        return dvMissionDropDownQuestionsList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iMissionDropDownQuestionID", m_iMissionDropDownQuestionID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spMissionDropDownQuestionsGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
            m_strQuestion = drRecord["strQuestion"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}