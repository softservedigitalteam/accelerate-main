using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCompanies
/// </summary>
public class clsCompanies
{
    #region MEMBER VARIABLES

    private int m_iCompanyID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private String m_strTitle;
    private String m_strDescription;
    private String m_strTagLine;
    private String m_strPrimaryColor;
    private String m_strSecondaryColor;
    private String m_strTertiaryColor;
    private String m_strPrimaryContact;
    private String m_strPrimaryEmail;
    private String m_strPrimaryTelephone;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iCompanyID
    {
        get
        {
            return m_iCompanyID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public String strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public String strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public String strTagLine
    {
        get
        {
            return m_strTagLine;
        }
        set
        {
            m_strTagLine = value;
        }
    }

    public String strPrimaryColor
    {
        get
        {
            return m_strPrimaryColor;
        }
        set
        {
            m_strPrimaryColor = value;
        }

    }
    public String strSecondaryColor
    {
        get
        {
            return m_strSecondaryColor;
        }
        set
        {
            m_strSecondaryColor = value;
        }
    }

    public String strTertiaryColor
    {
        get
        {
            return m_strTertiaryColor;
        }
        set
        {
            m_strTertiaryColor = value;
        }
    }


    public String strPrimaryContact
    {
        get
        {
            return m_strPrimaryContact;
        }
        set
        {
            m_strPrimaryContact = value;
        }
    }

    public String strPrimaryEmail
    {
        get
        {
            return m_strPrimaryEmail;
        }
        set
        {
            m_strPrimaryEmail = value;
        }
    }

    public String strPrimaryTelephone
    {
        get
        {
            return m_strPrimaryTelephone;
        }
        set
        {
            m_strPrimaryTelephone = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    #endregion

    #region CONSTRUCTORS

    public clsCompanies()
    {
        m_iCompanyID = 0;
    }

    public clsCompanies(int iCompanyID)
    {
        m_iCompanyID = iCompanyID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iCompanyID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@strTagLine", m_strTagLine),
                        new SqlParameter("@strPrimaryColor", m_strPrimaryColor),
                        new SqlParameter("@strSecondaryColor", m_strSecondaryColor),
                        new SqlParameter("@strTertiaryColor", m_strTertiaryColor),
                        new SqlParameter("@strPrimaryContact", m_strPrimaryContact),
                        new SqlParameter("@strPrimaryEmail", m_strPrimaryEmail),
                        new SqlParameter("@strPrimaryTelephone", m_strPrimaryTelephone)
                  };

                //### Add
                m_iCompanyID = (int)clsDataAccess.ExecuteScalar("spCompaniesInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCompanyID", m_iCompanyID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@strTagLine", m_strTagLine),
                         new SqlParameter("@strPrimaryColor", m_strPrimaryColor),
                         new SqlParameter("@strSecondaryColor", m_strSecondaryColor),
                         new SqlParameter("@strTertiaryColor", m_strTertiaryColor),
                         new SqlParameter("@strPrimaryContact", m_strPrimaryContact),
                         new SqlParameter("@strPrimaryEmail", m_strPrimaryEmail),
                         new SqlParameter("@strPrimaryTelephone", m_strPrimaryTelephone)
                    };
                //### Update
                clsDataAccess.Execute("spCompaniesUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iCompanyID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCompanyID", iCompanyID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCompaniesDelete", sqlParameter);
    }

    public static DataTable GetCompaniesList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spCompaniesList", EmptySqlParameter);
    }

    public static DataTable GetCompaniesList(string strFilterExpression, string strSortExpression)
    {
        DataView dvCompaniesList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvCompaniesList = clsDataAccess.GetDataView("spCompaniesList", EmptySqlParameter);
        dvCompaniesList.RowFilter = strFilterExpression;
        dvCompaniesList.Sort = strSortExpression;

        return dvCompaniesList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
            {
                new SqlParameter("@iCompanyID", m_iCompanyID)
            };
            DataRow drRecord = clsDataAccess.GetRecord("spCompaniesGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_strTitle = drRecord["strTitle"].ToString();
            m_strDescription = drRecord["strDescription"].ToString();
            m_strTagLine = drRecord["strTagLine"].ToString();
            m_strPrimaryColor = drRecord["strPrimaryColor"].ToString();
            m_strSecondaryColor = drRecord["strSecondaryColor"].ToString();
            m_strTertiaryColor = drRecord["strTertiaryColor"].ToString();
            m_strPrimaryContact = drRecord["strPrimaryContact"].ToString();
            m_strPrimaryEmail = drRecord["strPrimaryEmail"].ToString();
            m_strPrimaryTelephone = drRecord["strPrimaryTelephone"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}