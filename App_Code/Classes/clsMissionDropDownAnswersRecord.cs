
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMissionAnswers
/// </summary>
public class clsMissionDropDownAnswers
{
    #region MEMBER VARIABLES

        private int m_iMissionDropDownAnswerID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iMissionDropDownQuestionID;
        private String m_strAnswer;
        private bool m_bIsCorrectAnswer;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iMissionDropDownAnswerID
        {
            get
            {
                return m_iMissionDropDownAnswerID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iMissionDropDownQuestionID
        {
            get
            {
                return m_iMissionDropDownQuestionID;
            }
            set
            {
                m_iMissionDropDownQuestionID = value;
            }
        }

        public String strAnswer
        {
            get
            {
                return m_strAnswer;
            }
            set
            {
                m_strAnswer = value;
            }
        }

        public bool bIsCorrectAnswer
        {
            get
            {
                return m_bIsCorrectAnswer;
            }
            set
            {
                m_bIsCorrectAnswer = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsMissionDropDownAnswers()
    {
        m_iMissionDropDownAnswerID = 0;
    }

    public clsMissionDropDownAnswers(int iMissionDropDownAnswerID)
    {
        m_iMissionDropDownAnswerID = iMissionDropDownAnswerID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iMissionDropDownAnswerID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iMissionDropDownQuestionID", m_iMissionDropDownQuestionID),
                        new SqlParameter("@strAnswer", m_strAnswer),
                        new SqlParameter("@bIsCorrectAnswer", m_bIsCorrectAnswer)                  
                  };

                  //### Add
                  m_iMissionDropDownAnswerID = (int)clsDataAccess.ExecuteScalar("spMissionDropDownAnswersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMissionDropDownAnswerID", m_iMissionDropDownAnswerID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionDropDownQuestionID", m_iMissionDropDownQuestionID),
                         new SqlParameter("@strAnswer", m_strAnswer),
                         new SqlParameter("@bIsCorrectAnswer", m_bIsCorrectAnswer)
                    };
                    //### Update
                    clsDataAccess.Execute("spMissionDropDownAnswersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iMissionDropDownAnswerID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMissionDropDownAnswerID", iMissionDropDownAnswerID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMissionDropDownAnswersDelete", sqlParameter);
        }

        public static DataTable GetMissionAnswersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spMissionDropDownAnswersList", EmptySqlParameter);
        }
        public static DataTable GetMissionAnswersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvMissionDropDownAnswersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvMissionDropDownAnswersList = clsDataAccess.GetDataView("spMissionDropDownAnswersList", EmptySqlParameter);
            dvMissionDropDownAnswersList.RowFilter = strFilterExpression;
            dvMissionDropDownAnswersList.Sort = strSortExpression;

            return dvMissionDropDownAnswersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iMissionDropDownAnswerID", m_iMissionDropDownAnswerID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spMissionDropDownAnswersGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iMissionDropDownQuestionID = Convert.ToInt32(drRecord["iMissionDropDownQuestionID"]);
                m_strAnswer = drRecord["strAnswer"].ToString();
                m_bIsCorrectAnswer = Convert.ToBoolean(drRecord["bIsCorrectAnswer"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}