
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsTeamShipAnswers
/// </summary>
public class clsUserDropDownAnswers
{
    #region MEMBER VARIABLES

        private int m_iUserDropDownAnswerID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iMissionID;
        private int m_iMissionDropDownAnswerID;
        private int m_iMissionDropDownQuestionID;
        private String m_strAnswer;
        private String m_strTitleAnswer;
        private bool m_bIsDisplayed;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iUserDropDownAnswerID
        {
            get
            {
                return m_iUserDropDownAnswerID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public int iMissionDropDownAnswerID
        {
            get
            {
                return m_iMissionDropDownAnswerID;
            }
            set
            {
                m_iMissionDropDownAnswerID = value;
            }
        }

        public int iMissionDropDownQuestionID
        {
            get
            {
                return m_iMissionDropDownQuestionID;
            }
            set
            {
                m_iMissionDropDownQuestionID = value;
            }
        }

        public String strAnswer
        {
            get
            {
                return m_strAnswer;
            }
            set
            {
                m_strAnswer = value;
            }
        }


        public bool bIsDisplayed
        {
            get
            {
                return m_bIsDisplayed;
            }
            set
            {
                m_bIsDisplayed = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsUserDropDownAnswers()
    {
        m_iUserDropDownAnswerID = 0;
    }

    public clsUserDropDownAnswers(int iUserDropDownAnswerID)
    {
        m_iUserDropDownAnswerID = iUserDropDownAnswerID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iUserDropDownAnswerID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iMissionID", m_iMissionID),
                        new SqlParameter("@iMissionDropDownQuestionID", m_iMissionDropDownQuestionID),
                        new SqlParameter("@iMissionDropDownAnswerID", m_iMissionDropDownAnswerID),
                        new SqlParameter("@strAnswer", m_strAnswer),
                        new SqlParameter("@bIsDisplayed", m_bIsDisplayed)                  
                  };

                  //### Add
                    m_iUserDropDownAnswerID = (int)clsDataAccess.ExecuteScalar("spUserDropDownAnswersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iUserDropDownAnswerID", m_iUserDropDownAnswerID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionID", m_iMissionID),
                         new SqlParameter("@iMissionDropDownQuestionID", m_iMissionDropDownQuestionID),
                         new SqlParameter("@iMissionDropDownAnswerID", m_iMissionDropDownAnswerID),
                         new SqlParameter("@strAnswer", m_strAnswer),
                         new SqlParameter("@bIsDisplayed", m_bIsDisplayed)
                    };
                    //### Update
                    clsDataAccess.Execute("spUserDropDownAnswersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iUserDropDownAnswerID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iUserDropDownAnswerID", iUserDropDownAnswerID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spUserDropDownAnswersDelete", sqlParameter);
        }

        public static DataTable GetDropDownAnswersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spUserDropDownAnswersList", EmptySqlParameter);
        }
        public static DataTable GetDropDownAnswersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvDropDownAnswersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvDropDownAnswersList = clsDataAccess.GetDataView("spUserDropDownAnswersList", EmptySqlParameter);
            dvDropDownAnswersList.RowFilter = strFilterExpression;
            dvDropDownAnswersList.Sort = strSortExpression;

            return dvDropDownAnswersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iUserDropDownAnswerID", m_iUserDropDownAnswerID)
                        };
                    DataRow drRecord = clsDataAccess.GetRecord("spUserDropDownAnswersGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);
             m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_iMissionDropDownAnswerID = Convert.ToInt32(drRecord["iMissionDropDownAnswerID"]);
                m_iMissionDropDownQuestionID = Convert.ToInt32(drRecord["iMissionDropDownQuestionID"]);
                m_strAnswer = drRecord["strAnswer"].ToString();
                m_bIsDisplayed = Convert.ToBoolean(drRecord["bIsDisplayed"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}