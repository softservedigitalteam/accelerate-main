using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsForums
/// </summary>
public class clsForums
{
    #region MEMBER VARIABLES

        private int m_iForumID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private string m_strTitle;
        private string m_strDescription;
        private int m_iGroupID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iForumID
        {
            get
            {
                return m_iForumID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public string strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public string strDescription
        {
            get
            {
                return m_strDescription;
            }
            set
            {
                m_strDescription = value;
            }
        }

        public int iGroupID
        {
            get
            {
                return m_iGroupID;
            }
            set
            {
                m_iGroupID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsForums()
    {
        m_iForumID = 0;
    }

    public clsForums(int iForumID)
    {
        m_iForumID = iForumID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iForumID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@iGroupID", m_iGroupID)                  
                  };

                  //### Add
                  m_iForumID = (int)clsDataAccess.ExecuteScalar("spForumsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iForumID", m_iForumID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@iGroupID", m_iGroupID)
                    };
                    //### Update
                    clsDataAccess.Execute("spForumsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iForumID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iForumID", iForumID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spForumsDelete", sqlParameter);
        }

        public static DataTable GetForumsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spForumsList", EmptySqlParameter);
        }

        public static DataTable GetForumsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvForumsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvForumsList = clsDataAccess.GetDataView("spForumsList", EmptySqlParameter);
            dvForumsList.RowFilter = strFilterExpression;
            dvForumsList.Sort = strSortExpression;

            return dvForumsList.ToTable();
        }


    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
            {
                //### Populate
                SqlParameter[] sqlParameter = new SqlParameter[] 
                {
                    new SqlParameter("@iForumID", m_iForumID)
                };
                DataRow drRecord = clsDataAccess.GetRecord("spForumsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                    m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

                if (drRecord["iEditedBy"] != DBNull.Value)
                    m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strTitle = drRecord["strTitle"].ToString();
                m_strDescription = drRecord["strDescription"].ToString();
                m_iGroupID = Convert.ToInt32(drRecord["iGroupID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}