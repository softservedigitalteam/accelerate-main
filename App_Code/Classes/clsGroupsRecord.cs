
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsGroups
/// </summary>
public class clsGroups
{
    #region MEMBER VARIABLES

    private int m_iGroupID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private int m_iLevelID;
    private int m_iWelcomeMessageID;
    private int m_iFinalMessageID;
    private int m_iCompanyID;
    private String m_strTitle;
    private String m_strDescription;
    private int m_iCustomGroupRankID;
    private int m_iCustomGeneralColourID;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iGroupID
    {
        get
        {
            return m_iGroupID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public int iLevelID
    {
        get
        {
            return m_iLevelID;
        }
        set
        {
            m_iLevelID = value;
        }
    }

    public int iWelcomeMessageID
    {
        get
        {
            return m_iWelcomeMessageID;
        }
        set
        {
            m_iWelcomeMessageID = value;
        }
    }

    public int iFinalMessageID
    {
        get
        {
            return m_iFinalMessageID;
        }
        set
        {
            m_iFinalMessageID = value;
        }
    }

    public int iCompanyID
    {
        get
        {
            return m_iCompanyID;
        }
        set
        {
            m_iCompanyID = value;
        }
    }

    public String strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public String strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public int iCustomGroupRankID
    {
        get
        {
            return m_iCustomGroupRankID;
        }
        set
        {
            m_iCustomGroupRankID = value;
        }
    }

    public int iCustomGeneralColourID
    {
        get
        {
            return m_iCustomGeneralColourID;
        }
        set
        {
            m_iCustomGeneralColourID = value;
        }
    }
    

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsGroups()
    {
        m_iGroupID = 0;
    }

    public clsGroups(int iGroupID)
    {
        m_iGroupID = iGroupID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iGroupID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iLevelID", m_iLevelID),
                        new SqlParameter("@iWelcomeMessageID", m_iWelcomeMessageID),
                        new SqlParameter("@iFinalMessageID", m_iFinalMessageID),
                        new SqlParameter("@iCompanyID", m_iCompanyID),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@iCustomGroupRankID", m_iCustomGroupRankID),
                        new SqlParameter("@iCustomGeneralColourID", m_iCustomGeneralColourID)
                  };

                //### Add
                m_iGroupID = (int)clsDataAccess.ExecuteScalar("spGroupsInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iGroupID", m_iGroupID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iLevelID", m_iLevelID),
                         new SqlParameter("@iWelcomeMessageID", m_iWelcomeMessageID),
                         new SqlParameter("@iFinalMessageID", m_iFinalMessageID),
                         new SqlParameter("@iCompanyID", m_iCompanyID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@iCustomGroupRankID", m_iCustomGroupRankID),
                         new SqlParameter("@iCustomGeneralColourID", m_iCustomGeneralColourID)
                    };
                //### Update
                clsDataAccess.Execute("spGroupsUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iGroupID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iGroupID", iGroupID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spGroupsDelete", sqlParameter);
    }

    public static DataTable GetGroupsList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spGroupsList", EmptySqlParameter);
    }

    public static DataTable GetGroupsList(string strFilterExpression, string strSortExpression)
    {
        DataView dvGroupsList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvGroupsList = clsDataAccess.GetDataView("spGroupsList", EmptySqlParameter);
        dvGroupsList.RowFilter = strFilterExpression;
        dvGroupsList.Sort = strSortExpression;

        return dvGroupsList.ToTable();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="iGroupTypeID">1 for Products And 2 For Services</param>
    /// <returns>The first Group with a Product/Service Linked to it</returns>
    public static DataTable GetFirstGroup(int iGroupTypeID)
    {
        DataView dvGroupsFirst = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { 
            new SqlParameter("@iGroupTypeID",iGroupTypeID)
        };
        dvGroupsFirst = clsDataAccess.GetDataView("spGroupsgetFirst", EmptySqlParameter);
        return dvGroupsFirst.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iGroupID", m_iGroupID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spGroupsGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_iLevelID = Convert.ToInt32(drRecord["iLevelID"]);
            m_iWelcomeMessageID = Convert.ToInt32(drRecord["iWelcomeMessageID"]);
            m_iFinalMessageID = Convert.ToInt32(drRecord["iFinalMessageID"]);
            m_iCompanyID = Convert.ToInt32(drRecord["iCompanyID"]);
            m_strTitle = drRecord["strTitle"].ToString();
            m_strDescription = drRecord["strDescription"].ToString();
            m_iCustomGroupRankID = Convert.ToInt32(drRecord["iCustomGroupRankID"]);
            m_iCustomGeneralColourID = Convert.ToInt32(drRecord["iCustomGeneralColourID"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}