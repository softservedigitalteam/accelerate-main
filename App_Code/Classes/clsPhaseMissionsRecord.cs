
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsPhaseMissions
/// </summary>
public class clsPhaseMissions
{
    #region MEMBER VARIABLES

        private int m_iPhaseMissionID;
        private String m_strTitle;
        private int m_iPhaseID;
        private int m_iMissionID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iPhaseMissionID
        {
            get
            {
                return m_iPhaseMissionID;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public int iPhaseID
        {
            get
            {
                return m_iPhaseID;
            }
            set
            {
                m_iPhaseID = value;
            }
        }

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsPhaseMissions()
    {
        m_iPhaseMissionID = 0;
    }

    public clsPhaseMissions(int iPhaseMissionID)
    {
        m_iPhaseMissionID = iPhaseMissionID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iPhaseMissionID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@iPhaseID", m_iPhaseID),
                        new SqlParameter("@iMissionID", m_iMissionID)                  
                  };

                  //### Add
                  m_iPhaseMissionID = (int)clsDataAccess.ExecuteScalar("spPhaseMissionsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iPhaseMissionID", m_iPhaseMissionID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@iPhaseID", m_iPhaseID),
                         new SqlParameter("@iMissionID", m_iMissionID)
                    };
                    //### Update
                    clsDataAccess.Execute("spPhaseMissionsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iPhaseMissionID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iPhaseMissionID", iPhaseMissionID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spPhaseMissionsDelete", sqlParameter);
        }

        public static DataTable GetPhaseMissionsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spPhaseMissionsList", EmptySqlParameter);
        }
        public static DataTable GetPhaseMissionsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvPhaseMissionsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvPhaseMissionsList = clsDataAccess.GetDataView("spPhaseMissionsList", EmptySqlParameter);
            dvPhaseMissionsList.RowFilter = strFilterExpression;
            dvPhaseMissionsList.Sort = strSortExpression;

            return dvPhaseMissionsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iPhaseMissionID", m_iPhaseMissionID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spPhaseMissionsGetRecord", sqlParameter);

                m_strTitle = drRecord["strTitle"].ToString();
                m_iPhaseID = Convert.ToInt32(drRecord["iPhaseID"]);
                m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}