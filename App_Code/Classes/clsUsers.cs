﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsUsersRecord
/// </summary>
public class clsUsers
{
    #region MEMBER VARIABLES

    private int m_iUserID;

    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;

    private String m_strFirstName;
    private String m_strSurname;
    private String m_strPassword;
    private String m_strEmailAddress;

    private bool m_bIsDeleted;
    
    #endregion

    #region PROPERTIES

    public int iUserID
    {
        get
        {
            return m_iUserID;
        }
    }
    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }
    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }
    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }
    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }
    public String strFirstName
    {
        get
        {
            return m_strFirstName;
        }
        set
        {
            m_strFirstName = value;
        }
    }
    public String strSurname
    {
        get
        {
            return m_strSurname;
        }
        set
        {
            m_strSurname = value;
        }
    }
    public String strPassword
    {
        get
        {
            return m_strPassword;
        }
        set
        {
            m_strPassword = value;
        }
    }
    public String strEmailAddress
    {
        get
        {
            return m_strEmailAddress;
        }
        set
        {
            m_strEmailAddress = value;
        }
    }
    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    #endregion

    #region CONSTRUCTORS

    public clsUsers()
    {
        m_iUserID = 0;
    }

    public clsUsers(int iUserID)
    {
        m_iUserID = iUserID;
        GetData();
    }

    public clsUsers(string strUsername, string strPassword)
    {
        //### Assign values to the parameter list for each corresponding column in the DB 
        SqlParameter[] sqlParametersInit = new SqlParameter[] 
                { 
                    new SqlParameter("@strEmail", strUsername), 
                    new SqlParameter("@strPassword", strPassword) 
                };

        DataRow drLoginRecord = clsDataAccess.GetRecord("spInitialiseUser", sqlParametersInit);

        if (drLoginRecord != null)
        {
            m_dtAdded = Convert.ToDateTime(drLoginRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drLoginRecord["iAddedBy"]);

            if (drLoginRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drLoginRecord["dtEdited"]);

            if (drLoginRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drLoginRecord["iEditedBy"]);

            m_strFirstName = drLoginRecord["strFirstName"].ToString();
            m_strSurname = drLoginRecord["strSurname"].ToString();
            m_strPassword = drLoginRecord["strPassword"].ToString();
            m_strEmailAddress = drLoginRecord["strEmailAddress"].ToString();

            m_bIsDeleted = Convert.ToBoolean(drLoginRecord["bIsDeleted"]);
        }
        else
            throw new Exception("Invalid User Login Credentials");
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iUserID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB 
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                { 
                    new SqlParameter("@dtAdded", m_dtAdded), 
                    new SqlParameter("@iAddedBy", m_iAddedBy), 
                    new SqlParameter("@strFirstName", m_strFirstName), 
                    new SqlParameter("@strSurname", m_strSurname), 
                    new SqlParameter("@strEmailAddress", m_strEmailAddress), 
                    new SqlParameter("@strPassword", m_strPassword) 
                };

                //### Add
                m_iUserID = (int)clsDataAccess.ExecuteScalar("spUsersInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                { 
                    new SqlParameter("@iUserID", m_iUserID),
                    new SqlParameter("@dtEdited", m_dtEdited), 
                    new SqlParameter("@iEditedBy", m_iEditedBy), 
                    new SqlParameter("@strFirstName", m_strFirstName), 
                    new SqlParameter("@strSurname", m_strSurname), 
                    new SqlParameter("@strEmailAddress", m_strEmailAddress), 
                    new SqlParameter("@strPassword", m_strPassword)
                };

                //### Update
                clsDataAccess.Execute("spUsersUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iUserID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[] 
                { 
                    new SqlParameter("@iUserID", iUserID)
                };

        //### Executes delete sp
        clsDataAccess.Execute("spUsersDelete", sqlParameter);
    }

    public static DataTable GetUsersList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
        return clsDataAccess.GetDataTable("spUsersList", EmptySqlParameter);
    }

    public static DataTable GetUsersList(string strFilterExpression, string strSortExpression)
    {
        DataView dvUsersList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };

        dvUsersList = clsDataAccess.GetDataView("spUsersList", EmptySqlParameter);

        dvUsersList.RowFilter = strFilterExpression;
        dvUsersList.Sort = strSortExpression;

        return dvUsersList.ToTable();
    }

    #endregion

    #region PROTECTED METHODS

    protected void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                { 
                    new SqlParameter("@iUserID", m_iUserID)
                };

            DataRow dtrRecord = clsDataAccess.GetRecord("spUsersGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(dtrRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(dtrRecord["iAddedBy"]);

            if (dtrRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(dtrRecord["dtEdited"]);

            if (dtrRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(dtrRecord["iEditedBy"]);

            m_strFirstName = dtrRecord["strFirstName"].ToString();
            m_strSurname = dtrRecord["strSurname"].ToString();
            m_strPassword = dtrRecord["strPassword"].ToString();
            m_strEmailAddress = dtrRecord["strEmailAddress"].ToString();

            m_bIsDeleted = Convert.ToBoolean(dtrRecord["bIsDeleted"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion
}