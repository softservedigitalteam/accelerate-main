
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsUserAchievements
/// </summary>
public class clsUserAchievements
{
    #region MEMBER VARIABLES

        private int m_iUserAchievementID;
        private int m_iAchievementID;
        private int m_iAccountUserID;
        
    #endregion 

    #region PROPERTIES

        public int iUserAchievementID
        {
            get
            {
                return m_iUserAchievementID;
            }
        }

        public int iAchievementID
        {
            get
            {
                return m_iAchievementID;
            }
            set
            {
                m_iAchievementID = value;
            }
        }

        public int iAccountUserID
        {
            get
            {
                return m_iAccountUserID;
            }
            set
            {
                m_iAccountUserID = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsUserAchievements()
    {
        m_iUserAchievementID = 0;
    }

    public clsUserAchievements(int iUserAchievementID)
    {
        m_iUserAchievementID = iUserAchievementID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iUserAchievementID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@iAchievementID", m_iAchievementID),
                        new SqlParameter("@iAccountUserID", m_iAccountUserID)                  
                  };

                  //### Add
                  m_iUserAchievementID = (int)clsDataAccess.ExecuteScalar("spUserAchievementsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iUserAchievementID", m_iUserAchievementID),
                         new SqlParameter("@iAchievementID", m_iAchievementID),
                         new SqlParameter("@iAccountUserID", m_iAccountUserID)
                    };
                    //### Update
                    clsDataAccess.Execute("spUserAchievementsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iUserAchievementID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iUserAchievementID", iUserAchievementID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spUserAchievementsDelete", sqlParameter);
        }

        public static DataTable GetUserAchievementsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spUserAchievementsList", EmptySqlParameter);
        }
        public static DataTable GetUserAchievementsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvUserAchievementsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvUserAchievementsList = clsDataAccess.GetDataView("spUserAchievementsList", EmptySqlParameter);
            dvUserAchievementsList.RowFilter = strFilterExpression;
            dvUserAchievementsList.Sort = strSortExpression;

            return dvUserAchievementsList.ToTable();
        }
        public static DataTable GetUserAchievementsRecords(int iAccountUserID)
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAccountUserID", iAccountUserID)
        };
            //clsDataAccess.Execute("spUserAchievementsGetRecords", sqlParameter);

            DataView dvUserAchievementsList = new DataView();
            dvUserAchievementsList = clsDataAccess.GetDataView("spUserAchievementsGetRecords", sqlParameter);

            return dvUserAchievementsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iUserAchievementID", m_iUserAchievementID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spUserAchievementsGetRecord", sqlParameter);

                m_iAchievementID = Convert.ToInt32(drRecord["iAchievementID"]);
                m_iAccountUserID = Convert.ToInt32(drRecord["iAccountUserID"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}