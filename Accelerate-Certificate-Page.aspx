﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" CodeFile="Accelerate-Certificate-Page.aspx.cs" Inherits="Accelerate_Certificate_Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="infobox">

            <center>
                <div id="pdfDiv" runat="server" class="well-short" style="background: #d7d8d6; color: #000; padding-bottom: 12px;">
                        <iframe style="width: 100%; height: 1200px; border: none;" id="pdfFrameViewer" runat="server" />
                    </div>
                <br /><br /><br />
            </center>

    </div>

    <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="Green3 btn" OnClick="btnContinue_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScrMang" Runat="Server">
</asp:Content>

