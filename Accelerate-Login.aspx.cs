﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;
using System.DirectoryServices;
using System.Data;
using System.IO;

public partial class AccelerateLogin : System.Web.UI.Page
{

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            string err = string.Empty;

            //get currently logged in user - WINDOWS USER or the account used to open the current browser window/tab
            string username = HttpContext.Current.User.Identity.Name;

            //call AD function to validate current user
            string strUserSummary = isUserInAD(username, ref err);

            if (strUserSummary != String.Empty)
            {
                string[] delim = { "<br/>" };

                List<string> lstUserInfo = strUserSummary.Split(delim, StringSplitOptions.None).ToList();

                string strUserName = "";

                foreach(string s in lstUserInfo)
                {
                    if(s.Contains("Email :"))
                    {
                        strUserName = s.Substring(s.IndexOf(":") + 1);
                        strUserName = strUserName.Replace(" ", string.Empty);
                        break;
                    }
                }

                clsAccountUsers clsAccountUser = new clsAccountUsers(strUserName, clsCommonFunctions.GetMd5Sum(strUserName));

                Session["clsAccountUsers"] = clsAccountUser;

                Response.Redirect("Accelerate-Home.aspx");
            }
            else
            {
                int iUserID = Convert.ToInt32(HttpContext.Current.Request.Cookies["UserID"].Value);

                //bool bIsChecked = Convert.ToBoolean(Request.Cookies["rememberCookie"].Value);

                clsAccountUsers clsAccountUsers = new clsAccountUsers(iUserID);

                if (clsAccountUsers.iAccountUserID != 0)
                {
                    Session["clsAccountUsers"] = clsAccountUsers;
                    Response.Redirect("Accelerate-Home.aspx");
                }

                txtUsername.Focus();
                //chkRememberMe.Checked = bIsChecked;
            }
        }
        catch
        {
            txtUsername.Focus();

            //### Logout User
            if (Request.QueryString["sysLogout"] == "true")
            {
                Response.Cookies["UserID"].Value = null;
                //### Clear all session variables
                Session.Clear();
                //Clear Cache
                if (Cache["clsGroup"] != null)
                    Cache.Remove("clsGroup");
                if (Cache["dtPhase1Missions"] != null)
                    Cache.Remove("dtPhase1Missions");
                if (Cache["dtPhase2Missions"] != null)
                    Cache.Remove("dtPhase2Missions");
                if (Cache["dtPhase3Missions"] != null)
                    Cache.Remove("dtPhase3Missions");
            }
        }

        popColourScheme();
    }

    protected void popColourScheme()
    {
        DataRow drGetLoginColours = clsDataAccess.GetRecord("SELECT TOP(1) * FROM tblCustomLoginColours WHERE bIsDeleted = 'false'");

        StringBuilder strbProfile = new StringBuilder();

        if (drGetLoginColours != null)
        {
            int colourID = Convert.ToInt32(drGetLoginColours["iCustomLoginColourID"]);

            if (colourID!=0)
            {
                clsCustomLoginColours clsCustomLoginColours = new clsCustomLoginColours(colourID);

                if (clsCustomLoginColours != null)
                {
                    litStyleSheet.Text = "<link href='" + clsCustomLoginColours.strLink + "' rel='stylesheet' />";

                    string strFileName = clsCustomLoginColours.strMasterImage;
                    string strFullPath = "CustomLoginColours/" + clsCustomLoginColours.strPathToImages + "/" + Path.GetFileNameWithoutExtension(strFileName) + Path.GetExtension(strFileName);
                    strbProfile.AppendLine("<div>");
                    strbProfile.AppendLine("<center><img src='" + strFullPath + "' width='300' height='auto' /></center></div>");
                }
            }   
        }

        litLoginImage.Text = strbProfile.ToString();
    }

    protected void lnkbtnForgottenPassword_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = false;
        lnkbtnBackToLogin.Visible = true;
        divPassword.Visible = false;
        lnkbtnLogin.Visible = false;
        lnkbtnSubmit.Visible = true;
    }

    protected void lnkbtnBackToLogin_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = true;
        lnkbtnBackToLogin.Visible = false;
        divPassword.Visible = true;
        lnkbtnLogin.Visible = true;
        lnkbtnSubmit.Visible = false;
    }

    protected void lnkbtnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            clsAccountUsers clsAccountUsers = new clsAccountUsers(txtUsername.Text, clsCommonFunctions.GetMd5Sum(txtPassword.Text));

            if (chkRememberMe.Checked == true)
            {
                HttpCookie UserCookie = new HttpCookie("UserID");
                UserCookie.Value = clsAccountUsers.iAccountUserID.ToString();
                UserCookie.Expires = DateTime.Now.AddMonths(1);
                Response.Cookies.Add(UserCookie);
            }

            Session["clsAccountUsers"] = clsAccountUsers;

            Response.Redirect("Accelerate-Home.aspx");
        }
        catch
        {
            litValidationMessage.Text = "Invalid Login Credentials.";

            if ((txtPassword.Text == "") || (txtPassword.Text == null))
            {
                litValidationMessage.Text = "Login requires a Password.";
            }
            else if ((txtUsername.Text == "") || (txtUsername.Text == null))
            {
                litValidationMessage.Text = "Login requires a username.";
            }

            if ((txtUsername.Text == "") && (txtPassword.Text == ""))
            {
                litValidationMessage.Text = "Login requires a username and matching password.";
            }

            Alert.Attributes["class"] = "alert alert-danger";
            litAlertHeading.Text = "Error";
        }
    }

    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        //### Validate email
        if (txtUsername.Text == "")
        {
            litValidationMessage.Text = "Please enter your email address.";
            Alert.Attributes["class"] = "alert alert-danger";
            litAlertHeading.Text = "Error";
        }
        else
        {
            //### Check if email address exists
            if (clsCommonFunctions.DoesRecordExist("tblAccountUsers", "strEmailAddress = '" + txtUsername.Text + "' AND bIsDeleted = 0") == true)
            {
                //### Change password & Send mail
                string strRandomPassword = "";
                //string strPassword = "";
                //strRandomPassword = clsCommonFunctions.strCreateRandomPassword(8);
                //strPassword = txtUsername.Text;

                //### Hash random password
                strRandomPassword = clsCommonFunctions.GetMd5Sum(txtUsername.Text);

                ForgottenPassword(txtUsername.Text, strRandomPassword);

                StringBuilder strbMailBuilder = new StringBuilder();

                strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
                strbMailBuilder.AppendLine("<table width='100%' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td height='50'></td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End of Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
                strbMailBuilder.AppendLine("<tbody>");

                strbMailBuilder.AppendLine("<!-- Content -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td width='60'></td>");
                strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, Arial;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
                strbMailBuilder.AppendLine("Dear User<br />");
                strbMailBuilder.AppendLine("Your password has been reset.<br />");
                strbMailBuilder.AppendLine("Your temporary password is shown below:<br />");
                strbMailBuilder.AppendLine("Password: " + txtUsername.Text + "<br/>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("<td width='60'></td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End of Content -->");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("<td width='20'></td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td height='50'></td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End of Spacing -->");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("<!-- End of textbanner -->");

                //strbMailBuilder.AppendLine("Dear User<br /><br />");
                //strbMailBuilder.AppendLine("Your password has been reset.<br /><br />");
                //strbMailBuilder.AppendLine("Your temporary password is shown below:<br /><br />");
                //strbMailBuilder.AppendLine("Password: " + txtUsername.Text + "<br/>");

                string strContent = strbMailBuilder.ToString();

                //DataConnection.GetDataObject().ExecuteScalar("spForgottenPassword '" + txtUsername.Text + "', '" + strRandomPassword + "'");
                //emailComponent.SendMail("noreply@gobundu.co.za", txtUsername.Text, "", "", "Forgotten Password", strContent, true);

                Attachment[] empty = new Attachment[] { };

                try
                {
                    passwordReminder.SendMail("no-reply@raonboarding.co.za", txtUsername.Text, "", "", "Forgotten Password", strContent, "", "", empty, true);
                }
                catch { }

                //### Redirect
                litValidationMessage.Text = "Your temporary password has been sent.";
                Alert.Attributes["class"] = "alert alert-success";
                litAlertHeading.Text = "Success";
            }
            else
            {
                //### If no email address found
                litValidationMessage.Text = "The email you have entered is not a registered email address.";
                Alert.Attributes["class"] = "alert alert-dismissable";
                litAlertHeading.Text = "Sorry";
            }
        }
    }

    #endregion

    #region PASSWORD METHODS

    public static void ForgottenPassword(string strEmailAddress, string strRandomPassword)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@strEmailAddress", strEmailAddress),
                new SqlParameter("@strRandomPassword", strRandomPassword)
            };
        //### Executes Password Reset sp
        clsDataAccess.Execute("spForgottenAccountUserPassword", sqlParameter);
    }

    #endregion

    #region SINGLE SIGN ON

    /// <summary>
    /// Utility function to string away the domain name E.G: zadeloitte\jsmith => jsmith
    /// </summary>
    /// <param name="username">Full username with domain</param>
    /// <returns></returns>
    private string ExtractUserName(string username)
    {
        //username will be passed with the escape character \. example zadeloitte\\jsmith
        string[] userPath = username.Split(new char[] { '\\' });
        return userPath[userPath.Length - 1];
    }

    /// <summary>
    /// Check if the person using the browser is in AD
    /// </summary>
    /// <param name="username">the name of the logged/browser user</param>
    /// <param name="err">Populates the error reference variable with the error message</param>
    /// <returns>Returns a string with all the required pieces of information</returns>
    private string isUserInAD(string username, ref string err)
    {
        //put your fully qualified Domain name here
        string strLDAPPath = "LDAP://zajnb0000.za.deloitte.com/DC=za,DC=deloitte,DC=com";
        string result = string.Empty;
        SearchResult srResult = null;

        try
        {
            //initiate AD connection on the supplied Domain connection string
            DirectoryEntry de = new DirectoryEntry(strLDAPPath); //Adds the domain path as a directory entry

            //initiate the AD search object and add items to be returned
            DirectorySearcher ds = new DirectorySearcher(de); // Creates a new DirectorySearch instance with the path given by de

            //filter the AD object tree by searching only the USERS/PERSON classes
            ds.Filter = "(&(objectClass=user)(objectCategory=person)(sAMAccountName=" + ExtractUserName(username) + "))"; // Filters through the DirectorySearch instance with the username given checking for a match

            //If the value is found the fields below are used to retrieve certain information about the user.

            ds.PropertiesToLoad.Add("samaccountname");
            ds.PropertiesToLoad.Add("givenname"); //This is an optional Line and is only used as a filter for the current search result.
            ds.PropertiesToLoad.Add("mail"); //This is an optional Line and is only used as a filter for the current search result.
            ds.PropertiesToLoad.Add("telephoneNumber"); //This is an optional Line and is only used as a filter for the current search result.
            ds.PropertiesToLoad.Add("mobile"); //This is an optional Line and is only used as a filter for the current search result.
            ds.PropertiesToLoad.Add("sn"); //This is an optional Line and is only used as a filter for the current search result.

            //check if user is in AD and return just one record even if there is multiple matches, just one
            srResult = ds.FindOne();

            //if user has been found, get the attributes/data from object
            //if the user is not in AD, the result will be NULL
            if (srResult != null)
            {
                string samaccountname = (string)srResult.Properties["samaccountname"][0];
                string givenname = (string)srResult.Properties["givenname"][0];
                string email = (string)srResult.Properties["mail"][0];
                string phone = (string)srResult.Properties["telephoneNumber"][0];
                string mobile = (string)srResult.Properties["mobile"][0];
                string surname = (string)srResult.Properties["sn"][0];

                //return result and set error to null
                result = string.Format("Name : {0} <br/> Surname : {1} <br/> Username : {2} <br/>  Email : {3}<br/> Tel: {4} <br/> Cell: {5}", givenname, surname, samaccountname, email, phone, mobile);
                err = null;
            }
            else
            {
                result = "Invalid user - stop fooling around...";
                err = result;
            }
        }
        catch (Exception ex)
        {
            err = ex.Message;
        }

        //return result to calling function
        return result;
    }

    #endregion

}