﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" CodeFile="Accelerate-Change-Password.aspx.cs" Inherits="Accelerate_Change_Password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" lang="javascript">
        function CheckOnOff(rdoId, gridName) {
            var rdo = document.getElementById(rdoId);
            /* Getting an array of all the INPUT controls on the form.*/
            var rdo = document.getElementById(rdoId);
            var all = document.getElementsByTagName("input");
            for (i = 0; i < all.length; i++) {
                /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
                if (all[i].type == "radio" && all[i].id != rdo.id) {
                    var count = all[i].id.indexOf(gridName);
                    if (count != -1) {
                        all[i].checked = false;
                    }
                }
            }
            rdo.checked = true; /* Finally making the clicked radio button CHECKED */
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <div class="sectionContainerLeft innerBoxWithShadow margintopten">
                <h2 class="genericHeading">Change Password</h2>
                <div class="ProfileMissionHead" style="margin-left:20px;">
                    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                        <asp:Literal ID="litValidationMessage" runat="server"></asp:Literal>
                    </div>
                    <div class="labelDiv">Password</div>
                    <asp:TextBox runat="server" ID="txtPassword" type="password" class="txtTextBoxGeneric" />
                    <asp:RequiredFieldValidator ID="rfvPassword"  CssClass="red" ControlToValidate="txtPassword" runat="server" ErrorMessage="This field is required."></asp:RequiredFieldValidator>
                    <div class="labelDiv">Confirm Password</div>
                    <asp:TextBox runat="server" ID="txtConfirmPassword" type="password" class="txtTextBoxGeneric" />
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" CssClass="red" ControlToValidate="txtConfirmPassword" runat="server" ErrorMessage="This field is required."></asp:RequiredFieldValidator><br />
                    <asp:CompareValidator ID="PasswordCompareValidator" CssClass="red" ControlToCompare="txtConfirmPassword" ControlToValidate="txtPassword" runat="server" ErrorMessage="Please make sure that the password matches"></asp:CompareValidator>
                    <br class="clr"/>
                </div>
                <asp:Button runat="server" ID="btnSave" CssClass="alignLeft " Width="80px" Text="Save" OnClick="btnSave_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

