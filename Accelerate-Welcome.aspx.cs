﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Welcome_To : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
    }

    protected void btnSmallProceed_Click(object sender, EventArgs e)
    {
        Response.Redirect("Accelerate-Profile-Mission.aspx");
    }
}