
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsRanksView
/// </summary>
public partial class CMS_clsCustomGroupRanksView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCustomGroupRanksList;

    List<clsCustomGroupRanks> glstCustomGroupRanks;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtCustomGroupRanksList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstCustomGroupRanks"] == null)
            {
                glstCustomGroupRanks = new List<clsCustomGroupRanks>();
                Session["glstCustomGroupRanks"] = glstCustomGroupRanks;
            }
            else
                glstCustomGroupRanks = (List<clsCustomGroupRanks>)Session["glstCustomGroupRanks"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTag) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtCustomGroupRanksList = clsCustomGroupRanks.GetRanksList(FilterExpression, "");

        Session["dtCustomGroupRanksList"] = dtCustomGroupRanksList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCustomGroupRanksAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsRanksList object
        try
        {
            dtCustomGroupRanksList = new DataTable();

            if (Session["dtCustomGroupRanksList"] == null)
                dtCustomGroupRanksList = clsCustomGroupRanks.GetRanksList();
            else
                dtCustomGroupRanksList = (DataTable)Session["dtCustomGroupRanksList"];

            dgrGrid.DataSource = dtCustomGroupRanksList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CustomGroupRanksView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CustomGroupRanksView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CustomGroupRanksView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCustomGroupRankID = int.Parse((sender as LinkButton).CommandArgument);

        clsCustomGroupRanks.Delete(iCustomGroupRankID);

        DataTable dtCustomGroupRanksList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A RANK") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTag) LIKE '%" + txtSearch.Text + "%'";
            dtCustomGroupRanksList = clsCustomGroupRanks.GetRanksList(FilterExpression, "");
        }
        else
        {
            dtCustomGroupRanksList = clsCustomGroupRanks.GetRanksList();
        }

        Session["dtCustomGroupRanksList"] = dtCustomGroupRanksList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CustomGroupRanksView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCustomGroupRanksList = new DataTable();

        if (Session["dtCustomGroupRanksList"] == null)
            dtCustomGroupRanksList = clsCustomGroupRanks.GetRanksList();
        else
            dtCustomGroupRanksList = (DataTable)Session["dtCustomGroupRanksList"];

        DataView dvTemp = dtCustomGroupRanksList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCustomGroupRanksList = dvTemp.ToTable();
        Session["dtCustomGroupRanksList"] = dtCustomGroupRanksList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCustomGroupRankID
            int iCustomGroupRankID = 0;
            iCustomGroupRankID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCustomGroupRanksAddModify.aspx?action=edit&iCustomGroupRankID=" + iCustomGroupRankID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkDeleteItem = (HyperLink)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

            //### Get the iCustomGroupRankID
            int iCustomGroupRankID = 0;
            iCustomGroupRankID = int.Parse(dgrItemDelete.Cells[0].Text);

            //### Add attributes to delete link
            lnkDeleteItem.CssClass = "dgrDeleteLink";
            lnkDeleteItem.Attributes.Add("href", "frmRanksView.aspx?action=delete&iCustomGroupRankID=" + iCustomGroupRankID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Ranks FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTag) LIKE '%" + prefixText + "%'";
        DataTable dtRanks = clsCustomGroupRanks.GetRanksList(FilterExpression, "");
        List<string> glstCustomGroupRanks = new List<string>();

        if (dtRanks.Rows.Count > 0)
        {
            foreach (DataRow dtrRanks in dtRanks.Rows)
            {
                glstCustomGroupRanks.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrRanks["strTag"].ToString(), dtrRanks["iCustomGroupRankID"].ToString()));
            }
        }
        else
            glstCustomGroupRanks.Add("No Ranks Available.");
        strReturnList = glstCustomGroupRanks.ToArray();
        return strReturnList;
    }

    #endregion
}
