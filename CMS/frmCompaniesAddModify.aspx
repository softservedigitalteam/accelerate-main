<%@ Page Title="Companies" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmCompaniesAddModify.aspx.cs" Inherits="CMS_frmCompaniesAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" lang="javascript">
     function CheckOnOff(rdoId,gridName)
     {
         var rdo = document.getElementById(rdoId);
         /* Getting an array of all the INPUT controls on the form.*/
         var rdo = document.getElementById(rdoId);
         var all = document.getElementsByTagName("input");
         for(i=0;i<all.length;i++)
         {
             /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
             if(all[i].type=="radio" && all[i].id != rdo.id)
             {
                 var count=all[i].id.indexOf(gridName);
                 if(count!=-1)
                 {
                     all[i].checked=false;
                 }
             }
         }
     rdo.checked=true; /* Finally making the clicked radio button CHECKED */
     }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="updMain" runat="server">
    <ContentTemplate>

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Company Name:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCompanyName" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Tag Line:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTagLine" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Description:</div>
         <div class="fieldDiv">
             <asp:TextBox style="overflow:auto" ID="txtDescription" TextMode="MultiLine" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,3000)" onblur="setValid(this);" Width="100%"/>
         </div>
         <br class="clearingSpacer" />
         <br />
     </div>

      <%--   <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Primary Color:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPrimaryColor" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

         <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Secondary Color:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSecondaryColor" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

         <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Tertiary Color:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTertiaryColor" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>--%>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Primary Contact:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPrimaryContact" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Primary Email:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPrimaryEmail" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,120);" Style="text-transform: lowercase;"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Primary Telephone:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPrimaryTelephone" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,15)"/>
         </div>
         <br class="clearingSpacer" />
     </div>

<%--     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
          <div class="labelDiv">Images:<br />Minimum 104 x 104</div>
         <div class="fieldDiv">
             <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" Width="100%"  onblur="setValid(this);" onchange="this.form.submit()"/>
             <%--<div style="float:left;"><asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" onclick="btnUpload_Click" style="margin-left:5px;" /></div>
             <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear:both"/>
             <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                 <ContentTemplate>
                     <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                         <ItemTemplate>
                             <table cellspacing="5">
                                 <%# Container.DataItem %>
                             </table>
                             <div align="center" style="padding-top: 5px;">
                                 <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainImages" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                             </div>
                         </ItemTemplate>
                     </asp:DataList>
                 </ContentTemplate>
             </asp:UpdatePanel><br />
             <b><asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
         </div>
         <br class="clearingSpacer" />
     </div>--%>

    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>

         </ContentTemplate>
<%--         <Triggers>
             <asp:PostBackTrigger ControlID="btnUpload" />
         </Triggers>--%>
     </asp:UpdatePanel>
</asp:Content>
