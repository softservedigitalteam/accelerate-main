using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMissionTypesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMissionTypes clsMissionTypes;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iMissionTypeID is passed through then we want to instantiate the object with that iMissionTypeID
            if ((Request.QueryString["iMissionTypeID"] != "") && (Request.QueryString["iMissionTypeID"] != null))
            {
                clsMissionTypes = new clsMissionTypes(Convert.ToInt32(Request.QueryString["iMissionTypeID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsMissionTypes = new clsMissionTypes();
            }
            Session["clsMissionTypes"] = clsMissionTypes;
        }
        else
        {
            clsMissionTypes = (clsMissionTypes)Session["clsMissionTypes"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMissionTypesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Title added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' MissionType=''/><div class=\"validationMessage\">Please fill out all mandatory fields - MissionType not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtTitle.Text = clsMissionTypes.strTitle;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsMissionTypes.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionTypes.iAddedBy = clsUsers.iUserID;
        clsMissionTypes.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionTypes.iEditedBy = clsUsers.iUserID;
        clsMissionTypes.strTitle = txtTitle.Text;

        clsMissionTypes.Update();

        Session["dtMissionTypesList"] = null;

        //### Go back to view page
        Response.Redirect("frmMissionTypesView.aspx");
    }

    #endregion
}