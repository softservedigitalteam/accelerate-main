<%@ Page Title="Add Missions To Phase" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmPhaseMissionsView.aspx.cs" Inherits="CMS_clsMissionsView" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script lang="javascript" type="text/javascript">
    //### Password check
    function clearText() {
            document.getElementById("<%=txtSearch.ClientID%>").value = ""
    }
</script>
<script type="text/javascript">
    function RefreshUpdatePanel(sender) {
        __doPostBack(sender, '');
    }
</script>

<!-- Event Manager for selection of item from auto-complete list -->
<script lang="javascript" type="text/javascript">
    function ClientItemSelected(sender, e) {
        $get("<%=hfMissionID.ClientID %>").value = e.get_value();
        $get('<%=lnkbtnSearch.ClientID %>').click();
    }

    function toggleDiv(divID) {
        $(divID).slideToggle();
    }
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Panel ID="pSearch" runat="server" DefaultButton="lnkbtnSearch">
        <div class="currentPageTab" id="divPageName" style="width:200px; height:45px; margin-top:5px;" >
                                <asp:Label ID="lblPhaseName" runat="server" ForeColor="#5a5a5a" Font-Bold="true"></asp:Label>
                            </div>
        <div class="controlDiv" style="margin-top:15px;">
            <div class="fieldSearchDiv">
                
                <asp:TextBox ID="txtSearch" runat="server" CssClass="roundedCornerTextBox" Text="SEARCH FOR A MISSION TO ADD" onfocus="clearText();"/>
                <ajax:AutoCompleteExtender ID="aceMissions" runat="server" TargetControlID="txtSearch" ServiceMethod="FilterBusinessRule"
                        EnableCaching="true" MinimumPrefixLength="1" CompletionInterval="0" CompletionSetCount="3" 
                        CompletionListCssClass="popupCompletionList" CompletionListItemCssClass="popupCompletionListItem" CompletionListHighlightedItemCssClass="popupCompletionListItemHighlight"
                        OnClientItemSelected="ClientItemSelected" FirstRowSelected="true" />
                        <asp:HiddenField ID="hfMissionID" runat="server" Value="" />
            </div>
            <div class="buttonsSearchDiv"><asp:LinkButton ID="lnkbtnSearch" runat="server" CssClass="searchButton" onclick="lnkbtnSearch_Click" /></div>
        </div>
        <br class="clearingSpacer"/>
    </asp:Panel>

    <br />

    <asp:UpdatePanel ID="udpMissionsView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Literal ID="litMissionHeading" runat="server">ADD MISSIONS FROM THIS TABLE TO THE CURRENT PHASE</asp:Literal><br /><br />
            <asp:DataGrid ID="dgrGrid" Runat="server" AutoGenerateColumns="False" AllowSorting="true" 
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrGrid_PageIndexChanged" 
                OnSortCommand="dgrGrid_SortCommand" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev"
                HeaderStyle-CssClass="dgrHeader"  ItemStyle-CssClass="dgrItem" AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iMissionID" Visible="false" />
                    <asp:BoundColumn DataField="strTitle" SortExpression="strTitle" HeaderText="Mission Name" HeaderStyle-CssClass="dgrHeaderLeft" HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strTag" SortExpression="strTag" HeaderText="Mission Tag" HeaderStyle-HorizontalAlign="Left"/>

                    <asp:TemplateColumn HeaderText="Add" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-CssClass="dgrHeaderRight">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkAddItem" runat="Server" CssClass="dgrLinkAdd" CommandArgument='<%# Eval("iMissionID") %>' OnClick="lnkAddItem_Click" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>
            <br /><asp:Literal ID="litOrderHeading" runat="server">LIST OF MISSIONS LINKED TO THE CURRENT PHASE</asp:Literal><br />
            <asp:Literal ID="litPhaseName" runat="server"></asp:Literal><br /><br />

            <asp:DataGrid ID="dgOrder" Runat="server" AutoGenerateColumns="False" AllowSorting="true" 
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgOrder_PageIndexChanged" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev"
                HeaderStyle-CssClass="dgrHeader"  ItemStyle-CssClass="dgrItem" AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iPhaseMissionID" Visible="false" />
                    <asp:BoundColumn DataField="strTitle" SortExpression="strTitle" HeaderText="Mission Name" HeaderStyle-CssClass="dgrHeaderLeft" HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strMissionTag" SortExpression="strMissionTag" HeaderText="Mission Tag" HeaderStyle-HorizontalAlign="Left"/>

                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"  HeaderStyle-CssClass="dgrHeaderRight" >
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDeleteItem" runat="server" CssClass="dgrLinkDelete" CommandArgument='<%# Eval("iPhaseMissionID") %>' OnClick="lnkDeleteItem_Click" OnClientClick="return jsDeleteConfirm()"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>

            <asp:Literal runat="server" ID="litTotal" ></asp:Literal>

            
            <div class="buttonsRightDiv" style="clear:both;display:none;">
                <div class="Line"></div>
                <asp:LinkButton ID="lnkbtnSubmit" runat="server" CssClass="submitButton" onclick="lnkbtnSubmit_Click" />
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
