using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmUserAnswersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsUserAnswers clsUserAnswers;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popAccountUser();
             popMissionQuestion();

            //### If the iUserAnswerID is passed through then we want to instantiate the object with that iUserAnswerID
            if ((Request.QueryString["iUserAnswerID"] != "") && (Request.QueryString["iUserAnswerID"] != null))
            {
                clsUserAnswers = new clsUserAnswers(Convert.ToInt32(Request.QueryString["iUserAnswerID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsUserAnswers = new clsUserAnswers();
            }
            Session["clsUserAnswers"] = clsUserAnswers;
        }
        else
        {
            clsUserAnswers = (clsUserAnswers)Session["clsUserAnswers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmUserAnswersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtAnswer, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstAccountUser, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstMissionQuestion, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">UserAnswer added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - UserAnswer not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtAnswer.Text = "";
        clsValidation.SetValid(txtAnswer);
        lstAccountUser.SelectedValue = "0";
        clsValidation.SetValid(lstAccountUser);
        lstMissionQuestion.SelectedValue = "0";
        clsValidation.SetValid(lstMissionQuestion);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtAnswer.Text = clsUserAnswers.strAnswer;
         lstAccountUser.SelectedValue = clsUserAnswers.iAccountUserID.ToString();
         lstMissionQuestion.SelectedValue = clsUserAnswers.iMissionQuestionID.ToString();
    }
    
    private void popAccountUser()
    {
         DataTable dtAccountUsersList = new DataTable();
         lstAccountUser.DataSource = clsAccountUsers.GetAccountUsersList();

         //### Populates the drop down list with PK and TITLE;
         lstAccountUser.DataValueField = "iAccountUserID";
         lstAccountUser.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstAccountUser.DataBind();

         //### Add default select option;
         lstAccountUser.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popMissionQuestion()
    {
         DataTable dtMissionQuestionsList = new DataTable();
         lstMissionQuestion.DataSource = clsMissionQuestions.GetMissionQuestionsList();

         //### Populates the drop down list with PK and TITLE;
         lstMissionQuestion.DataValueField = "iMissionQuestionID";
         lstMissionQuestion.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstMissionQuestion.DataBind();

         //### Add default select option;
         lstMissionQuestion.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsUserAnswers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsUserAnswers.iAddedBy = clsUsers.iUserID;
        clsUserAnswers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsUserAnswers.iEditedBy = clsUsers.iUserID;
        clsUserAnswers.strAnswer = txtAnswer.Text;
        clsUserAnswers.iAccountUserID = Convert.ToInt32(lstAccountUser.SelectedValue.ToString());
        clsUserAnswers.iMissionQuestionID = Convert.ToInt32(lstMissionQuestion.SelectedValue.ToString());

        clsUserAnswers.Update();

        Session["dtUserAnswersList"] = null;

        //### Go back to view page
        Response.Redirect("frmUserAnswersView.aspx");
    }

    #endregion
}