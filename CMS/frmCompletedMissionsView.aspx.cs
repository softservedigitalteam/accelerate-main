using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsCompletedMissionsView
/// </summary>
public partial class CMS_clsCompletedMissionsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCompletedMissionsList;

    List<clsCompletedMissions> glstCompletedMissions;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtCompletedMissionsList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstCompletedMissions"] == null)
            {
                glstCompletedMissions = new List<clsCompletedMissions>();
                Session["glstCompletedMissions"] = glstCompletedMissions;
            }
            else
                glstCompletedMissions = (List<clsCompletedMissions>)Session["glstCompletedMissions"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="() LIKE '%" + txtSearch.Text + "%'";
        DataTable dtCompletedMissionsList = clsCompletedMissions.GetCompletedMissionsList(FilterExpression, "");

        Session["dtCompletedMissionsList"] = dtCompletedMissionsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCompletedMissionsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCompletedMissionsList object
        try
        {
            dtCompletedMissionsList = new DataTable();

            if (Session["dtCompletedMissionsList"] == null)
                dtCompletedMissionsList = clsCompletedMissions.GetCompletedMissionsList();
            else
                dtCompletedMissionsList = (DataTable)Session["dtCompletedMissionsList"];

            dgrGrid.DataSource = dtCompletedMissionsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CompletedMissionsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CompletedMissionsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CompletedMissionsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCompletedMissionID = int.Parse((sender as LinkButton).CommandArgument);

        clsCompletedMissions.Delete(iCompletedMissionID);

        DataTable dtCompletedMissionsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A COMPLETED MISSION") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "() LIKE '%" + txtSearch.Text + "%'";
            dtCompletedMissionsList = clsCompletedMissions.GetCompletedMissionsList(FilterExpression, "");
        }
        else
        {
            dtCompletedMissionsList = clsCompletedMissions.GetCompletedMissionsList();
        }

        Session["dtCompletedMissionsList"] = dtCompletedMissionsList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CompletedMissionsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCompletedMissionsList = new DataTable();

        if (Session["dtCompletedMissionsList"] == null)
            dtCompletedMissionsList = clsCompletedMissions.GetCompletedMissionsList();
        else
            dtCompletedMissionsList = (DataTable)Session["dtCompletedMissionsList"];

        DataView dvTemp = dtCompletedMissionsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCompletedMissionsList = dvTemp.ToTable();
        Session["dtCompletedMissionsList"] = dtCompletedMissionsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCompletedMissionID
            int iCompletedMissionID = 0;
            iCompletedMissionID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCompletedMissionsAddModify.aspx?action=edit&iCompletedMissionID=" + iCompletedMissionID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iCompletedMissionID
             int iCompletedMissionID = 0;
             iCompletedMissionID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmCompletedMissionsView.aspx?action=delete&iCompletedMissionID=" + iCompletedMissionID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region CompletedMissions FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "() LIKE '%" + prefixText + "%'";
        DataTable dtCompletedMissions = clsCompletedMissions.GetCompletedMissionsList(FilterExpression, "");
        List<string> glstCompletedMissions = new List<string>();

        if (dtCompletedMissions.Rows.Count > 0)
        {
            foreach (DataRow dtrCompletedMissions in dtCompletedMissions.Rows)
            {
                glstCompletedMissions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dtrCompletedMissions["iCompletedMissionID"].ToString(), dtrCompletedMissions["iCompletedMissionID"].ToString()));
            }
        }
        else
            glstCompletedMissions.Add("No Completed Missions Available.");
        strReturnList = glstCompletedMissions.ToArray();
        return strReturnList;
    }

    #endregion
}
