﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMS_frmLandingZone : System.Web.UI.Page
{

    int iNoGoups = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //NoOfUsersPerGroup();   
    }

    # region POP METHODS

    //### Runs through all the groups and checks how many users are in that group
    private void NoOfUsersPerGroup()
    {

        StringBuilder sbNoOfUsersPerGroup = new StringBuilder();
        string GroupName = "";
        int iGroupID = 0;
        string strNoOfUsersPerGroup = "";
        int iCompleteMissionCount = 0;

        DataTable dtGroupList = new DataTable();
        dtGroupList = clsGroups.GetGroupsList();

        sbNoOfUsersPerGroup.AppendLine("SYSTEM SUMMARY<br/><br/>");

        //## Calculates No Of groups
        foreach (DataRow dtrGroupList in dtGroupList.Rows)
        {
            int iNoOfUsers = 0;
            iGroupID = Convert.ToInt32(dtrGroupList["iGroupID"]);
            GroupName = dtrGroupList["strTitle"].ToString();

            DataTable dtAccountUserList = new DataTable();
            dtAccountUserList = clsAccountUsers.GetAccountUsersList("iGroupID=" + iGroupID, "");


            //## Calculates No Of Users
            foreach (DataRow dtrAccountUser in dtAccountUserList.Rows)
            {
                ++iNoOfUsers;
                int iAccountUserID = Convert.ToInt32(dtrAccountUser["iAccountUserID"]);
                int iGroupPhaseID = Convert.ToInt32(dtrAccountUser["iGroupID"]);

                DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupPhaseID, "");

                //## Gets the No Of Phase Per Group and pass through the ID to get Number of Completed Missions
                foreach (DataRow dtrGroupPhasesList in dtGroupPhases.Rows)
                {
                    int iCurrentPhaseID = Convert.ToInt32(dtrGroupPhasesList["iPhaseID"]);
                    ++iNoGoups;

                    iCompleteMissionCount = iCompleteMissionCount + getCompleteMissionsFromCurrentPhase(iCurrentPhaseID, iAccountUserID);
                }
            }

            sbNoOfUsersPerGroup.AppendLine(iNoOfUsers + " User(s) in " + GroupName + "<br/>");  
        }
        //sbNoOfUsersPerGroup.AppendLine("<br/>");
        //sbNoOfUsersPerGroup.AppendLine("<br/>");
        sbNoOfUsersPerGroup.AppendLine(iCompleteMissionCount.ToString() + " Missions Completed Overall");

        //strNoOfUsersPerGroup = strNoOfUsersPerGroup + sbNoOfUsersPerGroup.ToString();
        litReport.Text = sbNoOfUsersPerGroup.ToString();
    }

    //### Get the Phases for that Group
    protected DataTable getAllPhasesForCurrentUserGroup(int iCurrentGroup)
    {
        DataTable dtPhaseList = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iCurrentGroup, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        //int iTotalPhases = dtPhaseList.Rows.Count;

        return dtPhaseList;
    }

    protected int getCompleteMissionsFromCurrentPhase(int iCurrentPhaseID, int iAccountUserID)
    {
        clsPhases clsCurrentPhase = new clsPhases(iCurrentPhaseID);
        DataTable dtPhase = clsPhases.GetPhasesList("iPhaseID=" + iCurrentPhaseID, "");
        int iCompleteMissionsFromCurrentPhase = 0;

        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        DateTime dtStartTime = new DateTime();
        DateTime dtEndTime = new DateTime();

        foreach (DataRow dtrCurrentPhase in dtPhase.Rows)
        {
           
            dtStartTime = Convert.ToDateTime(clsCurrentPhase.dtStartDate);
            dtEndTime = Convert.ToDateTime(clsCurrentPhase.dtEndDate);

            if (dtStartTime <= dtCurrentDate && dtEndTime >= dtCurrentDate)
            {
                iCompleteMissionsFromCurrentPhase = iCompleteMissionsFromCurrentPhase + getCompleteMissionsList(iCurrentPhaseID, iAccountUserID);
            }
        }

        return iCompleteMissionsFromCurrentPhase;
    }

    protected int getCompleteMissionsList(int iPhaseID, int iAccountUserID)
    {
        int iNoOfCompletedMissions = 0;

        //### GET MISSIONS FOR CURRENT PHASE
        DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iPhaseID);

        foreach (DataRow dtrMissionItems in dtMissionList.Rows)
        {
            int iMissionID = Convert.ToInt32(dtrMissionItems["iMissionID"]);

            clsMissions clsMissions = new clsMissions(iMissionID);
            int iMissionTypeID = clsMissions.iMissionTypeID;

            if (iMissionTypeID == 1)
            {
                if (bIsTextMissionComplete(iMissionID, iAccountUserID))
                    ++iNoOfCompletedMissions;
            }
            else if (iMissionTypeID == 2)
            {
                if (bIsTeamLeadershipMissionComplete(iMissionID, iAccountUserID))
                    ++iNoOfCompletedMissions;
            }
        }
        return iNoOfCompletedMissions;
    }

    protected DataTable getAllMissionsForCurrentUserPhase(int iCurrentPhase)
    {
        DataTable dtMissionList = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iCurrentPhase, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        int iTotalMissions = dtMissionList.Rows.Count;

        return dtMissionList;
    }
    protected bool bIsTextMissionComplete(int iCurrentMissionID, int iAccountUserID)
    {
        bool bIsMissionComplete = false;

        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount) *100;

            if (dblCurrentPercentage == 100)
                bIsMissionComplete = true;
        }

        return bIsMissionComplete;
    }

    //### Returns bIsTeamLeadershipMissionComplete for Teamship mission
    protected bool bIsTeamLeadershipMissionComplete(int iCurrentMissionID, int iAccountUserID)
    {
        bool bIsTeamLeadershipMissionComplete = false;

        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount) * 100;

            if (dblCurrentPercentage == 100)
                bIsTeamLeadershipMissionComplete = true;
        }

        return bIsTeamLeadershipMissionComplete;
    }

    #endregion

}
