<%@ Page Title="Account Users" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmCompaniesView.aspx.cs" Inherits="CMS_clsCompaniesView" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        //### Password check
        function clearText() {
            document.getElementById("<%=txtSearch.ClientID%>").value = ""
    }
    </script>
    <script type="text/javascript">
        function RefreshUpdatePanel(sender) {
            __doPostBack(sender, '');
        }
    </script>

    <!-- Event Manager for selection of item from auto-complete list -->
    <script lang="javascript" type="text/javascript">
        function ClientItemSelected(sender, e) {
            $get("<%=hfAccountUserID.ClientID %>").value = e.get_value();
        $get('<%=lnkbtnSearch.ClientID %>').click();
    }

    function toggleDiv(divID) {
        $(divID).slideToggle();
    }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Panel ID="pSearch" runat="server" DefaultButton="lnkbtnSearch">
        <div class="controlDiv" style="margin-top: 15px;">
            <div class="fieldSearchDiv">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="roundedCornerTextBox" Text="SEARCH FOR A COMPANY" onfocus="clearText();" />
                <ajax:AutoCompleteExtender ID="aceAccountUsers" runat="server" TargetControlID="txtSearch" ServiceMethod="FilterBusinessRule"
                    EnableCaching="true" MinimumPrefixLength="1" CompletionInterval="0" CompletionSetCount="3"
                    CompletionListCssClass="popupCompletionList" CompletionListItemCssClass="popupCompletionListItem" CompletionListHighlightedItemCssClass="popupCompletionListItemHighlight"
                    OnClientItemSelected="ClientItemSelected" FirstRowSelected="true" />
                <asp:HiddenField ID="hfAccountUserID" runat="server" Value="" />
            </div>
            <div class="buttonsSearchDiv">
                <asp:LinkButton ID="lnkbtnSearch" runat="server" CssClass="searchButton" OnClick="lnkbtnSearch_Click" /></div>
        </div>
        <br class="clearingSpacer" />
    </asp:Panel>

    <br />

    <asp:UpdatePanel ID="udpAccountUsersView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:DataGrid ID="dgrGrid" runat="server" AutoGenerateColumns="False" AllowSorting="true"
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrGrid_PageIndexChanged"
                OnSortCommand="dgrGrid_SortCommand" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev"
                HeaderStyle-CssClass="dgrHeader" ItemStyle-CssClass="dgrItem" AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iCompanyID" Visible="false" />
                    <asp:BoundColumn DataField="strTitle" SortExpression="strTitle" HeaderText="Company Name" HeaderStyle-CssClass="dgrHeaderLeft" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundColumn DataField="strTagLine" SortExpression="strTagLine" HeaderText="Tag Line" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundColumn DataField="strPrimaryContact" SortExpression="strPrimaryContact" HeaderText="Primary Contact" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundColumn DataField="strPrimaryEmail" SortExpression="strPrimaryEmail" HeaderText="Primary Email" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundColumn DataField="strPrimaryTelephone" SortExpression="strPrimaryTelephone" HeaderText="Primary Number" HeaderStyle-HorizontalAlign="left" />

                    <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEditItem" runat="Server" CssClass="dgrLinkEdit" OnDataBinding="dgrGrid_PopEditLink" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-CssClass="dgrHeaderRight">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDeleteItem" runat="server" CssClass="dgrLinkDelete" CommandArgument='<%# Eval("iCompanyID") %>' OnClick="lnkDeleteItem_Click" OnClientClick="return jsDeleteConfirm()" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnAdd" runat="server" CssClass="addButton" OnClick="lnkbtnAdd_Click" />
    </div>

<%--    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnImport" runat="server" CssClass="importButton"/>
    </div>--%>

</asp:Content>
