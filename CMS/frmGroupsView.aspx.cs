using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for clsGroupsView
/// </summary>
public partial class CMS_clsGroupsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtGroupsList;

    List<clsGroups> glstGroups;

    private static string lstGroupsValue;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtGroupsList"] = null;
            //popGroupType();
            PopulateFormData("");
            popGroup();
        }
        else
        {
            if (Session["glstGroups"] == null)
            {
                glstGroups = new List<clsGroups>();
                Session["glstGroups"] = glstGroups;
            }
            else
                glstGroups = (List<clsGroups>)Session["glstGroups"];
        }
        PopulateFormData("");
    }

    protected void lstGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        int iGroupID = Convert.ToInt32(lstGroupFilter.SelectedValue.ToString());

        if (iGroupID != -1)
            PopulateFormData("iLevelID=" + iGroupID);
        else
            PopulateFormData("");
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression = "(strTitle) LIKE '%" + txtSearch.Text + "%'";

        DataTable dtGroupsList = clsGroups.GetGroupsList(FilterExpression, "");

        Session["dtGroupsList"] = dtGroupsList;

        PopulateFormData("");
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmGroupsAddModify.aspx");
    }

    //protected void lstGroupType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    dtGroupsList = new DataTable();
    //    if (lstGroupType.SelectedValue != "0")
    //    {
    //        dtGroupsList = clsGroups.GetGroupsList("iGroupTypeID = " + lstGroupType.SelectedValue.ToString(), "strTitle");
    //    }
    //    else
    //    {
    //        dtGroupsList = clsGroups.GetGroupsList("", "strTitle");
    //    }

    //    dgrGrid.DataSource = dtGroupsList;

    //    Session["dtGroupsList"] = dtGroupsList;

    //    dgrGrid.DataBind();

    //    lstGroupsValue = lstGroupType.SelectedValue;
    //}
   
    #endregion

    #region POPULATE METHODS
    //private void popGroupType()
    //{
    //    DataTable dtGroupTypesList = new DataTable();
    //    lstGroupType.DataSource = clsGroupTypes.GetGroupTypesList();

    //    //### Populates the drop down list with PK and TITLE;
    //    lstGroupType.DataValueField = "iGroupTypeID";
    //    lstGroupType.DataTextField = "strTitle";

    //    //### Bind the data to the list;
    //    lstGroupType.DataBind();

    //    //### Add default select option;
    //    lstGroupType.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}

    private void popGroup()
    {
        DataTable dtGroupsList = new DataTable();
        lstGroupFilter.DataSource = clsGroups.GetGroupsList("iLevelID=0", "strTitle ASC");

        //### Populates the drop down list with PK and TITLE;
        lstGroupFilter.DataValueField = "iGroupID";
        lstGroupFilter.DataTextField = "strTitle";

        //### Bind the data to the list;
        lstGroupFilter.DataBind();

        //### Add default select option;
        //lstGroupFilter.Items.Insert(0, new ListItem("--Not Selected--", "-2"));
        lstGroupFilter.Items.Insert(0, new ListItem("-- MASTER GROUP ITEMS --", "0"));
        lstGroupFilter.Items.Insert(0, new ListItem("-- ALL --", "-1"));
    }
    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData(string strFilter)
    {
        //### Populate datagrid using clsGroupsList object
        try
        {
            dtGroupsList = new DataTable();

            //dtGroupsList = clsGroups.GetGroupsList(strFilter, "strTitle");

            //Added after commented out changes. Using for delete method and to refresh grid after a delete has occured
            if (Session["dtGroupsList"] == null)
                dtGroupsList = clsGroups.GetGroupsList(strFilter, "dtAdded DESC");
            else
                dtGroupsList = (DataTable)Session["dtGroupsList"];

            dgrGrid.DataSource = dtGroupsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["GroupsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["GroupsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["GroupsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iGroupID = int.Parse((sender as LinkButton).CommandArgument);

        clsGroups.Delete(iGroupID);

        DataTable dtGroupsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A BUSINESS UNIT") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle) LIKE '%" + txtSearch.Text + "%'";
            dtGroupsList = clsGroups.GetGroupsList(FilterExpression, "");
        }
        else
        {
            dtGroupsList = clsGroups.GetGroupsList("", "strTitle");
        }

        Session["dtGroupsList"] = dtGroupsList;

        PopulateFormData("");
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["GroupsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData("");
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtGroupsList = new DataTable();

        if (Session["dtGroupsList"] == null)
            dtGroupsList = clsGroups.GetGroupsList();
        else
            dtGroupsList = (DataTable)Session["dtGroupsList"];

        DataView dvTemp = dtGroupsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtGroupsList = dvTemp.ToTable();
        Session["dtGroupsList"] = dtGroupsList;

        PopulateFormData("");
    }

    public void dgrGrid_PoplnkAddFeaturesToGroups(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkAddFeaturesToGroups = (HyperLink)sender;
            DataGridItem dgrItemAddFeatures = (DataGridItem)lnkAddFeaturesToGroups.Parent.Parent;

            //### Get the ID
            int iGroupID = 0;
            iGroupID = int.Parse(dgrItemAddFeatures.Cells[0].Text);

            //### Add attributes to edit link
            lnkAddFeaturesToGroups.Attributes.Add("href", "frmGroupFeaturesView.aspx?iGroupID=" + iGroupID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PoplnkAddPhasesToGroups(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkAddPhasesToGroups = (HyperLink)sender;
            DataGridItem dgrItemAddMissions = (DataGridItem)lnkAddPhasesToGroups.Parent.Parent;

            //### Get the ID
            int iGroupID = 0;
            iGroupID = int.Parse(dgrItemAddMissions.Cells[0].Text);

            //### Add attributes to edit link
            lnkAddPhasesToGroups.Attributes.Add("href", "frmGroupPhasesView.aspx?iGroupID=" + iGroupID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_PoplnkCopyGroups(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkCopyGroup = (HyperLink)sender;
            DataGridItem dgrCopyGroup = (DataGridItem)lnkCopyGroup.Parent.Parent;

            //### Get the iGroupID
            int iGroupID = 0;
            iGroupID = int.Parse(dgrCopyGroup.Cells[0].Text);

            //### Add attributes to edit link
            lnkCopyGroup.Attributes.Add("href", "frmGroupCopy.aspx?action=edit&iGroupID=" + iGroupID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iGroupID
            int iGroupID = 0;
            iGroupID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmGroupsAddModify.aspx?action=edit&iGroupID=" + iGroupID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkDeleteItem = (HyperLink)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

            //### Get the iGroupID
            int iGroupID = 0;
            iGroupID = int.Parse(dgrItemDelete.Cells[0].Text);

            //### Add attributes to delete link
            lnkDeleteItem.CssClass = "dgrDeleteLink";
            lnkDeleteItem.Attributes.Add("href", "frmGroupsView.aspx?action=delete&iGroupID=" + iGroupID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region Groups FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";

        //if (lstGroupsValue != "0" && lstGroupsValue != null)
        //{
        //    FilterExpression += "AND iGroupTypeID = " + lstGroupsValue;
        //}

        DataTable dtGroups = clsGroups.GetGroupsList(FilterExpression, "");
        List<string> glstGroups = new List<string>();

        if (dtGroups.Rows.Count > 0)
        {
            foreach (DataRow dtrGroups in dtGroups.Rows)
            {
                glstGroups.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrGroups["strTitle"].ToString(), dtrGroups["iGroupID"].ToString()));
            }
        }
        else
            glstGroups.Add("No Business Units Available.");
        strReturnList = glstGroups.ToArray();
        return strReturnList;
    }
    #endregion

}


