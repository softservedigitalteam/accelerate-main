
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsDefaultImagesView
/// </summary>
public partial class CMS_clsDefaultImagesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtDefaultImagesList;

    List<clsDefaultImages> glstDefaultImages;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtDefaultImagesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstDefaultImages"] == null)
            {
                glstDefaultImages = new List<clsDefaultImages>();
                Session["glstDefaultImages"] = glstDefaultImages;
            }
            else
                glstDefaultImages = (List<clsDefaultImages>)Session["glstDefaultImages"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle +' '+ strDescription) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtDefaultImagesList = clsDefaultImages.GetDefaultImagesList(FilterExpression, "");

        Session["dtDefaultImagesList"] = dtDefaultImagesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmDefaultImagesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsDefaultImagesList object
        try
        {
            dtDefaultImagesList = new DataTable();

            if (Session["dtDefaultImagesList"] == null)
                dtDefaultImagesList = clsDefaultImages.GetDefaultImagesList();
            else
                dtDefaultImagesList = (DataTable)Session["dtDefaultImagesList"];

            dgrGrid.DataSource = dtDefaultImagesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["DefaultImagesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["DefaultImagesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["DefaultImagesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iDefaultImageID = int.Parse((sender as LinkButton).CommandArgument);

        clsDefaultImages.Delete(iDefaultImageID);

        DataTable dtDefaultImagesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A DEFAULT IMAGE") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle +' '+ strDescription) LIKE '%" + txtSearch.Text + "%'";
            dtDefaultImagesList = clsDefaultImages.GetDefaultImagesList(FilterExpression, "");
        }
        else
        {
            dtDefaultImagesList = clsDefaultImages.GetDefaultImagesList();
        }

        Session["dtDefaultImagesList"] = dtDefaultImagesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["DefaultImagesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtDefaultImagesList = new DataTable();

        if (Session["dtDefaultImagesList"] == null)
            dtDefaultImagesList = clsDefaultImages.GetDefaultImagesList();
        else
            dtDefaultImagesList = (DataTable)Session["dtDefaultImagesList"];

        DataView dvTemp = dtDefaultImagesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtDefaultImagesList = dvTemp.ToTable();
        Session["dtDefaultImagesList"] = dtDefaultImagesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iDefaultImageID
            int iDefaultImageID = 0;
            iDefaultImageID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmDefaultImagesAddModify.aspx?action=edit&iDefaultImageID=" + iDefaultImageID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iDefaultImageID
             int iDefaultImageID = 0;
             iDefaultImageID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmAchievementsView.aspx?action=delete&iDefaultImageID=" + iDefaultImageID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Achievements FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle +' '+ strDescription) LIKE '%" + prefixText + "%'";
        DataTable dtAchievements = clsDefaultImages.GetDefaultImagesList(FilterExpression, "");
        List<string> glstDefaultImages = new List<string>();

        if (dtAchievements.Rows.Count > 0)
        {
            foreach (DataRow dtrAchievements in dtAchievements.Rows)
            {
                glstDefaultImages.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrAchievements["strTitle"].ToString() +' '+"" + dtrAchievements["strDescription"].ToString(), dtrAchievements["iDefaultImageID"].ToString()));
            }
        }
        else
            glstDefaultImages.Add("No Default Images Available.");
        strReturnList = glstDefaultImages.ToArray();
        return strReturnList;
    }

    #endregion
}
