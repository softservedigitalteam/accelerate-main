using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMissionDropDownAnswersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMissionDropDownAnswers clsMissionDropDownAnswers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popMissionQuestion();

            //### If the iMissionDropDownAnswerID is passed through then we want to instantiate the object with that iMissionDropDownAnswerID
             if ((Request.QueryString["iMissionDropDownAnswerID"] != "") && (Request.QueryString["iMissionDropDownAnswerID"] != null))
            {
                clsMissionDropDownAnswers = new clsMissionDropDownAnswers(Convert.ToInt32(Request.QueryString["iMissionDropDownAnswerID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsMissionDropDownAnswers = new clsMissionDropDownAnswers();
            }
            Session["clsMissionDropDownAnswers"] = clsMissionDropDownAnswers;
        }
        else
        {
            clsMissionDropDownAnswers = (clsMissionDropDownAnswers)Session["clsMissionDropDownAnswers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMissionDropDownAnswersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(lstMissionQuestion, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtAnswer, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">MissionAnswer added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - MissionAnswer not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        lstMissionQuestion.SelectedValue = "0";
        clsValidation.SetValid(lstMissionQuestion);
        txtAnswer.Text = "";
        clsValidation.SetValid(txtAnswer);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        lstMissionQuestion.SelectedValue = clsMissionDropDownAnswers.iMissionDropDownQuestionID.ToString();
        txtAnswer.Text = clsMissionDropDownAnswers.strAnswer;

        if (clsMissionDropDownAnswers.bIsCorrectAnswer)
            cbIsCorrect.Checked = true;
    }
    
    private void popMissionQuestion()
    {
         DataTable dtMissionQuestionsList = new DataTable();
         lstMissionQuestion.DataSource = clsMissionDropDownQuestions.GetMissionQuestionsList();

         //### Populates the drop down list with PK and TITLE;
         lstMissionQuestion.DataValueField = "iMissionDropDownQuestionID";
         lstMissionQuestion.DataTextField = "strQuestion";

         //### Bind the data to the list;
         lstMissionQuestion.DataBind();

         //### Add default select option;
         lstMissionQuestion.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsMissionDropDownAnswers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionDropDownAnswers.iAddedBy = clsUsers.iUserID;
        clsMissionDropDownAnswers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionDropDownAnswers.iEditedBy = clsUsers.iUserID;
        clsMissionDropDownAnswers.iMissionDropDownQuestionID = Convert.ToInt32(lstMissionQuestion.SelectedValue.ToString());
        clsMissionDropDownAnswers.strAnswer = txtAnswer.Text;

        clsMissionDropDownAnswers.bIsCorrectAnswer = cbIsCorrect.Checked;

        clsMissionDropDownAnswers.Update();

        Session["dtMissionDropDownAnswersList"] = null;

        //### Go back to view page
        Response.Redirect("frmMissionDropDownAnswersView.aspx");
    }

    #endregion
}