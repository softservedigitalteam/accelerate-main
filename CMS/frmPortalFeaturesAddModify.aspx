<%@ Page Title="Portal Features" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmPortalFeaturesAddModify.aspx.cs" Inherits="CMS_frmPortalFeaturesAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" lang="javascript">
     function CheckOnOff(rdoId,gridName)
     {
         var rdo = document.getElementById(rdoId);
         /* Getting an array of all the INPUT controls on the form.*/
         var rdo = document.getElementById(rdoId);
         var all = document.getElementsByTagName("input");
         for(i=0;i<all.length;i++)
         {
             /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
             if(all[i].type=="radio" && all[i].id != rdo.id)
             {
                 var count=all[i].id.indexOf(gridName);
                 if(count!=-1)
                 {
                     all[i].checked=false;
                 }
             }
         }
     rdo.checked=true; /* Finally making the clicked radio button CHECKED */
     }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="updMain" runat="server">
    <ContentTemplate>

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label> 
    </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Feature Type:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstFeatureType" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);">
                 <asp:ListItem>-- Not Selected --</asp:ListItem>
                 <asp:ListItem>Normal</asp:ListItem>
                 <asp:ListItem>Checklist</asp:ListItem>
                 <asp:ListItem>Multiple</asp:ListItem>
             </asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,300)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Description:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtDescription" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,4999)" TextMode="MultiLine" Rows="8" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">You Tube Link:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtYouTubeLink" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,1500)" TextMode="MultiLine" Rows="8" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Images:</div>
         <div class="fieldDiv">
             <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" Width="350px"  onblur="setValid(this);" onchange="this.form.submit()"/>
             <%--<div style="float:left;"><asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" onclick="btnUpload_Click" style="margin-left:5px;" /></div>--%>
             <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear:both"/>
             <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                 <ContentTemplate>
                     <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <asp:Image ID="imgLoader" runat="server" ImageUrl="images/imgLoader.gif" />Loading. Please wait.
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                     <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                         <ItemTemplate>
                             <table cellspacing="5">
                                 <%# Container.DataItem %>
                             </table>
                             <div align="center" style="padding-top: 5px; display: none;">
                                 <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainImages" Checked="true" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                             </div>
                         </ItemTemplate>
                     </asp:DataList>
                 </ContentTemplate>
             </asp:UpdatePanel><br />
             <b><asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
         </div>
         <br class="clearingSpacer" />
     </div>

        

         <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <%--<asp:Label ID="lblDescribe" runat="server" Text="Please make sure file has the name that you wish and that spaces are represented as '-' dashes."></asp:Label>--%>
         <div class="labelDiv">Document:</div>
         <div class="fieldDiv">
             <asp:FileUpload ID="DocumentUpload" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" Width="350px" onchange="this.form.submit()"/>
             <%--<div style="float:left;"><asp:LinkButton ID="btnDocumentUpload" runat="server" CssClass="uploadButton" onclick="btnDocumentUpload_Click" style="margin-left:5px;" /></div>--%>
             <br class="clearingSpacer" />
             <asp:Label ID="lblUniqueDocumentPath" runat="Server" Visible="false"></asp:Label><br /><asp:Label ID="lblFileSize" runat="Server" Visible="false"></asp:Label>
             <asp:UpdatePanel ID="udpDocument" runat="server" ChildrenAsTriggers="true">
                 <ContentTemplate>
                     <asp:DataList ID="dlDocument" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                         <ItemTemplate>
                             <table cellspacing="5">
                                 <%# Container.DataItem %>
                             </table>
                             <div align="center" style="padding-top: 5px;">
                                 <asp:RadioButton ID="rdbMainDocument" runat="server" Checked="true" GroupName="MainDocument" Text="Main Doc" onclick="javascript:CheckOnOff(this.id,'dlDocument');" Visible="false" /><br />
                             </div>
                         </ItemTemplate>
                     </asp:DataList>
                 </ContentTemplate>
             </asp:UpdatePanel><br />
             <b><asp:Label ID="lblUploadDocError" runat="server"></asp:Label></b>
         </div>
         <br class="clearingSpacer" />
     </div>


    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>

         </ContentTemplate>
<%--         <Triggers>
             <asp:PostBackTrigger ControlID="btnUpload" />
             <asp:PostBackTrigger ControlID="btnDocumentUpload" />
         </Triggers>--%>
     </asp:UpdatePanel>
</asp:Content>
