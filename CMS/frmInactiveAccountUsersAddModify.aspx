<%@ Page Title="InactiveAccountUsers" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmInactiveAccountUsersAddModify.aspx.cs" Inherits="CMS_frmInactiveAccountUsersAddModify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">First Name:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtFirstName" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Surname:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSurname" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Email Address:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,120)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Phone Number:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Business Unit:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtJobTitle" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,150)" TextMode="MultiLine" Rows="2" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <%--<div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Group:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstGroup" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>--%>

    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
