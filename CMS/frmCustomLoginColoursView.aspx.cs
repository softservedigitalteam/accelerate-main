
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsCustomLoginColoursView
/// </summary>
public partial class CMS_clsCustomLoginColoursView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCustomLoginColoursList;

    List<clsCustomLoginColours> glstCustomLoginColours;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtCustomLoginColoursList"] = null;
            PopulateFormData();
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strMasterImage +' '+ strPanelBackgroundColour +' '+ strRememberMeTextColour +' '+ strButtonBackgroundColour +' '+ strButtonTextColour +' '+ strTextBelowButtonColour) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtCustomLoginColoursList = clsCustomLoginColours.GetCustomLoginColoursList(FilterExpression, "");

        Session["dtCustomLoginColoursList"] = dtCustomLoginColoursList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCustomLoginColoursAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCustomLoginColoursList object
        try
        {
            dtCustomLoginColoursList = new DataTable();

            if (Session["dtCustomLoginColoursList"] == null)
                dtCustomLoginColoursList = clsCustomLoginColours.GetCustomLoginColoursList();
            else
                dtCustomLoginColoursList = (DataTable)Session["dtCustomLoginColoursList"];

            dgrGrid.DataSource = dtCustomLoginColoursList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CustomLoginColoursView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CustomLoginColoursView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CustomLoginColoursView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCustomLoginColourID = int.Parse((sender as LinkButton).CommandArgument);

        clsCustomLoginColours.Delete(iCustomLoginColourID);

        DataTable dtCustomLoginColoursList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A CUSTOM LOGIN COLOUR") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strMasterImage +' '+ strPanelBackgroundColour +' '+ strRememberMeTextColour +' '+ strButtonBackgroundColour +' '+ strButtonTextColour +' '+ strTextBelowButtonColour) LIKE '%" + txtSearch.Text + "%'";
            dtCustomLoginColoursList = clsCustomLoginColours.GetCustomLoginColoursList(FilterExpression, "");
        }
        else
        {
            dtCustomLoginColoursList = clsCustomLoginColours.GetCustomLoginColoursList();
        }

        Session["dtCustomLoginColoursList"] = dtCustomLoginColoursList;

        PopulateFormData();
        //Session["dtCustomLoginColoursList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CustomLoginColoursView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCustomLoginColoursList = new DataTable();

        if (Session["dtCustomLoginColoursList"] == null)
            dtCustomLoginColoursList = clsCustomLoginColours.GetCustomLoginColoursList();
        else
            dtCustomLoginColoursList = (DataTable)Session["dtCustomLoginColoursList"];

        DataView dvTemp = dtCustomLoginColoursList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCustomLoginColoursList = dvTemp.ToTable();
        Session["dtCustomLoginColoursList"] = dtCustomLoginColoursList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCustomLoginColourID
            int iCustomLoginColourID = 0;
            iCustomLoginColourID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCustomLoginColoursAddModify.aspx?action=edit&iCustomLoginColourID=" + iCustomLoginColourID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region CustomLoginColours FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strMasterImage +' '+ strPanelBackgroundColour +' '+ strRememberMeTextColour +' '+ strButtonBackgroundColour +' '+ strButtonTextColour +' '+ strTextBelowButtonColour) LIKE '%" + prefixText + "%'";
        DataTable dtCustomLoginColours = clsCustomLoginColours.GetCustomLoginColoursList(FilterExpression, "");
        List<string> glstCustomLoginColours = new List<string>();

        if (dtCustomLoginColours.Rows.Count > 0)
        {
            foreach (DataRow dtrCustomLoginColours in dtCustomLoginColours.Rows)
            {
                glstCustomLoginColours.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCustomLoginColours["strMasterImage"].ToString() +' '+"" + dtrCustomLoginColours["strPanelBackgroundColour"].ToString() +' '+"" + dtrCustomLoginColours["strRememberMeTextColour"].ToString() +' '+"" + dtrCustomLoginColours["strButtonBackgroundColour"].ToString() +' '+"" + dtrCustomLoginColours["strButtonTextColour"].ToString() +' '+"" + dtrCustomLoginColours["strTextBelowButtonColour"].ToString(), dtrCustomLoginColours["iCustomLoginColourID"].ToString()));
            }
        }
        else
            glstCustomLoginColours.Add("No CustomLoginColours Available.");
        strReturnList = glstCustomLoginColours.ToArray();
        return strReturnList;
    }

    #endregion
}
