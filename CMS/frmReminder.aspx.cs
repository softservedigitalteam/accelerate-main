﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMS_Reminder : System.Web.UI.Page
{
    clsUsers clsUsers;
    int iNoOfMissionsCompleted = 0;
    int iNoOfUsersPerGroup=0;
   
    #region EVENTS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers"];
        
        if (!IsPostBack)
        {
            
        }
        else
        {
            
        }

    }

    protected void btnRemindUser_Click(object sender, EventArgs e)
    {
        getAllPhasesForCurrentUserGroup();
    }

    #endregion

    #region EMAIL METHODS

    protected void SendReport(int iAccountUserID, string strMissionList)
    {
        clsAccountUsers clsCurrentAccountUser = new clsAccountUsers(iAccountUserID);

        //### Settings
        string strReportName = "Reminder";

        StringBuilder strbMailBuilder = new StringBuilder();

        strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
        strbMailBuilder.AppendLine("<table width='100%' bgcolor='#efefef' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td height='50'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
        strbMailBuilder.AppendLine("<tbody>");

        strbMailBuilder.AppendLine("<!-- Content -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td width='60'></td>");
        strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
        strbMailBuilder.AppendLine("Dear " + clsCurrentAccountUser.strFirstName + "<br /><br />");
        strbMailBuilder.AppendLine("The following missions have not been completed:<br /><br />");
        strbMailBuilder.AppendLine(strMissionList);
        strbMailBuilder.AppendLine("<br/>");
        strbMailBuilder.AppendLine("Please Login to the system to complete the missions<br/>");
        strbMailBuilder.AppendLine("" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "/Accelerate-Home.aspx");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td width='60'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Content -->");

        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td width='20'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td height='50'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of textbanner -->");

        Attachment[] empty = new Attachment[] { };

        clsEmailSettings clsEmailSettings = new clsEmailSettings(true, clsCurrentAccountUser.iAccountUserID);

        if (clsEmailSettings.bEmailMissions)
        {

            try
            {
                reminderEmail.SendMail("no-reply@raonboarding.co.za", clsCurrentAccountUser.strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "", "", empty, true);
            }
            catch { }
        }
    }

        //lblValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">There is no such user on the system.</div>";
    

    #endregion

    # region POP METHODS
  
    //### Runs through all users and gets each user group for Phase to get missions--> then mission q and a
    private void getAllPhasesForCurrentUserGroup()
    {
        DataTable dtAccountUserList = new DataTable();
        dtAccountUserList = clsAccountUsers.GetAccountUsersList();

        foreach (DataRow dtrAccountUser in dtAccountUserList.Rows)
        {
            int iAccountUserID = Convert.ToInt32(dtrAccountUser["iAccountUserID"]);
            int iGroupID = Convert.ToInt32(dtrAccountUser["iGroupID"]);
            string strUncompleteMissionList = "";
            //string strFirstName = dtrAccountUser["strFirstName"].ToString();

            DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, ""); 

            foreach (DataRow dtrGroupPhasesList in dtGroupPhases.Rows)
            {
                int iCurrentPhaseID = Convert.ToInt32(dtrGroupPhasesList["iPhaseID"]);
                strUncompleteMissionList = strUncompleteMissionList + getUncompleteMissionsFromCurrentPhase(iCurrentPhaseID, iAccountUserID);
            }
            SendReport(iAccountUserID, strUncompleteMissionList); 
        }
    }

    //### Get the Phases for that Group
    protected DataTable getAllPhasesForCurrentUserGroup(int iCurrentGroup)
    {
        DataTable dtPhaseList = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iCurrentGroup, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        //int iTotalPhases = dtPhaseList.Rows.Count;

        return dtPhaseList;
    }

    protected string getUncompleteMissionsFromCurrentPhase(int iCurrentPhaseID, int iAccountUserID)
    {
        clsPhases clsCurrentPhase = new clsPhases(iCurrentPhaseID);
        DataTable dtPhase = clsPhases.GetPhasesList("iPhaseID=" + iCurrentPhaseID, "");

        int iCount = 0;
        string strUncompleteMissions = "";
        string strUncompleteMissionsList = "";

        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        DateTime dtStartTime = new DateTime();
        DateTime dtEndTime = new DateTime();

        foreach (DataRow dtrCurrentPhase in dtPhase.Rows)
        {
            ++iCount;
            dtStartTime = Convert.ToDateTime(clsCurrentPhase.dtStartDate);
            dtEndTime = Convert.ToDateTime(clsCurrentPhase.dtEndDate);

            if (dtStartTime <= dtCurrentDate && dtEndTime >= dtCurrentDate)
            {
                strUncompleteMissions = strUncompleteMissions.ToString() + getUncompleteMissionsList(iCurrentPhaseID, iAccountUserID);
            }
        }

        strUncompleteMissionsList = strUncompleteMissions.ToString() + "<br />The -" + clsCurrentPhase.strTitle + "- phase expires on " + clsCurrentPhase.dtEndDate.ToString("dd MMMM yyyy") + "<br /><br />";

        return strUncompleteMissionsList;
    }

    protected string getUncompleteMissionsList(int iPhaseID, int iAccountUserID)
    {
        StringBuilder strbMissionLiteral = new StringBuilder();
        string strMissions = "";

        //### GET MISSIONS FOR CURRENT PHASE
        DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iPhaseID);

        foreach (DataRow dtrMissionItems in dtMissionList.Rows)
        {
            int iMissionID = Convert.ToInt32(dtrMissionItems["iMissionID"]);

            clsMissions clsMissions = new clsMissions(iMissionID);
            int iMissionTypeID = clsMissions.iMissionTypeID;

            if (iMissionTypeID == 1)
            {
                if(!bIsTextMissionComplete(iMissionID, iAccountUserID))
                    strbMissionLiteral.Append(clsMissions.strTitle + "<br />");
            }
            else if (iMissionTypeID == 2)
            {
                if (!bIsTeamLeadershipMissionComplete(iMissionID, iAccountUserID))
                    strbMissionLiteral.Append(clsMissions.strTitle + "<br />");
            }
            strMissions =  strbMissionLiteral.ToString();
        }
        return strMissions;
    }

    protected string getCompleteMissionsFromCurrentPhase(int iCurrentPhaseID, int iAccountUserID)
    {
        clsPhases clsCurrentPhase = new clsPhases(iCurrentPhaseID);
        DataTable dtPhase = clsPhases.GetPhasesList("iPhaseID=" + iCurrentPhaseID, "");

        int iCount = 0;
        string strUncompleteMissions = "";
        string strUncompleteMissionsList = "";

        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        DateTime dtStartTime = new DateTime();
        DateTime dtEndTime = new DateTime();

        foreach (DataRow dtrCurrentPhase in dtPhase.Rows)
        {
            ++iCount;
            dtStartTime = Convert.ToDateTime(clsCurrentPhase.dtStartDate);
            dtEndTime = Convert.ToDateTime(clsCurrentPhase.dtEndDate);

            if (dtStartTime <= dtCurrentDate && dtEndTime >= dtCurrentDate)
            {
                strUncompleteMissions = strUncompleteMissions.ToString() + getCompleteMissionsList(iCurrentPhaseID, iAccountUserID);
            }
        }

        strUncompleteMissionsList = strUncompleteMissions.ToString() + "<br />The -" + clsCurrentPhase.strTitle + "- phase expires on " + clsCurrentPhase.dtEndDate.ToString("dd MMMM yyyy") + "<br /><br />";

        return strUncompleteMissionsList;
    }

    protected string getCompleteMissionsList(int iPhaseID, int iAccountUserID)
    {
        StringBuilder strbMissionLiteral = new StringBuilder();
        string strMissions = "";

        //### GET MISSIONS FOR CURRENT PHASE
        DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iPhaseID);

        foreach (DataRow dtrMissionItems in dtMissionList.Rows)
        {
            int iMissionID = Convert.ToInt32(dtrMissionItems["iMissionID"]);

            clsMissions clsMissions = new clsMissions(iMissionID);
            int iMissionTypeID = clsMissions.iMissionTypeID;

            if (iMissionTypeID == 1)
            {
                if (bIsTextMissionComplete(iMissionID, iAccountUserID))
                    strbMissionLiteral.Append(clsMissions.strTitle + "<br />");
            }
            else if (iMissionTypeID == 2)
            {
                if (bIsTeamLeadershipMissionComplete(iMissionID, iAccountUserID))
                    strbMissionLiteral.Append(clsMissions.strTitle + "<br />");
            }
            strMissions = strbMissionLiteral.ToString();
        }
        return strMissions;
    }

    protected DataTable getAllMissionsForCurrentUserPhase(int iCurrentPhase)
    {
        DataTable dtMissionList = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iCurrentPhase, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        int iTotalMissions = dtMissionList.Rows.Count;

        return dtMissionList;
    }

    protected string popCurrentMissionInfo(int iCurrentMissionID)
    {
        string strCurrentMission;

        DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iCurrentMissionID);

        clsMissions clsMissions = new clsMissions(iCurrentMissionID);

        StringBuilder strbCurrentMission = new StringBuilder();

        strbCurrentMission.AppendLine(clsMissions.strTitle.ToUpper() + "<br/>");
           // + "POINTS: " + clsMissions.iBonusPoints );

        //if (clsMissions.strDescription.Length >= 250)
        //    strbCurrentMission.AppendLine("<p class='currentMissionText'>" + clsMissions.strDescription.Substring(0, 250) + "...</p>");
        //else
        //    strbCurrentMission.AppendLine("<p class='currentMissionText'>" + clsMissions.strDescription + "...</p>");

        strCurrentMission = strbCurrentMission.ToString();

        return strCurrentMission;
    }

    protected bool bIsTextMissionComplete(int iCurrentMissionID, int iAccountUserID)
    {
        bool bIsMissionComplete = false;

        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount) * 100;
            
            if (dblCurrentPercentage == 100)
                bIsMissionComplete = true;
        }

        return bIsMissionComplete;
    }

    //### Returns bIsTeamLeadershipMissionComplete for Teamship mission
    protected bool bIsTeamLeadershipMissionComplete(int iCurrentMissionID, int iAccountUserID)
    {
        bool bIsTeamLeadershipMissionComplete = false;

        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount) * 100;
            
            if (dblCurrentPercentage == 100)
                bIsTeamLeadershipMissionComplete = true;
        }

        return bIsTeamLeadershipMissionComplete;
    }

    #endregion
}