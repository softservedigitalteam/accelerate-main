using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmCustomGroupRanksAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCustomGroupRanks clsCustomGroupRanks;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iCustomGroupRankID is passed through then we want to instantiate the object with that iCustomGroupRankID
            if ((Request.QueryString["iCustomGroupRankID"] != "") && (Request.QueryString["iCustomGroupRankID"] != null))
            {
                clsCustomGroupRanks = new clsCustomGroupRanks(Convert.ToInt32(Request.QueryString["iCustomGroupRankID"]));

                //### Populate the form
                //popGroups();
                popFormData();
            }
            else
            {
                //popGroups();
                clsCustomGroupRanks = new clsCustomGroupRanks();
            }
            Session["clsCustomGroupRanks"] = clsCustomGroupRanks;
        }
        else
        {
            clsCustomGroupRanks = (clsCustomGroupRanks)Session["clsCustomGroupRanks"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmRanksView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;
        bCanSave = clsValidation.IsNullOrEmpty(txtRank1Points, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtRank1Title, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTag, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Rank added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Rank not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        //txtTitle.Text = "";
        //clsValidation.SetValid(txtTitle);
        //txtPointLevel.Text = "";
        //clsValidation.SetValid(txtPointLevel);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtRank1Title.Text = clsCustomGroupRanks.strRank1Title;
        txtRank2Title.Text = clsCustomGroupRanks.strRank2Title;
        txtRank3Title.Text = clsCustomGroupRanks.strRank3Title;
        txtRank4Title.Text = clsCustomGroupRanks.strRank4Title;
        txtRank5Title.Text = clsCustomGroupRanks.strRank5Title;
        txtRank6Title.Text = clsCustomGroupRanks.strRank6Title;
        txtRank7Title.Text = clsCustomGroupRanks.strRank7Title;

        //Rank Points
        if (clsCustomGroupRanks.iRank1Points != 0)
        {
            txtRank1Points.Text = clsCustomGroupRanks.iRank1Points.ToString();
            lblRank1.Text = "[" + 0 + " - " + txtRank1Points.Text + "]";
            lblRank1.Visible = true;
            lblRank1.Style.Add("float", "left");  
            lblRank1.Style.Add("margin-top", "10px");
            lblRank1.Style.Add("margin-left", "10px");
        }
        if (clsCustomGroupRanks.iRank2Points != 0)
        {
            txtRank2Points.Text = clsCustomGroupRanks.iRank2Points.ToString();
            lblRank2.Text = "[" + txtRank1Points.Text + " - " + txtRank2Points.Text + "]";
            lblRank2.Visible = true;
            lblRank2.Style.Add("float", "left");
            lblRank2.Style.Add("margin-top", "10px");
            lblRank2.Style.Add("margin-left", "10px");
        }
        if (clsCustomGroupRanks.iRank3Points != 0)
        {
            txtRank3Points.Text = clsCustomGroupRanks.iRank3Points.ToString();
            lblRank3.Text = "[" + txtRank2Points.Text + " - " + txtRank3Points.Text + "]";
            lblRank3.Visible = true;
            lblRank3.Style.Add("float", "left");
            lblRank3.Style.Add("margin-top", "10px");
            lblRank3.Style.Add("margin-left", "10px");
        }
        if (clsCustomGroupRanks.iRank4Points != 0)
        {
            txtRank4Points.Text = clsCustomGroupRanks.iRank4Points.ToString();
            lblRank4.Text = "[" + txtRank3Points.Text + " - " + txtRank4Points.Text + "]";
            lblRank4.Visible = true;
            lblRank4.Style.Add("float", "left");
            lblRank4.Style.Add("margin-top", "10px");
            lblRank4.Style.Add("margin-left", "10px");
        }
        if (clsCustomGroupRanks.iRank5Points != 0)
        {
            txtRank5Points.Text = clsCustomGroupRanks.iRank5Points.ToString();
            lblRank5.Text = "[" + txtRank4Points.Text + " - " + txtRank5Points.Text + "]";
            lblRank5.Visible = true;
            lblRank5.Style.Add("float", "left");
            lblRank5.Style.Add("margin-top", "10px");
            lblRank5.Style.Add("margin-left", "10px");
        }
        if (clsCustomGroupRanks.iRank6Points != 0)
        {
            txtRank6Points.Text = clsCustomGroupRanks.iRank6Points.ToString();
            lblRank6.Text = "[" + txtRank5Points.Text + " - " + txtRank6Points.Text + "]";
            lblRank6.Visible = true;
            lblRank6.Style.Add("float", "left");
            lblRank6.Style.Add("margin-top", "10px");
            lblRank6.Style.Add("margin-left", "10px");
        }
        if (clsCustomGroupRanks.iRank7Points != 0)
        {
            txtRank7Points.Text = clsCustomGroupRanks.iRank7Points.ToString();
            lblRank7.Text = "[" + txtRank6Points.Text + " - " + txtRank7Points.Text + "]";
            lblRank7.Visible = true;
            lblRank7.Style.Add("float", "left");
            lblRank7.Style.Add("margin-top", "10px");
            lblRank7.Style.Add("margin-left", "10px");
        }

        PopulationValidation();

        //lstGroup.SelectedValue = clsCustomGroupRanks.iGroupID.ToString();
        txtTag.Text = clsCustomGroupRanks.strTag;

        //Rank Titles

    }

    //private void popGroups()
    //{
    //    DataTable dtGroupsList = new DataTable();
    //    lstGroup.DataSource = clsGroups.GetGroupsList();

    //    //### Populates the drop down list with PK and TITLE;
    //    lstGroup.DataValueField = "iGroupID";
    //    lstGroup.DataTextField = "strTitle";

    //    //### Bind the data to the list;
    //    lstGroup.DataBind();

    //    //### Add default select option;
    //    lstGroup.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //Other
        clsCustomGroupRanks.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomGroupRanks.iAddedBy = clsUsers.iUserID;
        clsCustomGroupRanks.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomGroupRanks.iEditedBy = clsUsers.iUserID;
        clsCustomGroupRanks.iGroupID = 0;//Convert.ToInt32(lstGroup.SelectedValue.ToString());
        clsCustomGroupRanks.strTag = txtTag.Text;

        //Rank Titles
        clsCustomGroupRanks.strRank1Title = txtRank1Title.Text;
        clsCustomGroupRanks.strRank2Title = txtRank2Title.Text;
        clsCustomGroupRanks.strRank3Title = txtRank3Title.Text;
        clsCustomGroupRanks.strRank4Title = txtRank4Title.Text;
        clsCustomGroupRanks.strRank5Title = txtRank5Title.Text;
        clsCustomGroupRanks.strRank6Title = txtRank6Title.Text;
        clsCustomGroupRanks.strRank7Title = txtRank7Title.Text;

        //Rank Points
        if (txtRank1Points.Text != "" && txtRank1Points.Text != null)
        {
            clsCustomGroupRanks.iRank1Points = Convert.ToInt32(txtRank1Points.Text);
        }
        else
        {
            clsCustomGroupRanks.iRank1Points = 0;
        }

        if (txtRank2Points.Text != "" && txtRank2Points.Text != null)
        {
            clsCustomGroupRanks.iRank2Points = Convert.ToInt32(txtRank2Points.Text);
        }
        else
        {
            clsCustomGroupRanks.iRank2Points = 0;
        }

        if (txtRank3Points.Text != "" && txtRank3Points.Text != null)
        {
            clsCustomGroupRanks.iRank3Points = Convert.ToInt32(txtRank3Points.Text);
        }
        else
        {
            clsCustomGroupRanks.iRank3Points = 0;
        }

        if (txtRank4Points.Text != "" && txtRank4Points.Text != null)
        {
            clsCustomGroupRanks.iRank4Points = Convert.ToInt32(txtRank4Points.Text);
        }
        else
        {
            clsCustomGroupRanks.iRank4Points = 0;
        }

        if (txtRank5Points.Text != "" && txtRank5Points.Text != null)
        {
            clsCustomGroupRanks.iRank5Points = Convert.ToInt32(txtRank5Points.Text);
        }
        else
        {
            clsCustomGroupRanks.iRank5Points = 0;
        }

        if (txtRank6Points.Text != "" && txtRank6Points.Text != null)
        {
            clsCustomGroupRanks.iRank6Points = Convert.ToInt32(txtRank6Points.Text);
        }
        else
        {
            clsCustomGroupRanks.iRank6Points = 0;
        }

        if (txtRank7Points.Text != "" && txtRank7Points.Text != null)
        {
            clsCustomGroupRanks.iRank7Points = Convert.ToInt32(txtRank7Points.Text);
        }
        else
        {
            clsCustomGroupRanks.iRank7Points = 0;
        }

        //Save Call
        clsCustomGroupRanks.Update();

        //DataTable dtGroups = clsGroups.GetGroupsList();
        //foreach(DataRow dr in dtGroups.Rows)
        //{
        //    if(Convert.ToInt32(dr["iGroupID"]) == clsCustomGroupRanks.iGroupID)
        //    {
        //        clsGroups clsGroup = new clsGroups(clsCustomGroupRanks.iGroupID);
        //        clsGroup.dtEdited = DateTime.Now;
        //        clsGroup.iEditedBy = clsUsers.iUserID;
        //        clsGroup.iCustomGroupRankID = clsCustomGroupRanks.iCustomGroupRankID;
        //        clsGroup.Update();
        //        break;
        //    }
        //}
        ///WTF???
        ///

        Session["dtRanksList"] = null;

        //### Go back to view page
        Response.Redirect("frmCustomGroupRanksView.aspx");
    }

    #endregion

    protected void txtRank1Points_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank1Title_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }


    protected void txtRank2Title_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank2Points_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }
    protected void txtRank3Title_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank3Points_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank4Title_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank4Points_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank5Title_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank5Points_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank6Title_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank6Points_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank7Title_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }

    protected void txtRank7Points_TextChanged(object sender, EventArgs e)
    {
        PopulationValidation();
    }


    private void PopulationValidation()
    {
        if ((txtRank1Title.Text != "" && txtRank1Title.Text != null) && (txtRank1Points.Text != "0" && txtRank1Points.Text != "" && txtRank1Points.Text != null))
        {
            pnlRank2.Visible = true;

            if ((txtRank2Title.Text != "" && txtRank2Title.Text != null) && (txtRank2Points.Text != "0" && txtRank2Points.Text != "" && txtRank2Points.Text != null))
            {
                if (Convert.ToInt32(txtRank2Points.Text) > Convert.ToInt32(txtRank1Points.Text))
                {
                    pnlRank3.Visible = true;
                    mandatoryDiv.Visible = false;
                }
                else
                {
                    mandatoryDiv.Visible = true;
                    lblValidationMessage.Text = "The rank values must be incremental. Rank 2's point threshold must be higher than rank 1, etc.";

                    pnlRank3.Visible = false;
                    pnlRank4.Visible = false;
                    pnlRank5.Visible = false;
                    pnlRank6.Visible = false;
                    pnlRank7.Visible = false;

                    //Textboxes
                    txtRank3Points.Text = "";
                    txtRank4Points.Text = "";
                    txtRank5Points.Text = "";
                    txtRank6Points.Text = "";
                    txtRank7Points.Text = "";

                    txtRank3Title.Text = "";
                    txtRank4Title.Text = "";
                    txtRank5Title.Text = "";
                    txtRank6Title.Text = "";
                    txtRank7Title.Text = "";
                }

                if ((txtRank3Title.Text != "" && txtRank3Title.Text != null) && (txtRank3Points.Text != "0" && txtRank3Points.Text != "" && txtRank3Points.Text != null))
                {
                    if (Convert.ToInt32(txtRank3Points.Text) > Convert.ToInt32(txtRank2Points.Text))
                    {
                        pnlRank4.Visible = true;
                        mandatoryDiv.Visible = false;
                    }
                    else
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "The rank values must be incremental. Rank 2's point threshold must be higher than rank 1, etc.";

                        pnlRank4.Visible = false;
                        pnlRank5.Visible = false;
                        pnlRank6.Visible = false;
                        pnlRank7.Visible = false;

                        //Textboxes
                        txtRank4Points.Text = "";
                        txtRank5Points.Text = "";
                        txtRank6Points.Text = "";
                        txtRank7Points.Text = "";

                        txtRank4Title.Text = "";
                        txtRank5Title.Text = "";
                        txtRank6Title.Text = "";
                        txtRank7Title.Text = "";
                    }

                    if ((txtRank4Title.Text != "" && txtRank4Title.Text != null) && (txtRank4Points.Text != "0" && txtRank4Points.Text != "" && txtRank4Points.Text != null))
                    {
                        if (Convert.ToInt32(txtRank4Points.Text) > Convert.ToInt32(txtRank3Points.Text))
                        {
                            pnlRank5.Visible = true;
                            mandatoryDiv.Visible = false;
                        }
                        else
                        {
                            mandatoryDiv.Visible = true;
                            lblValidationMessage.Text = "The rank values must be incremental. Rank 2's point threshold must be higher than rank 1, etc.";

                            pnlRank5.Visible = false;
                            pnlRank6.Visible = false;
                            pnlRank7.Visible = false;

                            //Textboxes
                            txtRank5Points.Text = "";
                            txtRank6Points.Text = "";
                            txtRank7Points.Text = "";

                            txtRank5Title.Text = "";
                            txtRank6Title.Text = "";
                            txtRank7Title.Text = "";
                        }

                        if ((txtRank5Title.Text != "" && txtRank5Title.Text != null) && (txtRank5Points.Text != "0" && txtRank5Points.Text != "" && txtRank5Points.Text != null))
                        {
                            if (Convert.ToInt32(txtRank5Points.Text) > Convert.ToInt32(txtRank4Points.Text))
                            {
                                pnlRank6.Visible = true;
                                mandatoryDiv.Visible = false;
                            }
                            else
                            {
                                mandatoryDiv.Visible = true;
                                lblValidationMessage.Text = "The rank values must be incremental. Rank 2's point threshold must be higher than rank 1, etc.";

                                pnlRank6.Visible = false;
                                pnlRank7.Visible = false;

                                //Textboxes

                                txtRank6Points.Text = "";
                                txtRank7Points.Text = "";

                                txtRank6Title.Text = "";
                                txtRank7Title.Text = "";
                            }

                            if ((txtRank6Title.Text != "" && txtRank6Title.Text != null) && (txtRank6Points.Text != "0" && txtRank6Points.Text != "" && txtRank6Points.Text != null))
                            {
                                if (Convert.ToInt32(txtRank6Points.Text) > Convert.ToInt32(txtRank5Points.Text))
                                {
                                    pnlRank7.Visible = true;
                                    mandatoryDiv.Visible = false;
                                }
                                else
                                {
                                    mandatoryDiv.Visible = true;
                                    lblValidationMessage.Text = "The rank values must be incremental. Rank 2's point threshold must be higher than rank 1, etc.";

                                    pnlRank7.Visible = false;

                                    txtRank7Title.Text = "";
                                    txtRank7Points.Text = "";
                                }
                            }
                            else
                            {
                                pnlRank7.Visible = false;

                                txtRank6Points.Text = "";
                                txtRank7Title.Text = "";
                                txtRank7Points.Text = "";

                            }
                        }
                        else
                        {
                            pnlRank6.Visible = false;
                            pnlRank7.Visible = false;

                            //Textboxes
                            txtRank5Points.Text = "";
                            txtRank6Points.Text = "";
                            txtRank7Points.Text = "";

                            txtRank6Title.Text = "";
                            txtRank7Title.Text = "";
                        }
                    }
                    else
                    {
                        pnlRank5.Visible = false;
                        pnlRank6.Visible = false;
                        pnlRank7.Visible = false;

                        //Textboxes
                        txtRank4Points.Text = "";
                        txtRank5Points.Text = "";
                        txtRank6Points.Text = "";
                        txtRank7Points.Text = "";

                        txtRank5Title.Text = "";
                        txtRank6Title.Text = "";
                        txtRank7Title.Text = "";
                    }
                }
                else
                {
                    pnlRank4.Visible = false;
                    pnlRank5.Visible = false;
                    pnlRank6.Visible = false;
                    pnlRank7.Visible = false;

                    //Textboxes
                    txtRank3Points.Text = "";
                    txtRank4Points.Text = "";
                    txtRank5Points.Text = "";
                    txtRank6Points.Text = "";
                    txtRank7Points.Text = "";

                    txtRank4Title.Text = "";
                    txtRank5Title.Text = "";
                    txtRank6Title.Text = "";
                    txtRank7Title.Text = "";


                }
            }
            else
            {
                pnlRank3.Visible = false;
                pnlRank4.Visible = false;
                pnlRank5.Visible = false;
                pnlRank6.Visible = false;
                pnlRank7.Visible = false;

                //Textboxes
                txtRank2Points.Text = "";
                txtRank3Points.Text = "";
                txtRank4Points.Text = "";
                txtRank5Points.Text = "";
                txtRank6Points.Text = "";
                txtRank7Points.Text = "";

                txtRank3Title.Text = "";
                txtRank4Title.Text = "";
                txtRank5Title.Text = "";
                txtRank6Title.Text = "";
                txtRank7Title.Text = "";

            }
        }
        else
        {
            //Panels
            pnlRank2.Visible = false;
            pnlRank3.Visible = false;
            pnlRank4.Visible = false;
            pnlRank5.Visible = false;
            pnlRank6.Visible = false;
            pnlRank7.Visible = false;

            //Textboxes
            txtRank2Points.Text = "";
            txtRank3Points.Text = "";
            txtRank4Points.Text = "";
            txtRank5Points.Text = "";
            txtRank6Points.Text = "";
            txtRank7Points.Text = "";

            txtRank2Title.Text = "";
            txtRank3Title.Text = "";
            txtRank4Title.Text = "";
            txtRank5Title.Text = "";
            txtRank6Title.Text = "";
            txtRank7Title.Text = "";
        }
    }
}