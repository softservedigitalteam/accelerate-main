
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsMissionKnowledgeMansView
/// </summary>
public partial class CMS_clsMissionKnowledgeMansView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMissionKnowledgeMansList;

    List<clsMissionKnowledgeMans> glstMissionKnowledgeMans;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtMissionKnowledgeMansList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstMissionKnowledgeMans"] == null)
            {
                glstMissionKnowledgeMans = new List<clsMissionKnowledgeMans>();
                Session["glstMissionKnowledgeMans"] = glstMissionKnowledgeMans;
            }
            else
                glstMissionKnowledgeMans = (List<clsMissionKnowledgeMans>)Session["glstMissionKnowledgeMans"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strMasterImage) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtMissionKnowledgeMansList = clsMissionKnowledgeMans.GetMissionKnowledgeMansList(FilterExpression, "");

        Session["dtMissionKnowledgeMansList"] = dtMissionKnowledgeMansList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMissionKnowledgeMansAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsMissionKnowledgeMansList object
        try
        {
            dtMissionKnowledgeMansList = new DataTable();

            if (Session["dtMissionKnowledgeMansList"] == null)
                dtMissionKnowledgeMansList = clsMissionKnowledgeMans.GetMissionKnowledgeMansList();
            else
                dtMissionKnowledgeMansList = (DataTable)Session["dtMissionKnowledgeMansList"];

            dgrGrid.DataSource = dtMissionKnowledgeMansList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MissionKnowledgeMansView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MissionKnowledgeMansView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MissionKnowledgeMansView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iMissionKnowledgeManID = int.Parse((sender as LinkButton).CommandArgument);

        clsMissionKnowledgeMans.Delete(iMissionKnowledgeManID);

        DataTable dtMissionKnowledgeMansList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A MISSION KNOWLEDGE MAN") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strMasterImage) LIKE '%" + txtSearch.Text + "%'";
            dtMissionKnowledgeMansList = clsMissionKnowledgeMans.GetMissionKnowledgeMansList(FilterExpression, "");
        }
        else
        {
            dtMissionKnowledgeMansList = clsMissionKnowledgeMans.GetMissionKnowledgeMansList();
        }

        Session["dtMissionKnowledgeMansList"] = dtMissionKnowledgeMansList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MissionKnowledgeMansView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMissionKnowledgeMansList = new DataTable();

        if (Session["dtMissionKnowledgeMansList"] == null)
            dtMissionKnowledgeMansList = clsMissionKnowledgeMans.GetMissionKnowledgeMansList();
        else
            dtMissionKnowledgeMansList = (DataTable)Session["dtMissionKnowledgeMansList"];

        DataView dvTemp = dtMissionKnowledgeMansList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMissionKnowledgeMansList = dvTemp.ToTable();
        Session["dtMissionKnowledgeMansList"] = dtMissionKnowledgeMansList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iMissionKnowledgeManID
            int iMissionKnowledgeManID = 0;
            iMissionKnowledgeManID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmMissionKnowledgeMansAddModify.aspx?action=edit&iMissionKnowledgeManID=" + iMissionKnowledgeManID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iMissionKnowledgeManID
             int iMissionKnowledgeManID = 0;
             iMissionKnowledgeManID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmMissionKnowledgeMansView.aspx?action=delete&iMissionKnowledgeManID=" + iMissionKnowledgeManID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region MissionKnowledgeMans FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtMissionKnowledgeMans = clsMissionKnowledgeMans.GetMissionKnowledgeMansList(FilterExpression, "");
        List<string> glstMissionKnowledgeMans = new List<string>();

        if (dtMissionKnowledgeMans.Rows.Count > 0)
        {
            foreach (DataRow dtrMissionKnowledgeMans in dtMissionKnowledgeMans.Rows)
            {
                glstMissionKnowledgeMans.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMissionKnowledgeMans["strMasterImage"].ToString(), dtrMissionKnowledgeMans["iMissionKnowledgeManID"].ToString()));
            }
        }
        else
            glstMissionKnowledgeMans.Add("No MissionKnowledgeMans Available.");
        strReturnList = glstMissionKnowledgeMans.ToArray();
        return strReturnList;
    }

    #endregion
}
