using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMissionQuestionsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMissionQuestions clsMissionQuestions;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popMission();

            //### If the iMissionQuestionID is passed through then we want to instantiate the object with that iMissionQuestionID
            if ((Request.QueryString["iMissionQuestionID"] != "") && (Request.QueryString["iMissionQuestionID"] != null))
            {
                clsMissionQuestions = new clsMissionQuestions(Convert.ToInt32(Request.QueryString["iMissionQuestionID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsMissionQuestions = new clsMissionQuestions();
            }
            Session["clsMissionQuestions"] = clsMissionQuestions;
        }
        else
        {
            clsMissionQuestions = (clsMissionQuestions)Session["clsMissionQuestions"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMissionQuestionsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;
        bool bAtLeastOneCB = true;
        bool bOnlyOneCB = true;

        bCanSave = clsValidation.IsNullOrEmpty(lstMission, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtQuestion, bCanSave);

       //if ((cbIsText.Checked) || (cbIsDropDownList.Checked) || (cbIsMultipleChoice.Checked) || (cbIsTeamStructure.Checked))
       //{
       //    if (((cbIsText.Checked) && (cbIsDropDownList.Checked)) || ((cbIsText.Checked) && (cbIsMultipleChoice.Checked)) || ((cbIsText.Checked) && (cbIsTeamStructure.Checked)) || ((cbIsDropDownList.Checked) && (cbIsMultipleChoice.Checked)) || ((cbIsDropDownList.Checked) && (cbIsTeamStructure.Checked)) || ((cbIsMultipleChoice.Checked) && (cbIsTeamStructure.Checked)))
       //    {
       //        bOnlyOneCB = false;
       //    }
       //    else
       //    {
       //        bOnlyOneCB = true;
       //    }

       //    bAtLeastOneCB = true;
       //}
       //else
       //{
       //    bAtLeastOneCB = false;
       //}

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Mission Question added successfully</div></div>";

            if ((bOnlyOneCB) && (bAtLeastOneCB))
            {
                SaveData();
            }
            else
            {
                if (!bAtLeastOneCB)
                {
                    mandatoryDiv.Visible = true;
                    lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please choose an answer type - Mission Question not added</div></div>";
                }
                else if (!bOnlyOneCB)
                {
                    mandatoryDiv.Visible = true;
                    lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please only choose one answer type - Mission Question not added</div></div>";
                }
            }
            
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Mission Question not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        lstMission.SelectedValue = "0";
        clsValidation.SetValid(lstMission);
        txtQuestion.Text = "";
        clsValidation.SetValid(txtQuestion);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        lstMission.SelectedValue = clsMissionQuestions.iMissionID.ToString();
        txtQuestion.Text = clsMissionQuestions.strQuestion;

        if (clsMissionQuestions.bIsText)
            cbIsText.Checked = true;
        //if (clsMissionQuestions.bIsDropDownList)
        //    cbIsDropDownList.Checked = true;
        //if (clsMissionQuestions.bIsMultipleChoice)
        //    cbIsMultipleChoice.Checked = true;
        //if (clsMissionQuestions.bIsTeamStructure)
        //    cbIsTeamStructure.Checked = true;
        clsMissionQuestions = new clsMissionQuestions(Convert.ToInt32(Request.QueryString["iMissionQuestionID"]));
        if (clsMissionQuestions.bIsCertificateQuestion == true)
        {
            chkIsCertificateQuestion.Checked = true;
        }
    }
    
    private void popMission()
    {
         DataTable dtMissionsList = new DataTable();
         lstMission.DataSource = clsMissions.GetMissionsList("iMissionTypeID=1", "");

         //### Populates the drop down list with PK and TITLE;
         lstMission.DataValueField = "iMissionID";
         lstMission.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstMission.DataBind();

         //### Add default select option;
         lstMission.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsMissionQuestions.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionQuestions.iAddedBy = clsUsers.iUserID;
        clsMissionQuestions.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionQuestions.iEditedBy = clsUsers.iUserID;
        clsMissionQuestions.iMissionID = Convert.ToInt32(lstMission.SelectedValue.ToString());
        clsMissionQuestions.strQuestion = txtQuestion.Text;

        clsMissionQuestions.bIsText = true;//cbIsText.Checked;
        //clsMissionQuestions.bIsDropDownList = cbIsDropDownList.Checked;
        //clsMissionQuestions.bIsMultipleChoice = cbIsMultipleChoice.Checked;
        //clsMissionQuestions.bIsTeamStructure = cbIsTeamStructure.Checked;
        clsMissionQuestions.bIsCertificateQuestion = chkIsCertificateQuestion.Checked;

        clsMissionQuestions.Update();
        Session["dtMissionQuestionsList"] = null;

        //### Go back to view page
        Response.Redirect("frmMissionQuestionsView.aspx");
    }

    #endregion
}