﻿//### Check max length
function SetMaxLength(txt,maxLen){
    if (txt.value.length > (maxLen-1)){
        alert("This field is limited to " + maxLen + " characters.\n" + "You have reached the limit.")
        txt.focus()
        txt.value = txt.value.substring(0, maxLen)
    }
} 

/************************************************************************************************************/
/************************************************************************************************************/

//### Email validator
function emailValidator(elem){
    var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if(elem.value.length > 0){
        if(elem.value.match(emailExp)){
            return true;
        }else{
            alert('Not a valid email address');
            elem.focus();
            return false;
        }
    }
}  

/************************************************************************************************************/
/************************************************************************************************************/

//### Check if caps lock is on
function checkCapsLock( e ) {
	var myKeyCode=0;
	var myShiftKey=false;
	var myMsg='Caps Lock is On.\n\nTo prevent entering your password incorrectly,\nyou should press Caps Lock to turn it off.';

	// Internet Explorer 4+
	if ( document.all ) {
		myKeyCode=e.keyCode;
		myShiftKey=e.shiftKey;

	// Netscape 4
	} else if ( document.layers ) {
		myKeyCode=e.which;
		myShiftKey=( myKeyCode == 16 ) ? true : false;

	// Netscape 6
	} else if ( document.getElementById ) {
		myKeyCode=e.which;
		myShiftKey=( myKeyCode == 16 ) ? true : false;
	}

	// Upper case letters are seen without depressing the Shift key, therefore Caps Lock is on
	if ( ( myKeyCode >= 65 && myKeyCode <= 90 ) && !myShiftKey ) {
		alert( myMsg );

	// Lower case letters are seen while depressing the Shift key, therefore Caps Lock is on
	} else if ( ( myKeyCode >= 97 && myKeyCode <= 122 ) && myShiftKey ) {
		alert( myMsg );
	}
}

/************************************************************************************************************/
/************************************************************************************************************/

//### Field Limitor
function textCounter(field,counter,maxlimit,linecounter){

	//### Text width
	var fieldWidth =  parseInt(field.offsetWidth);
	var charcnt = field.value.length;        

	//### Ttrim the extra text
	if (charcnt > maxlimit) { 
		field.value = field.value.substring(0, maxlimit);
	}
	else 
	{ 
	    //### Progress bar percentage
	    var percentage = parseInt(100 - (( maxlimit - charcnt) * 100)/maxlimit) ;
	    document.getElementById(counter).style.width =  parseInt((fieldWidth*percentage)/100)+"px";
	    document.getElementById(counter).innerHTML="Limit: "+percentage+"%"
	    
	    //### Color correction on style from CCFFF -> CC0000
	    setcolor(document.getElementById(counter),percentage,"background-color");
	}
}

function setcolor(obj,percentage,prop){
	obj.style[prop] = "rgb(80%,"+(100-percentage)+"%,"+(100-percentage)+"%)";
}

/************************************************************************************************************/
/************************************************************************************************************/

//### Confirm delete
function jsDeleteConfirm() {
    return confirm('You are about to delete this item.\r\nDo you want to continue?');
}

/************************************************************************************************************/
/************************************************************************************************************/

function validateEmail(target) {
    if (target.value != '' || target.value != null) {
        var emailExpression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (target.value.length > 0) {
            if (target.value.match(emailExpression)) {
                divImageHolder.className = 'validationImageValid';
            }
            else {
                divImageHolder.className = 'validationImageValid';
            }
        }
        else {
            divImageHolder.className = 'validationImageValid';
        }
    }
    else {
        divImageHolder.className = 'validationImageValid';
    }
}

/************************************************************************************************************/
/************************************************************************************************************/

//### Ensure value is a number
function jsIsNumeric(myVal) {
    if (isNaN(myVal.value)) {
        alert("This field should be a numeric value only.")
        setTimeout(function () { document.getElementById(myVal.id).focus(); }, 1);
        return false;
    }

    return true;
}

//### Ensure value is a number
function jsIsInteger(myVal) {
    if (isNaN(myVal.value) || (myVal.value.indexOf('.') != -1)) {
        alert("This field should be a numeric non-decimal value.")
        setTimeout(function () { document.getElementById(myVal.id).focus(); }, 1);
        return false;
    }
    return true;
}

/************************************************************************************************************/
/************************************************************************************************************/

//### Set the image Class to Valid or invalid
function setValid(obj, invalidValue) {
    if (invalidValue == undefined) {
        if (obj.value != "") {
            obj.parentElement.parentElement.children[0].children[0].className = "validationImageValid";
        } else {
            obj.parentElement.parentElement.children[0].children[0].className = "validationImageMandatory";
        }
    } else {
        if (obj.value != invalidValue) {
            obj.parentElement.parentElement.children[0].children[0].className = "validationImageValid";
        } else {
            obj.parentElement.parentElement.children[0].children[0].className = "validationImageMandatory";
        }
    }
}

function setValidFile(obj, bValue) {
    if (bValue) {
        document.getElementById(obj).parentElement.parentElement.children[0].children[0].className = "validationImageValid";
    } else {
        document.getElementById(obj).parentElement.parentElement.children[0].children[0].className = "validationImageMandatory";
    }
}

function setTimeValid(obj) {
    if (obj.value != "") {
        obj.parentElement.parentElement.parentElement.children[0].children[0].className = "validationImageValid";
    } else {
        obj.parentElement.parentElement.parentElement.children[0].children[0].className = "validationImageMandatory";
    }
}

function setValidEditor(obj, bValue) {
    if (bValue) {
        document.getElementById(obj).className = "validationImageValid";
    } else {
        document.getElementById(obj).className = "validationImageMandatory";
    }
}

function setAllGreen() {
    $(document).ready(function () { $('.validationImageMandatory').removeClass('validationImageMandatory').addClass('validationImageValid') });
}

/************************************************************************************************************/
/**********************************************

function hideShow() {
    if (document.getElementById(BespokeTime).style.display == 'none') {
        document.getElementById(BespokeTime).style.display = 'block';
    }
}
**************************************************************/