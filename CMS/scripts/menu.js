function toggleMenuItem(menuTriggerItemID, menuContentItemID) {

    if (document.getElementById(menuContentItemID).style.display == 'none' || document.getElementById(menuTriggerItemID).className == 'arrowLeft') {
        document.getElementById(menuTriggerItemID).className = 'arrowDown';
    }
    else {
        document.getElementById(menuTriggerItemID).className = 'arrowLeft';
    }

    $('#' + menuContentItemID).slideToggle('slow');
}