﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

public partial class CMS_frmUsersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAccountUsers clsAccountUsersRecord;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
            //### If the iUserID is passed through then we want to instantiate the obect with that iUserID
            if (Request.QueryString["iUserID"] != "" && Request.QueryString["iUserID"] != null)
            {
                clsAccountUsersRecord = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iUserID"]));
            }
            else
            {
                clsAccountUsersRecord = new clsAccountUsers();
            }
            Session["clsAccountUsersRecord"] = clsAccountUsersRecord;
        }
        else
        {
            clsAccountUsersRecord = (clsAccountUsers)Session["clsAccountUsersRecord"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmAccountUsersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtPassword);
        bCanSave = clsValidation.IsNullOrEmpty(txtConfirmPassword);

        if (bCanSave == true)
        {
            //lblValidationMessage.Text = "<div><img src=\"images/Validation/imgFaceHappy.png\" alt=\"\" title=\"\"/>USER ADDED SUCCESSFULLY</div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div style=\"padding:15px 0px 0px 13px\"><img src=\"images/Validation/imgFaceSad.png\" alt=\"\" title=\"\"/><div class=\"validationMessage\">Please fill out all mandatory fields - USER NOT ADDED</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtPassword.Text = "";
        clsValidation.SetValid(txtPassword);
        //txtPassword.CssClass = "roundedCornerTextBox";

        txtConfirmPassword.Text = "";
        clsValidation.SetValid(txtConfirmPassword);
        //txtConfirmPassword.CssClass["roundedCornerTextBox"];
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsAccountUsersRecord.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsAccountUsersRecord.iEditedBy = clsUsers.iUserID;

        //### Only update the password if there is a value
        if (txtPassword.Text != "")
        {    
            //### Hash the password
            string strHashPassword = clsCommonFunctions.GetMd5Sum(txtPassword.Text);
            clsAccountUsersRecord.strPassword = strHashPassword;
        }

        clsAccountUsersRecord.bIsDeleted = true;

        clsAccountUsersRecord.Update();

        //### redirect back to view page
        Response.Redirect("frmAccountUsersView.aspx");
    }

    #endregion
}