using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class CMS_frmCustomColoursAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCustomColours clsCustomColours;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iCustomColourID is passed through then we want to instantiate the object with that iCustomColourID
            if ((Request.QueryString["iCustomColourID"] != "") && (Request.QueryString["iCustomColourID"] != null))
            {
                clsCustomColours = new clsCustomColours(Convert.ToInt32(Request.QueryString["iCustomColourID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCustomColours = new clsCustomColours();
            }
            Session["clsCustomColours"] = clsCustomColours;
        }
        else
        {
            clsCustomColours = (clsCustomColours)Session["clsCustomColours"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCustomColoursView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;
        bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtProfileHeader, bCanSave); 
        bCanSave = clsValidation.IsNullOrEmpty(txtTopBarBG, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtCurrentRank, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtNextRank, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtAchievementText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTopProgressBar, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTopBarText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTopBarHoverBG, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTopBarHoverText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSideBarHeaders, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSideBarHeaderText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSideBarSubAreas, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSideBarSubAreaText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtNotificationAreaBorder, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtNotificationSubText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtNotificationHeaderText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtMissionProgressBar, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPageHeaderText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPhaseHeaderBG, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPhaseHeaderText, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtCongratulationsBackground, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtCongratulationsAchievementImageContainer, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtUserName, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtMissionHeader, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtAchievementRankHeader, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtAchievementRank, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmailHeaderBanner, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">CustomColour added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - CustomColour not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTopBarBG.Text = "";
        clsValidation.SetValid(txtTopBarBG);
        txtTopBarText.Text = "";
        clsValidation.SetValid(txtTopBarText);
        txtSideBarHeaders.Text = "";
        clsValidation.SetValid(txtSideBarHeaders);
        txtSideBarHeaderText.Text = "";
        clsValidation.SetValid(txtSideBarHeaderText);
        txtSideBarSubAreas.Text = "";
        clsValidation.SetValid(txtSideBarSubAreas);
        txtSideBarSubAreaText.Text = "";
        clsValidation.SetValid(txtSideBarSubAreaText);
        txtMissionProgressBar.Text = "";
        clsValidation.SetValid(txtMissionProgressBar);
        txtPageHeaderText.Text = "";
        clsValidation.SetValid(txtPageHeaderText);
        txtPhaseHeaderBG.Text = "";
        clsValidation.SetValid(txtPhaseHeaderBG);
        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtLink.Text = "";
        clsValidation.SetValid(txtLink);
        txtAdditional3.Text = "";
        clsValidation.SetValid(txtAdditional3);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtUserName.Text = clsCustomColours.strUserNameColour;
        txtProfileHeader.Text = clsCustomColours.strProfileHeaderSection;
        txtTopBarBG.Text = clsCustomColours.strTopBarBG;
        txtTopBarText.Text = clsCustomColours.strTopBarText;
        txtCurrentRank.Text = clsCustomColours.strCurrentRank;
        txtNextRank.Text = clsCustomColours.strNextRank;
        txtMissionHeader.Text = clsCustomColours.strMissionHeaderText;
        txtAchievementText.Text = clsCustomColours.strAchievementText;
        txtTopProgressBar.Text = clsCustomColours.strTopProgressBar;
        txtTopBarHoverBG.Text = clsCustomColours.strTopBarHoverBG;
        txtTopBarHoverText.Text = clsCustomColours.strTopBarHoverText;
        txtSideBarHeaders.Text = clsCustomColours.strSideBarHeaders;
        txtSideBarHeaderText.Text = clsCustomColours.strSideBarHeaderText;
        txtSideBarSubAreas.Text = clsCustomColours.strSideBarSubAreas;
        txtSideBarSubAreaText.Text = clsCustomColours.strSideBarSubAreaText;
        txtAchievementRankHeader.Text = clsCustomColours.strAchievementRankHeaderText;
        txtAchievementRank.Text = clsCustomColours.strAchievementRankText;
        txtEmailHeaderBanner.Text = clsCustomColours.strEmailHeaderBanner;
        txtNotificationAreaBorder.Text = clsCustomColours.strNotificationAreaBorder;
        txtNotificationHeaderText.Text = clsCustomColours.strNotificationHeaderText;
        txtNotificationSubText.Text = clsCustomColours.strNotificationSubText;
        txtMissionProgressBar.Text = clsCustomColours.strMissionProgressBar;
        txtPageHeaderText.Text = clsCustomColours.strPageHeaderText;
        txtPhaseHeaderBG.Text = clsCustomColours.strPhaseHeaderBG;
        txtPhaseHeaderText.Text = clsCustomColours.strPhaseHeaderText;
        txtTitle.Text = clsCustomColours.strTitle;
        txtLink.Text = clsCustomColours.strLink;
        txtCongratulationsBackground.Text = clsCustomColours.strCongratulationsBackground;
        txtCongratulationsAchievementImageContainer.Text = clsCustomColours.strAchievementImageContainer;
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsCustomColours.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomColours.iAddedBy = clsUsers.iUserID;
        clsCustomColours.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomColours.iEditedBy = clsUsers.iUserID;
        clsCustomColours.strUserNameColour = txtUserName.Text;
        clsCustomColours.strProfileHeaderSection = txtProfileHeader.Text;
        clsCustomColours.strCurrentRank = txtCurrentRank.Text;
        clsCustomColours.strNextRank = txtNextRank.Text;
        clsCustomColours.strAchievementText = txtAchievementText.Text;
        clsCustomColours.strTopProgressBar = txtTopProgressBar.Text;
        clsCustomColours.strTopBarBG = txtTopBarBG.Text;
        clsCustomColours.strTopBarText = txtTopBarText.Text;
        clsCustomColours.strTopBarHoverBG = txtTopBarHoverBG.Text;
        clsCustomColours.strTopBarHoverText = txtTopBarHoverText.Text;
        clsCustomColours.strMissionHeaderText = txtMissionHeader.Text;
        clsCustomColours.strAchievementRankHeaderText = txtAchievementRankHeader.Text;
        clsCustomColours.strAchievementRankText = txtAchievementRank.Text;
        clsCustomColours.strSideBarHeaders = txtSideBarHeaders.Text;
        clsCustomColours.strSideBarHeaderText = txtSideBarHeaderText.Text;
        clsCustomColours.strSideBarSubAreas = txtSideBarSubAreas.Text;
        clsCustomColours.strSideBarSubAreaText = txtSideBarSubAreaText.Text;
        clsCustomColours.strNotificationAreaBorder = txtNotificationAreaBorder.Text;
        clsCustomColours.strNotificationHeaderText = txtNotificationHeaderText.Text;
        clsCustomColours.strNotificationSubText = txtNotificationSubText.Text;
        clsCustomColours.strMissionProgressBar = txtMissionProgressBar.Text;
        clsCustomColours.strPageHeaderText = txtPageHeaderText.Text;
        clsCustomColours.strPhaseHeaderBG = txtPhaseHeaderBG.Text;
        clsCustomColours.strPhaseHeaderText = txtPhaseHeaderText.Text;
        clsCustomColours.strTitle = txtTitle.Text;
        clsCustomColours.strEmailHeaderBanner = txtEmailHeaderBanner.Text;
        clsCustomColours.strCongratulationsBackground = txtCongratulationsBackground.Text;
        clsCustomColours.strAchievementImageContainer = txtCongratulationsAchievementImageContainer.Text;

        clsCustomColours.strLink = createCSSFileLink();
        //DocumentUpload.PostedFile.SaveAs(strSaveDirectory);


        clsCustomColours.Update();

        Session["dtCustomColoursList"] = null;

        //### Go back to view page
        Response.Redirect("frmCustomColoursView.aspx");
    }

    private string createCSSFileLink()
    {
        string strFileDirectory = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\CustomColourSchemes";

        if (!System.IO.Directory.Exists(strFileDirectory))
            System.IO.Directory.CreateDirectory(strFileDirectory);

        string strFullFilePath = strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory) + "\\" + txtTitle.Text + ".css";

        if (File.Exists(strFullFilePath))
            File.Delete(strFullFilePath);

        string strDirectoryForStyle = strFullFilePath.Replace(strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory) + "\\" + txtTitle.Text + ".css", strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory));

        if (!System.IO.Directory.Exists(strDirectoryForStyle))
            System.IO.Directory.CreateDirectory(strDirectoryForStyle);

        using (FileStream fstream = new FileStream(strFullFilePath, FileMode.Create))
        {
            using (StreamWriter sWriter = new StreamWriter(fstream, new ASCIIEncoding()))
            {
                sWriter.Write(createCSSFile());
            }
        }

        /*DocumentUpload.PostedFile.SaveAs(strUniqueDocumentFullPath + "\\" + strSaveLocation);*/
        strFullFilePath = strFullFilePath.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "");

        return strFullFilePath.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"], "");
    }

    private string createCSSFile()
    {
        StringBuilder strbCssFileContent = new StringBuilder();

        strbCssFileContent.AppendLine(".genericHeadingDark{background: " + txtSideBarHeaders.Text + ";color: " + txtSideBarHeaderText.Text + ";}");
        strbCssFileContent.AppendLine(".rightMenuContainer{background: " + txtSideBarSubAreas.Text + " url('http://www.accelerate.client-demo.co.za/images/imgRightPanel_bg.png') top left no-repeat; color: " + txtSideBarHeaderText.Text + ";}");
        strbCssFileContent.AppendLine(".percentageBlue{background-color: " + txtMissionProgressBar.Text + ";}");
        strbCssFileContent.AppendLine(".nav-collapse a{background: " + txtTopBarBG.Text +  " !important;}");
        strbCssFileContent.AppendLine(".nav-collapse a:hover{background: " + txtTopBarHoverBG.Text + " !important;}"); //" + txtTopBarBG.Text + "
        strbCssFileContent.AppendLine(".nav-collapse a{color: " + txtTopBarText.Text + " !important;}");
        strbCssFileContent.AppendLine(".nav-collapse a:hover{color: " + txtTopBarHoverText.Text + " !important;}");
        strbCssFileContent.AppendLine(".homeHeaderContainer{background: " + txtProfileHeader.Text + ";}");
        strbCssFileContent.AppendLine(".arrow-down{border-top: 20px solid " + txtProfileHeader.Text + ";}");
        strbCssFileContent.AppendLine(".NotificationsItemContainer{border-bottom: 2px solid " + txtNotificationAreaBorder.Text + ";}");
        strbCssFileContent.AppendLine(".rightMenuContainer{color: " + txtNotificationHeaderText.Text + ";}");
        strbCssFileContent.AppendLine(".NotificationsMessage{color: " + txtNotificationSubText.Text + ";}");
        strbCssFileContent.AppendLine(".linkWhite{color: " + txtSideBarSubAreaText.Text + " !important;}");
        strbCssFileContent.AppendLine(".achieverDetails { color: " + txtSideBarSubAreaText.Text + " !important;}");
        strbCssFileContent.AppendLine(".textField{ color: " + txtSideBarSubAreaText.Text + " !important;}");
        strbCssFileContent.AppendLine(".genericMainHeading{background-color: " + txtPhaseHeaderBG.Text + ";}");
        strbCssFileContent.AppendLine(".genericMainHeading h2{color: " + txtPageHeaderText.Text + ";}");
        strbCssFileContent.AppendLine(".grid figcaption{background: " + txtPhaseHeaderBG.Text + ";}");
        strbCssFileContent.AppendLine(".grid figcaption h3{color: " + txtPhaseHeaderText.Text + ";}");
        strbCssFileContent.AppendLine(".missionHeader{background-color: " + txtPhaseHeaderBG.Text +  ";}");
        strbCssFileContent.AppendLine(".homeAchievementImageContainer{color: " + txtAchievementText.Text + ";}");
        strbCssFileContent.AppendLine(".green{color: " + txtCurrentRank.Text + " !important;}");
        strbCssFileContent.AppendLine(".orange{color: " + txtNextRank.Text + ";}");
        strbCssFileContent.AppendLine(".progress-bar{background-color: " + txtTopProgressBar.Text + ";}");
        strbCssFileContent.AppendLine(".missionHeading{color: " + txtMissionHeader.Text + ";}");
        strbCssFileContent.AppendLine(".ProfileHead{color: " + txtUserName.Text + " !important;}");
        strbCssFileContent.AppendLine(".ovalAchiementStage{color : " + txtAchievementRankHeader.Text + ";}");
        strbCssFileContent.AppendLine(".ovalAchiementStage span{color : " + txtAchievementRank.Text + ";}");
        strbCssFileContent.AppendLine(".emailHeaderBanner{background-color: " + txtEmailHeaderBanner.Text + ";}");
        strbCssFileContent.AppendLine(".roundedCornerBox{background-color: " + txtCongratulationsAchievementImageContainer.Text + " !important;}");
        strbCssFileContent.AppendLine(".completeMissionInner{background: " + txtCongratulationsBackground.Text + " !important;}");

        return strbCssFileContent.ToString();
    }

    private string GetUniqueDocumentPath(string strFileDirectory)
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strFileDirectory + "\\Scheme" + iCount) == true)
        {
            iCount++;
        }
        return "Scheme" + iCount;

    }
    #endregion
}