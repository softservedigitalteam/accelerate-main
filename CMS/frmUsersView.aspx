﻿<%@ Page Title="CMS Users" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmUsersView.aspx.cs" Inherits="CMS_frmUsersView" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script lang="javascript" type="text/javascript">
        //### Password check
        function clearText() {
                document.getElementById("<%=txtSearch.ClientID%>").value = ""
        }
    </script>

    <script type="text/javascript">
        function RefreshUpdatePanel(sender) {
            __doPostBack(sender, '');
        }
    </script> 
     
    <!-- Event Manager for selection of item from auto-complete list -->
    <script lang="javascript" type="text/javascript">
        function ClientItemSelected(sender, e) {
            $get("<%=hfUserID.ClientID %>").value = e.get_value();
            $get('<%=lnkbtnSearch.ClientID %>').click();
        }

        function toggleDiv(divID) {
            $(divID).slideToggle();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Panel ID="pSearch" runat="server" DefaultButton="lnkbtnSearch">
        <div class="controlDiv" style="margin-top:15px;">
            <div class="fieldSearchDiv">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="roundedCornerTextBox" Text="SEARCH FOR A USER" onfocus="clearText();"/>
                <ajax:AutoCompleteExtender ID="aceUsers" runat="server" TargetControlID="txtSearch" ServiceMethod="FilterBusinessRule" 
                    EnableCaching="true" MinimumPrefixLength="1" CompletionInterval="0" CompletionSetCount="3" 
                    CompletionListCssClass="popupCompletionList" CompletionListItemCssClass="popupCompletionListItem" CompletionListHighlightedItemCssClass="popupCompletionListItemHighlight"
                    OnClientItemSelected="ClientItemSelected" FirstRowSelected="true" />
                <asp:HiddenField ID="hfUserID" runat="server" Value="" />
            </div>
            <div class="buttonsSearchDiv"><asp:LinkButton ID="lnkbtnSearch" runat="server" CssClass="searchButton" onclick="lnkbtnSearch_Click" /></div>
        </div>
        <br class="clearingSpacer"/>
    </asp:Panel>

    <br />

    <asp:UpdatePanel ID="udpUsersView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:DataGrid 
                ID="dgrGrid" Runat="server" AutoGenerateColumns="False" AllowSorting="true" 
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrGrid_PageIndexChanged" 
                OnSortCommand="dgrGrid_SortCommand" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev"
                HeaderStyle-CssClass="dgrHeader"  ItemStyle-CssClass="dgrItem" AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iUserID" Visible="false" />
            
                    <asp:BoundColumn DataField="strFirstName" SortExpression="strFirstName" HeaderText="First Name" HeaderStyle-CssClass="dgrHeaderLeft" HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strSurname" SortExpression="strSurname" HeaderText="Surname" HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strEmailAddress" HeaderText="Email" HeaderStyle-HorizontalAlign="Left"/>

                    <asp:TemplateColumn HeaderText="Edit Password" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEditPass" runat="Server" CssClass="dgrLinkEditPassword" OnDataBinding="dgrGrid_PopEditPassLink" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEditItem" runat="Server" CssClass="dgrLinkEdit" OnDataBinding="dgrGrid_PopEditLink" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"  HeaderStyle-CssClass="dgrHeaderRight" >
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDeleteItem" runat="server" CssClass="dgrLinkDelete" CommandArgument='<%# Eval("iUserID") %>' OnClick="lnkDeleteItem_Click" OnClientClick="return jsDeleteConfirm()"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>            
                </Columns> 
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnAdd" runat="server" CssClass="addButton" onclick="lnkbtnAdd_Click" />
    </div>

</asp:Content>