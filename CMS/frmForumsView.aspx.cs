
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsForumsView
/// </summary>
public partial class CMS_clsForumsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtForumsList;

    List<clsForums> glstForums;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtForumsList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstForums"] == null)
            {
                glstForums = new List<clsForums>();
                Session["glstForums"] = glstForums;
            }
            else
                glstForums = (List<clsForums>)Session["glstForums"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="() LIKE '%" + txtSearch.Text + "%'";
        DataTable dtForumsList = clsForums.GetForumsList(FilterExpression, "");

        Session["dtForumsList"] = dtForumsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmForumsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsForumsList object
        try
        {
            dtForumsList = new DataTable();

            if (Session["dtForumsList"] == null)
                dtForumsList = clsForums.GetForumsList();
            else
                dtForumsList = (DataTable)Session["dtForumsList"];

            dgrGrid.DataSource = dtForumsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["ForumsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ForumsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["ForumsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iForumID = int.Parse((sender as LinkButton).CommandArgument);

        clsForums.Delete(iForumID);

        DataTable dtForumCommentsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A FORUM") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "() LIKE '%" + txtSearch.Text + "%'";
            DataTable dtForumsList = clsForums.GetForumsList(FilterExpression, "");
        }
        else
        {
            dtForumsList = clsForums.GetForumsList();
        }

        Session["dtForumsList"] = dtForumsList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ForumsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtForumsList = new DataTable();

        if (Session["dtForumsList"] == null)
            dtForumsList = clsForums.GetForumsList();
        else
            dtForumsList = (DataTable)Session["dtForumsList"];

        DataView dvTemp = dtForumsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtForumsList = dvTemp.ToTable();
        Session["dtForumsList"] = dtForumsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iForumID
            int iForumID = 0;
            iForumID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmForumsAddModify.aspx?action=edit&iForumID=" + iForumID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iForumID
             int iForumID = 0;
             iForumID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmForumsView.aspx?action=delete&iForumID=" + iForumID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Forums FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "() LIKE '%" + prefixText + "%'";
        DataTable dtForums = clsForums.GetForumsList(FilterExpression, "");
        List<string> glstForums = new List<string>();

        if (dtForums.Rows.Count > 0)
        {
            foreach (DataRow dtrForums in dtForums.Rows)
            {
                glstForums.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dtrForums["iForumID"].ToString(), dtrForums["iForumID"].ToString()));
            }
        }
        else
            glstForums.Add("No Forums Available.");
        strReturnList = glstForums.ToArray();
        return strReturnList;
    }

    #endregion
}
