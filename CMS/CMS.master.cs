﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMS_CMS : System.Web.UI.MasterPage
{
    private clsUsers clsUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers"];
        
        //### Current page
        lblCurrentPage.Text = Page.Title.ToString();

        //### Set the time
        lblDateTime.Text = DateTime.Now.ToString("hh:mm:tt") + "&nbsp;|&nbsp;" + DateTime.Now.ToString("dd MMM yyyy");

        //### Populates the Welcome Message
        string strFullName = clsUsers.strFirstName + " " + clsUsers.strSurname;
        lblMessage.Text = "Hello " + strFullName + ""; 
    }

    #endregion
}
