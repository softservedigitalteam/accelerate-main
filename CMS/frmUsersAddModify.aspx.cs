﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMS_frmUsersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsUsers clsUsersRecord;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
            //### If the iUserID is passed through then we want to instantiate the obect with that iUserID
            if (Request.QueryString["iUserID"] != "" && Request.QueryString["iUserID"] != null)
            {
                clsUsersRecord = new clsUsers(Convert.ToInt32(Request.QueryString["iUserID"]));

                //### Populate the form
                popFormData();

                popValidFields();

                //### Hide password rows if editing
                if (Request.QueryString["action"] == "edit")
                {
                    divPassword.Attributes.Add("style", "display:none");
                    divConfirmPassword.Attributes.Add("style", "display:none");
                }
            }
            else
            {
                clsUsersRecord = new clsUsers();
            }

            Session["clsUsersRecord"] = clsUsersRecord;
        }
        else
        {
            clsUsersRecord = (clsUsers)Session["clsUsersRecord"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmUsersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtName);
        bCanSave = clsValidation.IsNullOrEmpty(txtSurname);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmail);

        //### Hide password rows if editing
        if (Request.QueryString["action"] != "edit")
        {
            bCanSave = clsValidation.IsNullOrEmpty(txtPassword);
            bCanSave = clsValidation.IsNullOrEmpty(txtConfirmPassword);
        }

        if ((bCanSave == true) && ((Convert.ToBoolean(hfCanSave.Value)) == true))
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt=\"\" title=\"\"/><div class=\"validationMessage\">USER ADDED SUCCESSFULLY</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt=\"\" title=\"\"/><div class=\"validationMessage\">Please fill out all mandatory fields - USER NOT ADDED</div></div>";

            popValidFields();
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtName.Text = "";

        clsValidation.SetValid(txtName);

        txtSurname.Text = "";
        clsValidation.SetValid(txtSurname);

        txtEmail.Text = "";
        clsValidation.SetValid(txtEmail);

        txtPassword.Text = "";
        clsValidation.SetValid(txtPassword);

        txtConfirmPassword.Text = "";
        clsValidation.SetValid(txtConfirmPassword);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtName.Text = clsUsersRecord.strFirstName;
        txtSurname.Text = clsUsersRecord.strSurname;
        txtEmail.Text = clsUsersRecord.strEmailAddress;
        hfCanSave.Value = "true";
    }

    private void popValidFields()
    {
        //### Make the relevant mandatory fields green
        string strScript = "";

        if (!String.IsNullOrEmpty(txtName.Text))
        {
            strScript += "setValidFile('" + txtName.ClientID + "',true);";
        }
        if (!String.IsNullOrEmpty(txtSurname.Text))
        {
            strScript += "setValidFile('" + txtSurname.ClientID + "',true);";
        }
        if (!String.IsNullOrEmpty(txtEmail.Text))
        {
            strScript += "setValidFile('" + txtEmail.ClientID + "',true);";
        }
        if (!String.IsNullOrEmpty(txtPassword.Text))
        {
            strScript += "setValidFile('" + txtPassword.ClientID + "',true);";
        }
        if (!String.IsNullOrEmpty(txtConfirmPassword.Text))
        {
            strScript += "setValidFile('" + txtConfirmPassword.ClientID + "',true);";
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "mandatoryFields", strScript + strScript, true);
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Only update the password if there is a value
        if (txtPassword.Text != "")
        {   
            //### Hash the password
            string strHashPassword = clsCommonFunctions.GetMd5Sum(txtPassword.Text);
            clsUsersRecord.strPassword = strHashPassword;
        }

        //### Add / Update
        clsUsersRecord.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsUsersRecord.iAddedBy = clsUsers.iUserID;
        clsUsersRecord.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsUsersRecord.iEditedBy = clsUsers.iUserID;
        clsUsersRecord.strFirstName = txtName.Text;
        clsUsersRecord.strSurname = txtSurname.Text;
        clsUsersRecord.strEmailAddress = txtEmail.Text.ToLower();

        clsUsersRecord.Update();

        //### redirect back to view page
        Response.Redirect("frmUsersView.aspx");
    }

    #endregion
}