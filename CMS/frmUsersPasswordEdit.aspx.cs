﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

public partial class CMS_frmUsersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsUsers clsUsersRecord;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }

        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
            //### If the iUserID is passed through then we want to instantiate the obect with that iUserID
            if (Request.QueryString["iUserID"] != "" && Request.QueryString["iUserID"] != null)
            {
                clsUsersRecord = new clsUsers(Convert.ToInt32(Request.QueryString["iUserID"]));
            }
            else
            {
                clsUsersRecord = new clsUsers();
            }
            Session["clsUsersRecord"] = clsUsersRecord;
        }
        else
        {
            clsUsersRecord = (clsUsers)Session["clsUsersRecord"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmUsersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtPassword);
        bCanSave = clsValidation.IsNullOrEmpty(txtConfirmPassword);

        if (bCanSave == true)
        {
            //lblValidationMessage.Text = "<div><img src=\"images/Validation/imgFaceHappy.png\" alt=\"\" title=\"\"/>USER ADDED SUCCESSFULLY</div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div style=\"padding:15px 0px 0px 13px\"><img src=\"images/Validation/imgFaceSad.png\" alt=\"\" title=\"\"/><div class=\"validationMessage\">Please fill out all mandatory fields - USER NOT ADDED</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtPassword.Text = "";
        clsValidation.SetValid(txtPassword);
        //txtPassword.CssClass = "roundedCornerTextBox";

        txtConfirmPassword.Text = "";
        clsValidation.SetValid(txtConfirmPassword);
        //txtConfirmPassword.CssClass["roundedCornerTextBox"];
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsUsersRecord.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsUsersRecord.iEditedBy = clsUsers.iUserID;

        //### Only update the password if there is a value
        if (txtPassword.Text != "")
        {    
            //### Hash the password
            string strHashPassword = clsCommonFunctions.GetMd5Sum(txtPassword.Text);
            clsUsersRecord.strPassword = strHashPassword;
        }

        clsUsersRecord.bIsDeleted = true;

        clsUsersRecord.Update();

        //### redirect back to view page
        Response.Redirect("frmUsersView.aspx");
    }

    #endregion
}