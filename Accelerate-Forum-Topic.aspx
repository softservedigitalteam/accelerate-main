﻿<%@ Page Title="Accelerate-Forum-Topic" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" CodeFile="Accelerate-Forum-Topic.aspx.cs" Inherits="Accelerate_Forum_Topic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" ID="updateForum">
        <ContentTemplate>
            <div class="infobox">
                <div class="top-bar-alternative">
                    <h3>Forum Topic -
                (<asp:Label ID="lblForumTitle" runat="server"></asp:Label>)</h3>
                </div>
                <div class="well">
                    <asp:Literal ID="litForumTopicComments" runat="server"></asp:Literal>
                </div>
                <div class="top-bar-alternative">
                    <h3>Add your comment</h3>
                </div>
                <div class="well-short">

                    <asp:TextBox ID="txtComment" runat="server" Placeholder="Enter your comment here..." CssClass="textboxAlt" TextMode="MultiLine" Style="min-height: 60px;"></asp:TextBox><br />
                    <asp:Button ID="btnAddForumTopic" runat="server" Text="Add a comment" CssClass="Green3 btn" OnClick="btnAddForumTopic_Click" /><br />
                    <br />
                </div>
            </div>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; height: 100%; width: 100%; overflow: show; margin: auto; top: 0; left: 0; bottom: 0; right: 0; z-index: 9999999; background-color: #fff; opacity: 0.8;">
                        <center><asp:Image ID="imgUpdateProgress3" runat="server" ImageUrl="img/Loader.gif" AlternateText="Loading ..." ToolTip="Loading ..." style="opacity:1;position:fixed;top: 43%;"/></center>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAddForumTopic" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

