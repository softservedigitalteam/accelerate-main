﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using SD = System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing;

public partial class Accelerate_Profile_Mission : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsAccountUserTrackings clsTracking;

    //clsMissions clsMissions;
    bool bIsDefault;
    bool bHasClosed;
    bool bIsCropped;

    public DataTable dtPhase1Missions;
    public DataTable dtPhase2Missions;
    public DataTable dtPhase3Missions;
    int iNextMissionID = 0;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        bool bHasDeleted = false;
        bIsCropped = false;

        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        btnFileUpload.Value = "Upload" + Environment.NewLine + " (Max 2Mbs)";

        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }

        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        try
        {
            clsTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        if (clsAccountUsers.bHasCompletedProfileMission)
            Response.Redirect("Accelerate-Home.aspx");
        try
        {
            popDefaultImages();
            popCurrentMissionBanner(1);
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        if (!IsPostBack)
        {
        }
    }

    #endregion

    #region POPULATION METHODS

    protected void popDefaultImages()
    {
        getDefaultImagesList();
    }

    protected void popCurrentMissionBanner(int iCurrentMissionID)
    {
        string strFullPathForMissionImage = "";
        clsMissions clsMissions = new clsMissions(iCurrentMissionID);

        if (clsMissions.iMissionVideoID != 0)
        {
            DataTable dtMissionVideos = clsMissionVideos.GetMissionVideosList("iMissionVideoID = " + clsMissions.iMissionVideoID, "");

            foreach (DataRow dr in dtMissionVideos.Rows)
            {
                strFullPathForMissionImage = "Missions/" + dr["strPathToImages"] + "/" + Path.GetFileNameWithoutExtension(dr["strMasterImage"].ToString()) + "" + Path.GetExtension(dr["strMasterImage"].ToString());
            }
        }
        else
        {
            strFullPathForMissionImage = "Missions/" + clsMissions.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsMissions.strMasterImage) + "" + Path.GetExtension(clsMissions.strMasterImage);
        }

        StringBuilder strbCurrentMission = new StringBuilder();

        strbCurrentMission.AppendLine("<div class='row-fluid'>");
        strbCurrentMission.AppendLine("<div class='span2'><center>");
        strbCurrentMission.AppendLine("<img src='" + strFullPathForMissionImage + "' width='60%;' align='center' />");
        strbCurrentMission.AppendLine("</center></div>");
        strbCurrentMission.AppendLine("<div class='span8' style='font-size: 12px; color: #000;'>");
        strbCurrentMission.AppendLine(clsMissions.strDescription.Replace("/n", "<br />"));
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("<div class='span2'>");
        strbCurrentMission.AppendLine("<span style='font-size: 3em; color: #FFF; line-height: 1em; font-weight: 100;'>" + clsMissions.iBonusPoints + "</span><br />");
        strbCurrentMission.AppendLine("<span style='font-size: 1.5em; color: #FFF; line-height: 1em; font-weight: 600;'>MISSION</span><br />");
        strbCurrentMission.AppendLine("<span style='font-size: 1.5em; color: #FFF; line-height: 1em; font-weight: 600;'>POINTS</span><br />");
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("</div><br style='clear:both;'/>");

        litBanner.Text = strbCurrentMission.ToString();

        litTitle.Text = clsMissions.strTitle;
    }
    #endregion

    #region SAVE METHODS

    protected void saveMissionAndLoadNext(int iCurrentMissionID)
    {
        string strMissionTitle = "";
        int iMissionPoints = 200;

        bool bAlreadyExists = clsCommonFunctions.DoesRecordExist("tblNotifications", "iMissionID='" + iCurrentMissionID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

        if (!bAlreadyExists)
        {
            clsNotifications clsNotifications = new clsNotifications();

            //### Add / Update
            clsNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsNotifications.iAddedBy = -10;
            clsNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsNotifications.iGroupID = Convert.ToInt32(clsAccountUsers.iGroupID);
            clsNotifications.iMissionID = iCurrentMissionID;
            clsNotifications.strMessage = clsAccountUsers.strFirstName + " has just completed the mission: Profile";

            clsNotifications.Update();

            clsMissions clsCurrentMission = new clsMissions(iCurrentMissionID);

            bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMission.iAchievementID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID);

            if (!bUserAchievementExists)
            {
                clsUserAchievements clsUserAchievement = new clsUserAchievements();
                clsUserAchievement.iAccountUserID = clsAccountUsers.iAccountUserID;
                clsUserAchievement.iAchievementID = clsCurrentMission.iAchievementID;
                clsUserAchievement.Update();
            }

            clsAccountUsers.iEditedBy = -20;
            clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsAccountUsers.strMasterImage = "crop_" + GetMainImagePath(dlImages);
            clsAccountUsers.iCurrentPoints = clsAccountUsers.iCurrentPoints + iMissionPoints;
            clsAccountUsers.bHasCompletedProfileMission = true;

            clsAccountUsers.Update();

            clsAccountUserTrackings clsTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);

            clsTracking.dtEdited = DateTime.Now;

            DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsList("iAccountUserID = " + clsAccountUsers.iAccountUserID, "");

            int iCount = 1;
            int iTotal = dtUserAchievements.Rows.Count;

            clsTracking.Update();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //TRACKING TABLE LOGIC END

            clsAccountUsers.ProfileMisionComplete(clsAccountUsers.iAccountUserID);

            Session["clsAccountUsers"] = clsAccountUsers;

            Response.Redirect("Accelerate-Home.aspx");
        }
    }

    protected void saveMissionAndLoadNext2(int iCurrentMissionID)
    {
        string strMissionTitle = "";
        int iMissionPoints = 200;

        bool bAlreadyExists = clsCommonFunctions.DoesRecordExist("tblNotifications", "iMissionID='" + iCurrentMissionID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

        if (!bAlreadyExists)
        {
            clsNotifications clsNotifications = new clsNotifications();

            //### Add / Update
            clsNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsNotifications.iAddedBy = -10;
            clsNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsNotifications.iGroupID = Convert.ToInt32(clsAccountUsers.iGroupID);
            clsNotifications.iMissionID = iCurrentMissionID;
            clsNotifications.strMessage = clsAccountUsers.strFirstName + " has just completed the mission: Profile";

            clsNotifications.Update();

            clsMissions clsCurrentMission = new clsMissions(iCurrentMissionID);

            bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMission.iAchievementID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID);

            if (!bUserAchievementExists)
            {
                clsUserAchievements clsUserAchievement = new clsUserAchievements();
                clsUserAchievement.iAccountUserID = clsAccountUsers.iAccountUserID;
                clsUserAchievement.iAchievementID = clsCurrentMission.iAchievementID;
                clsUserAchievement.Update();
            }

            clsAccountUsers.iEditedBy = -20;
            clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsAccountUsers.strMasterImage = GetMainImagePath(dlImages);
            clsAccountUsers.iCurrentPoints = clsAccountUsers.iCurrentPoints + iMissionPoints;
            clsAccountUsers.bHasCompletedProfileMission = true;

            clsAccountUsers.Update();

            clsAccountUserTrackings clsTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);

            clsTracking.dtEdited = DateTime.Now;

            DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsList("iAccountUserID = " + clsAccountUsers.iAccountUserID, "");

            int iCount = 1;
            int iTotal = dtUserAchievements.Rows.Count;

            clsTracking.Update();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //TRACKING TABLE LOGIC END

            clsAccountUsers.ProfileMisionComplete(clsAccountUsers.iAccountUserID);

            Session["clsAccountUsers"] = clsAccountUsers;

            Response.Redirect("Accelerate-Home.aspx");
        }
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 3;

    string strUniqueFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\AccountUsers";

    protected void btnCrop_Click(object sender, EventArgs e)
    {
        string strImageName = Session["WorkingImage"].ToString();

        //### Validate a cropped area has been selected
        if (HiddenW.Value != "" && HiddenW.Value != "0" && HiddenH.Value != "" && HiddenH.Value != "0" && HiddenX.Value != "" && HiddenY.Value != "")
        {
        int w = Convert.ToInt32(HiddenW.Value);
        int h = Convert.ToInt32(HiddenH.Value);
        string xString = HiddenX.Value.ToString();
        string yString = HiddenY.Value.ToString();
        if (xString.Contains("."))
        {
            xString = xString.Substring(0, xString.IndexOf("."));
        }
        if (yString.Contains("."))
        {
            yString = yString.Substring(0, yString.IndexOf("."));
        }
        //xString = xString.Substring(0, xString.IndexOf("."));
        //yString = yString.Substring(0, yString.IndexOf("."));
        int x = Convert.ToInt32(xString);
        int y = Convert.ToInt32(yString);

        byte[] CropImage = Crop(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + strImageName, w, h, x, y);

            using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
            {
                ms.Write(CropImage, 0, CropImage.Length);
                using (SD.Image CroppedImage = SD.Image.FromStream(ms, true))
                {
                    string SaveTo = strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + "crop_" + strImageName;
                    CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                    bIsCropped = true;
                }
            }

            CopyAndResizePic(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + strImageName);
            getList(lblUniquePath.Text);
       }
        //else
        //{
            //### Show message
            //mandatoryDiv.Visible = true;
            //lblValidationMessage.Text = "Select an area you wish to crop.";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "jCropUnique", "jCrop();", true);
       // }
    }

    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {
        try
        {
            using (SD.Image OriginalImage = SD.Image.FromFile(Img))
            {
                using (SD.Bitmap bmp = new SD.Bitmap(Width, Height))
                {
                    bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);

                    using (SD.Graphics Graphic = SD.Graphics.FromImage(bmp))
                    {
                        Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                        Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Graphic.DrawImage(OriginalImage, new SD.Rectangle(0, 0, Width, Height), X, Y, Width, Height, SD.GraphicsUnit.Pixel);

                        MemoryStream ms = new MemoryStream();
                        bmp.Save(ms, OriginalImage.RawFormat);

                        return ms.GetBuffer();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw (Ex);
        }
    }

    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\AccountUsers" + iCount) == true)
        {
            iCount++;
        }
        return "AccountUsers" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            Session["WorkingImage"] = FileUpload.FileName;

            clsAccountUsers.strPathToImages = strUniquePath;

            clsCommonFunctions.ResizeImage(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + Session["WorkingImage"], strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" +
                Session["WorkingImage"], 620, 620, true);

            imgCrop.ImageUrl = "../AccountUsers" + "/" + strUniquePath + "/" + strUploadFileName;
            ModalPopupExtenderCrop.Show();
            Session["WorkingImage"] = FileUpload.FileName;
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            //lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {
            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath.Replace(strFileName, "crop_") + strFileName, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 134, 134, false);

        }
        catch (Exception ex) { }
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iAccountUserID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iAccountUserID"]))
                iAccountUserID = Request.QueryString["iAccountUserID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    //<a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'></a>

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align:centre;'>
                                           <center><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /><br class='clr'/></center>" +
                                             "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton' style='height:25px; width:100px;'>Delete</div></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
            if (bIsCropped)
            {
                //try
                //{
                    saveMissionAndLoadNext(1);
                //}
                //catch(Exception ex)
                //{
                //    Response.Redirect("Accelerate-Error404.aspx");
                //}
            }
        }
        catch (Exception ex) { }
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }
    #endregion

    #region DEFAULT IMAGES METHODS

    List<string> lstDefaultImages;
    List<string> lstDefaultImagesFileNames;
    string strUniqueUploadFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\AccountUsers";
    string strUniqueDocumentFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\DefaultImages";

    public void getDefaultImage(String strPathToFolder, String strFileName)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        lstDefaultImages = new List<string>();
        lstDefaultImagesFileNames = new List<string>();

        int iLastIndexOf = strFileName.LastIndexOf('.');

        int iCounter = strFileName.Length - iLastIndexOf;

        string strFileNameCurrent = strFileName.Remove(iLastIndexOf, iCounter);

        DataTable dtDefaultImages = clsDefaultImages.GetDefaultImagesList();
        clsAccountUsers.strPathToImages = GetUniquePath();

        try
        {
            foreach (DataRow dr in dtDefaultImages.Rows)
            {
                string strPath = strPathToFolder;
                string strUserPath = strUniqueUploadFullPath + "\\" + clsAccountUsers.strPathToImages;
                //string[] oldFiles = Directory.GetFiles(strUserPath);
                string[] files = Directory.GetFiles(strPathToFolder + "\\" + dr["strPathToImages"].ToString());

                string iAccountUserID = "";
                if (!string.IsNullOrEmpty(Request.QueryString["iAccountUserID"]))
                    iAccountUserID = Request.QueryString["iAccountUserID"];

                int iImagesCount = 0;

                foreach (string strName in files)
                {
                    if (strName.Contains(strFileNameCurrent))
                    {
                        if (strName.IndexOf("_sml") != -1)
                        {

                            string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
                            strHTMLImages = strHTMLImages.Replace("\\", "/");
                            string strHtmlLink = strHTMLImages.Replace("../", "/");
                            //string strImagePath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + strHtmlLink;
                            //string strFinalImagePath = strHtmlLink.Replace("/DefaultImages", "");
                            //strFinalImagePath = strFinalImagePath.Replace("/", "");

                            string strDestFile = strUniqueUploadFullPath + "\\" + clsAccountUsers.strPathToImages;

                            if (!System.IO.Directory.Exists(strDestFile))
                            {
                                System.IO.Directory.CreateDirectory(strDestFile);
                                strDestFile = System.IO.Path.Combine(strDestFile, strFileName);
                                System.IO.File.Copy(strName, strDestFile, true);
                            }
                            else
                            {
                                strDestFile = System.IO.Path.Combine(strDestFile, strFileName);
                                System.IO.File.Copy(strName, strDestFile, true);
                            }

                            //### Generates a javascript postback for the delete method
                            String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + 0);

                            //<a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'></a>

                            lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align:centre;'>
                                           <center><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /><br class='clr'/></center>" +
                                                     "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton' style='height:25px; width:100px;'>Delete</div></div></center>" + @"
                                        </td>
                                    </tr>");
                            lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                            iImagesCount++;
                        }
                        else
                        {
                            string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
                            strHTMLImages = strHTMLImages.Replace("\\", "/");
                            string strHtmlLink = strHTMLImages.Replace("../", "/");
                            //string strImagePath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + strHtmlLink;
                            //string strFinalImagePath = strHtmlLink.Replace("/DefaultImages", "");
                            //strFinalImagePath = strFinalImagePath.Replace("/", "");

                            string strDestFile = strUniqueUploadFullPath + "\\" + clsAccountUsers.strPathToImages;

                            if (!System.IO.Directory.Exists(strDestFile))
                            {
                                System.IO.Directory.CreateDirectory(strDestFile);
                                strDestFile = System.IO.Path.Combine(strDestFile, strFileName);
                                System.IO.File.Copy(strName, strDestFile, true);
                            }
                            else
                            {
                                strDestFile = System.IO.Path.Combine(strDestFile, strFileName);
                                System.IO.File.Copy(strName, strDestFile, true);
                            }

                            //### Generates a javascript postback for the delete method
                            String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                            //<a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'></a>

                            lstDefaultImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align:centre;'>
                                           <center><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /><br class='clr'/></center>"
                                               + @"
                                        </td>
                                    </tr>");
                            lstDefaultImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                            iImagesCount++;
                        }

                    }
                }
            }

            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstDefaultImages"] = lstDefaultImages;
            Session["lstDefaultImagesFileNames"] = lstDefaultImagesFileNames;
            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
            imgCompleted.Style.Add("display", "inline-block");
        }
        catch (Exception ex) { }
    }

    public void getDefaultImagesList()
    {
        lstDefaultImages = new List<string>();
        lstDefaultImagesFileNames = new List<string>();

        DataTable dtDefaultImages = clsDefaultImages.GetDefaultImagesList();
        try
        {
            foreach (DataRow dr in dtDefaultImages.Rows)
            {

                string[] files = Directory.GetFiles(strUniqueDocumentFullPath + "//" + dr["strPathToImages"].ToString());

                string iAccountUserID = "";
                if (!string.IsNullOrEmpty(Request.QueryString["iAccountUserID"]))
                    iAccountUserID = Request.QueryString["iAccountUserID"];

                int iImagesCount = 0;

                foreach (string strName in files)
                {
                    if (strName.IndexOf("_sml") != -1)
                    {

                        string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
                        strHTMLImages = strHTMLImages.Replace("\\", "/");
                        string strHtmlLink = strHTMLImages.Replace("../", "/");
                        string strImagePath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + strHtmlLink;
                        string strFinalImagePath = strHtmlLink.Replace("/DefaultImages", "");
                        strFinalImagePath = strFinalImagePath.Replace("/", "");
                        //moving file

                        //### Generates a javascript postback for the delete method
                        String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDefaultImages:" + iImagesCount);

                        //<a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'></a>

                        lstDefaultImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align:centre;'>
                                           <center><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff; margin: 15px;'/><br class='clr'/></center>"
                                               + @"
                                        </td>
                                    </tr>");
                        lstDefaultImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                        iImagesCount++;
                    }
                }

            }
            dlDefaultImages.DataSource = lstDefaultImages;
            dlDefaultImages.DataBind();

            Session["lstDefaultImages"] = lstDefaultImages;
            Session["lstDefaultImagesFileNames"] = lstDefaultImagesFileNames;
        }
        catch (Exception ex)
        { }

    }

    private string GetMainDefaultImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainDefaultImage = (RadioButton)dliTarget.FindControl("rdbMainDefaultImage");

            string strHiddenValue = lblDefaultImages.Value.ToString();
            List<string> lstDefaultImages = strHiddenValue.Split('|').ToList();
            string strID = "";
            bool bChecked = false;
            int iCount = 1;
            foreach (string s in lstDefaultImages)
            {
                if (iCount == 1)
                {
                    strID = s;
                    iCount++;
                }
                else
                {
                    bChecked = Convert.ToBoolean(s);
                    break;
                }
            }

            if (rdbMainDefaultImage.ClientID == strID)
            {
                if (bChecked)
                {
                    lstDefaultImagesFileNames = (List<string>)Session["lstDefaultImagesFileNames"];
                    strReturn = lstDefaultImagesFileNames[dliTarget.ItemIndex];
                    break;
                }
            }
        }
        return strReturn;
    }

    private string GetSingleDefaultImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            lstDefaultImagesFileNames = (List<string>)Session["lstDefaultImagesFileNames"];
            strReturn = lstDefaultImagesFileNames[dliTarget.ItemIndex];
            break;
        }
        return strReturn;
    }

    #endregion

    protected void btnSaveImage_Click(object sender, EventArgs e)
    {
        clsAccountUsers.strMasterImage = GetMainDefaultImagePath(dlDefaultImages);
        getDefaultImage(strUniqueDocumentFullPath, clsAccountUsers.strMasterImage);
        btnFileUpload.Disabled = true;
        bIsDefault = true;
        try
        {
            saveMissionAndLoadNext2(1);
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    protected void btnChooseDefault_Click(object sender, EventArgs e)
    {
        int count = 0;
        foreach (DataListItem dliTarget in dlDefaultImages.Items)
        {
            count++;
        }

        if (count == 1)
        {
            clsAccountUsers.strMasterImage = GetSingleDefaultImagePath(dlDefaultImages);
            getDefaultImage(strUniqueDocumentFullPath, clsAccountUsers.strMasterImage);
            btnFileUpload.Disabled = true;
            bIsDefault = true;
            //try
            //{
                saveMissionAndLoadNext2(1);
            //}
            //catch (Exception ex)
            //{
            //    Response.Redirect("Accelerate-Error404.aspx");
            //}
        }
        else
        {
            ModalPopupExtender1.Show();
        }
        
    }

    protected void btnCloseImages_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
        pnlDefaultImages.Visible = false;
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (IsPostBack && FileUpload.PostedFile != null && FileUpload.PostedFile.FileName.Length > 0)
        {
            if (Session["lstImages"] == null)
            {
                lstImages = new List<string>();
                Session["lstImages"] = lstImages;
            }
            if (dlImages.Items.Count == iMaxImages)
            {
                mandatoryDiv.Visible = true;
                lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                getList(lblUniquePath.Text);
            }
            else
            {
                mandatoryDiv.Visible = false;

                string strUniquePath;
                if (lblUniquePath.Text == "")
                {
                    strUniquePath = GetUniquePath();
                    lblUniquePath.Text = strUniquePath;
                }
                else
                {
                    strUniquePath = lblUniquePath.Text;
                }
                UploadImages(strUniquePath);
                getList(strUniquePath);
                
            }
        }
    }

   
}