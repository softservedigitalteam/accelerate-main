﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using System.DirectoryServices;

public partial class Accelerate : System.Web.UI.MasterPage
{
    private clsAccountUsers clsAccountUsers;
    clsAccountUserTrackings clsAccountUserTracking;
    private clsGroups clsGroup;

    string cacheGroup = "";
    string cacheGroupPhase1 = "";
    string cacheGroupPhase2 = "";
    string cacheGroupPhase3 = "";

    DataTable dtPhase1Missions;
    DataTable dtPhase2Missions;
    DataTable dtPhase3Missions;
    int iNextMissionID = 0;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        clsAccountUserTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);
        clsGroup = new clsGroups(clsAccountUsers.iGroupID);

        popNotifications();
        popAchieversList();
        popProfile();
        //Testing colours
        popColourScheme();
        popWelcomeMessage();
    }

    #endregion

    //Testing colours
    protected void popColourScheme()
    {
        if (clsGroup.iCustomGeneralColourID != 0)
        {
            clsCustomGeneralColours clsCustomGeneralColours = new clsCustomGeneralColours(clsGroup.iCustomGeneralColourID);
            if (clsCustomGeneralColours != null)
            {
                litStyleSheet.Text = "<link href='" + clsCustomGeneralColours.strLink + "' rel='stylesheet' />";
            }
        }

    }

    //### Optimised ###
    protected void popProfile()
    {
        string strFileName = "";
        string strFullPath = "";
        int iCurrentPoints = clsAccountUsers.iCurrentPoints;
        int iPercentageComplete = 0;

        if (!string.IsNullOrEmpty(clsAccountUsers.strMasterImage))
        {
            strFullPath = clsAccountUsers.strPathToImages;

            if (!string.IsNullOrEmpty(clsAccountUsers.strMasterImage))
            {
                strFileName = clsAccountUsers.strMasterImage;
                strFullPath = "AccountUsers/" + clsAccountUsers.strPathToImages + "/" + Path.GetFileNameWithoutExtension(strFileName) + Path.GetExtension(strFileName);
            }
            else
            {
                strFullPath = "img/DefaultNoImageProfile.png";
            }
        }

        StringBuilder strbProfile = new StringBuilder();

        strbProfile.AppendLine("<div class='well ProfileSection' style='background: #00aae7; color: #fff;'>");
        strbProfile.AppendLine("<center><img onerror=\"this.src='img/DefaultNoImageProfile.png'\" src='" + strFullPath + "' /></center><br /><br />");
        strbProfile.AppendLine("<b>" + clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname + "</b><br /><br />");
        if (clsAccountUsers.strJobTitle != null && clsAccountUsers.strJobTitle.ToString() != "")
        {
            strbProfile.AppendLine("<b>Business Unit</b><br />");
            if (clsAccountUsers.strJobTitle != null)
            {
                strbProfile.AppendLine(clsAccountUsers.strJobTitle + "<br />");
            }
            else
            {
                strbProfile.AppendLine("<br />");
            }
        }
        strbProfile.AppendLine("<br />");
        strbProfile.AppendLine("<b>Current Rank</b><br />");

        litFirstName.Text = clsAccountUsers.strFirstName;
        string strRank = "";
        int iMaxPoints = 0;

        //**Get Rank Code**//
        //### Optimised ###

        DataRow drvCustomGroupRank = clsDataAccess.GetRecord("SELECT TOP(1) * FROM tblCustomGroupRanks WHERE iCustomGroupRankID = '" + clsGroup.iCustomGroupRankID + "'");
        if (drvCustomGroupRank != null)
        {
            string strRankCounter = "";
            string strPointsCounter = "";

            for (int i = 1; i < 8; i++)
            {
                strRankCounter = "";
                strPointsCounter = "";
                strRankCounter = "strRank" + i.ToString() + "Title";
                strPointsCounter = "iRank" + i.ToString() + "Points";

                if ((drvCustomGroupRank[strRankCounter].ToString() != "") && (drvCustomGroupRank[strRankCounter].ToString() != null) && (drvCustomGroupRank[strPointsCounter].ToString() != "") && (drvCustomGroupRank[strPointsCounter].ToString() != "0"))
                {
                    if (iMaxPoints < Convert.ToInt32(drvCustomGroupRank[strPointsCounter]))
                    {
                        iMaxPoints = Convert.ToInt32(drvCustomGroupRank[strPointsCounter]);
                    }
                }
                else
                {
                    break;
                }

            }
        }

        if (iCurrentPoints < iMaxPoints)
        {
            strRank = "Newbie";
        }
        else
        {
            strRank = "Deloitte Citizen";
        }

        strbProfile.AppendLine("" + strRank + "");
        strbProfile.AppendLine("</div>");
        strbProfile.AppendLine("<div class='rankStatus'>");

        //###   Custom Progress Bar
        if (iCurrentPoints < iMaxPoints)
        {
            strbProfile.AppendLine("<b>" + strRank + "</b>" + ": <span>Not there yet!</span>");
        }
        else
        {
            strbProfile.AppendLine("<span>At top rank!</span>");
        }

        strbProfile.AppendLine("</div>");
        strbProfile.AppendLine("<div class='progressBarSection'>");

        if (iCurrentPoints > iMaxPoints)
        {
            iCurrentPoints = iMaxPoints;
            iPercentageComplete = 100;
        }
        else if (iCurrentPoints == iMaxPoints)
        {
            iPercentageComplete = 100;
        }
        else if ((iCurrentPercentage(iCurrentPoints, iMaxPoints) / 2) < 10)
        {
            iPercentageComplete = iCurrentPercentage(iCurrentPoints, iMaxPoints) / 2;
        }
        else
        {
            iPercentageComplete = iCurrentPercentage(iCurrentPoints, iMaxPoints) / 2;
        }

        strbProfile.AppendLine("<div class='progressBar' style='width: " + iPercentageComplete + "%; font-weight:bold;'></div>");
        strbProfile.AppendLine("<span style='font-weight:bold;'>" + iPercentageComplete + "% Complete</span>");
        strbProfile.AppendLine("</div>");

        litProfile.Text = strbProfile.ToString();
    }

    #region Pop Data

    protected void popAchieversList()
    {
        //### Optimised ###
        DataTable dtAchieversList = clsAccountUsers.GetTop5AccountUsersList(clsAccountUsers.iGroupID);
        dtAchieversList.Columns.Add("strFullName");
        dtAchieversList.Columns.Add("strFullPath");

        foreach (DataRow dr in dtAchieversList.Rows)
        {
            if ((dr["strMasterImage"].ToString() == null) || (dr["strMasterImage"].ToString() == ""))
            {
                dr["strFullPath"] = "images/no-image.png";
            }
            else
            {
                dr["strFullPath"] = "AccountUsers/" + dr["strPathToImages"].ToString() + "/" + Path.GetFileNameWithoutExtension(dr["strMasterImage"].ToString()) + "" + Path.GetExtension(dr["strMasterImage"].ToString());
            }
        }

        rpTop3Achievers.DataSource = dtAchieversList;
        rpTop3Achievers.DataBind();
    }

    protected void popNotifications()
    {

        DataTable dtNotifications = clsNotifications.GetGroupNotificationsList(clsAccountUsers.iGroupID);

        StringBuilder strbNotifications = new StringBuilder();
        if (dtNotifications != null)
        {
            foreach (DataRow dr in dtNotifications.Rows)
            {

                strbNotifications.AppendLine("<b>Notification</b><br />");
                strbNotifications.AppendLine(dr["strMessage"].ToString() + " <br /><br />");
            }
        }
        else
        {
            strbNotifications.AppendLine("");
        }

        litActivityFeed.Text = strbNotifications.ToString();
    }

    //### Returns Current Percentage
    protected int iCurrentPercentage(int iCurrentPoints, int iTotalPoints)
    {
        double dblCurrentPercentage = Convert.ToDouble(iCurrentPoints) / Convert.ToDouble(iTotalPoints);

        int iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);

        return iCurrentPercentage;
    }

    protected void popWelcomeMessage()
    {
        //clsWelcomeMessages clsWelcomeMessages = new clsWelcomeMessages(clsGroup.iWelcomeMessageID);
        //string strWelcomeMessage = clsWelcomeMessages.strParagraphText.ToString();
        //strWelcomeMessage = strWelcomeMessage.Replace("\n", "<br />");
        //litWelcomeMessage.Text = strWelcomeMessage;
        clsCompanies clsCompanies = new clsCompanies(clsGroup.iCompanyID);
        string strCompanyDescription = clsCompanies.strDescription.ToString();
        strCompanyDescription = strCompanyDescription.Replace("\n", "<br />");
        litWelcomeMessage.Text = strCompanyDescription;
    }

    #endregion
}