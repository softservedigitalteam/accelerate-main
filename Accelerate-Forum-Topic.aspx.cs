﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Forum_Topic : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsForums clsForums;

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        if (!IsPostBack)
        {
            //### If the iForumID is passed through then we want to instantiate the object with that iForumID
            if ((Request.QueryString["iForumID"] != "") && (Request.QueryString["iForumID"] != null))
            {
                try
                {
                    clsForums = new clsForums(Convert.ToInt32(Request.QueryString["iForumID"]));

                    lblForumTitle.Text = clsForums.strTitle;
                    popForumComments(clsForums.iForumID);
                }
                catch(Exception ex)
                {
                    Response.Redirect("Accelerate-Error404.aspx");
                }
            }
        }
    }

    protected void btnAddForumTopic_Click(object sender, EventArgs e)
    {
        int iForumID = Convert.ToInt32(Request.QueryString["iForumID"]);

        //### Validate Forum Topic (TITLE)         
        //bool bAlreadyExists = clsCommonFunctions.DoesRecordExist("tblForumComments", "strComment='" + txtComment.Text + "' AND iForumID=" + iForumID + " AND bIsDeleted=0");
        bool bNotEqualToDefaultText;

        if ((txtComment.Text != "Enter your comment here...") &&(txtComment.Text !=""))
            bNotEqualToDefaultText = true;
        else
            bNotEqualToDefaultText = false;

        //if ((!bAlreadyExists) && (bNotEqualToDefaultText))
        if ((bNotEqualToDefaultText))
        {
            try
            {
                SaveData(iForumID);
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
        }

        //### Repopulate Forum Topics
        try
        {
            popForumComments(iForumID);
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        txtComment.Text = "";
    }

    #region SAVE DATA METHODS

    private void SaveData(int iForumID)
    {
        clsForumComments clsForumComments = new clsForumComments();

        //### Add / Update
        clsForumComments.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsForumComments.iAddedBy = clsAccountUsers.iAccountUserID;
        clsForumComments.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsForumComments.iEditedBy = 0;
        clsForumComments.iForumID = iForumID;
        clsForumComments.strComment = txtComment.Text;

        clsForumComments.Update();

        clsNotifications clsNotifications = new clsNotifications();

        //### Add / Update
        clsNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsNotifications.iAddedBy = -10;
        clsNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
        clsNotifications.iGroupID = Convert.ToInt32(clsAccountUsers.iGroupID);
        clsNotifications.iMissionID = -10;

        clsForums clsCurrentForum = new clsForums(iForumID);

        clsNotifications.strMessage = clsAccountUsers.strFirstName + " has just posted a comment on the topic: " + clsCurrentForum.strTitle + "";

        clsNotifications.Update();
    }

    #endregion

    #region POPULATE METHODS

    protected void popForumComments(int iForumID)
    {
        DataTable dtUserForum = clsForumComments.GetForumCommentsList("iForumID=" + iForumID, "");

        StringBuilder strbComments = new StringBuilder();

        string strTitle;
        int iForumCommentID;

        int iCount = 0;

        litForumTopicComments.Text = "";

        foreach (DataRow dtrAccountMember in dtUserForum.Rows)
        {
            ++iCount;

            iForumCommentID = Convert.ToInt32(dtrAccountMember["iForumCommentID"].ToString());

            clsForumComments clsCurrentForumComment = new clsForumComments(iForumCommentID);

            strTitle = clsCurrentForumComment.strComment;

            clsAccountUsers clsCurrentAccountUserCommenting = new clsAccountUsers(clsCurrentForumComment.iAddedBy);

            string strFileName;
            string strFullPath;

            if (!string.IsNullOrEmpty(clsAccountUsers.strMasterImage))
            {
                strFullPath = clsAccountUsers.strPathToImages;
                if (!string.IsNullOrEmpty(clsAccountUsers.strMasterImage))
                {
                    strFileName = clsAccountUsers.strMasterImage;
                    strFullPath = "AccountUsers/" + clsAccountUsers.strPathToImages + "/" + Path.GetFileNameWithoutExtension(strFileName) + Path.GetExtension(strFileName);
                }
            }
            else
                strFullPath = "images/no-image.png";

            if (iCount != 1)
                strbComments.AppendLine("<br style='clear:both' />");

            strbComments.AppendLine("<div style=''>");
            strbComments.AppendLine("<div style='float:left;'>");
            strbComments.AppendLine("<img src='" + strFullPath + "' title='" + clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname + "' alt='" + clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname + "' width='55px' height='55px' />");
            strbComments.AppendLine("</div>");
            strbComments.AppendLine("<div style='float:left;margin-left:20px;'>");
            strbComments.AppendLine("" + clsCurrentAccountUserCommenting.strFirstName + "&nbsp;commented on " + clsCurrentForumComment.dtAdded.ToString("dd MMM yyyy") + " (at " + clsCurrentForumComment.dtAdded.ToString("HH:mm") + ")");
            strbComments.AppendLine("<br class='clr'/>");
            strbComments.AppendLine("<span>" + strTitle.Replace("\n", "<br />") + "</span>");
            strbComments.AppendLine("</div>");
            strbComments.AppendLine("</div>");
            strbComments.AppendLine("<br style='clear:both' />");
        }

        litForumTopicComments.Text = strbComments.ToString();
    }

    #endregion
}