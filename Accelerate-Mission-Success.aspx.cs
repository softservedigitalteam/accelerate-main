﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Success : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsMissions clsMissions;
    public clsAccountUserTrackings clsTracking;
    int iMissionID = 0;
    int iNextMissionID;

    public DataTable dtPhase1Missions;
    public DataTable dtPhase2Missions;
    public DataTable dtPhase3Missions;

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
            clsTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);
            litPoints.Text = clsAccountUsers.iCurrentPoints.ToString();
            checkCompletedPhasesUserTracking();

            iMissionID = Convert.ToInt32(Request.QueryString["iMissionID"]);
            int iGroupID = clsAccountUsers.iGroupID;
            clsGroups clsGroup = new clsGroups(iGroupID);
        }
        catch (Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
        //if (clsGroup.iCustomGeneralColourID == 0)
        //{ }
        //else
        //    popStyleSheet(clsGroup.iCustomGeneralColourID);

        if (!IsPostBack)
        {
            if ((Request.QueryString["iMissionID"] != "") && (Request.QueryString["iMissionID"] != null))
            {
                //### Populate the form
                try
                {
                    popCompletedMessage(iMissionID);
                }
                catch(Exception ex)
                {
                    Response.Redirect("Accelerate-Error404.aspx");
                }
            }
        }
        //saveMissionAndLoadNext(iMissionID);
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (iNextMissionID != 0)
            Response.Redirect("Accelerate-Current-Mission.aspx?iMissionID=" + iNextMissionID);
        else
            Response.Redirect("Accelerate-Home.aspx");
    }

    protected void btnPhase1_Click(object sender, EventArgs e)
    {
        popUserPhaseAchievements(1);
    }

    protected void btnPhase2_Click(object sender, EventArgs e)
    {
        popUserPhaseAchievements(2);
    }

    protected void btnPhase3_Click(object sender, EventArgs e)
    {
        popUserPhaseAchievements(3);
    }

    protected void checkCompletedPhasesUserTracking()
    {
        clsPhases clsPhase1 = new clsPhases(clsTracking.iPhase1ID);
        litPhase1Text.Text = clsPhase1.strTitle;
        clsPhases clsPhase2 = new clsPhases(clsTracking.iPhase2ID);
        litPhase2Text.Text = clsPhase2.strTitle;
        clsPhases clsPhase3 = new clsPhases(clsTracking.iPhase3ID);
        litPhase3Text.Text = clsPhase3.strTitle;
        litPhase3Text.Text = "<span><span class='hideSmall'>Phase </span>3</span><span style='font-size:8px;color:#222;position:relative;top:-10px;width: 70%; text-overflow: ellipsis;'><br />" + clsPhase3.strTitle + "</span>";

        dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase1ID, "");
        dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase2ID, "");
        dtPhase3Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase3ID, "");

        int iCurrentPhaseLevel = clsTracking.iCurrentPhaseLevel;

        if (iCurrentPhaseLevel == 1)
        {
            popUserPhaseAchievements(1);
            btnPhase2.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
        }
        else if (iCurrentPhaseLevel == 2)
        {
            btnPhase2.Enabled = true;
            btnPhase2.Text = "<div class='showcaseSection Blue2'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
            popUserPhaseAchievements(2);
        }
        else if (iCurrentPhaseLevel == 3)
        {
            btnPhase2.Enabled = true;
            btnPhase3.Enabled = true;
            btnPhase2.Text = "<div class='showcaseSection Blue2'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Blue3'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
            popUserPhaseAchievements(3);
        }
    }

    protected void popUserPhaseAchievements(int iPhaseLevel)
    {
        DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsRecords(clsAccountUsers.iAccountUserID);

        DataTable dtUserDisplayAchievements = new DataTable();
        dtUserDisplayAchievements.Clear();
        dtUserDisplayAchievements.Columns.Add("iMissionID");
        dtUserDisplayAchievements.Columns.Add("strLink");
        dtUserDisplayAchievements.Columns.Add("strTitle");
        dtUserDisplayAchievements.Columns.Add("strNewRow");
        dtUserDisplayAchievements.Columns.Add("FullPathForImage");

        List<int> lstAchievements = new List<int>();
        List<int> lstMissions = new List<int>();
        //T
        List<int> lstNextMission = new List<int>();

        int iCount = 1;
        int iAchieveTotalCount = 0;

        if (iPhaseLevel == 1)
        {
            foreach (DataRow dr in dtPhase1Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }
            //T
            lstNextMission = lstMissions;

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }
        else if (iPhaseLevel == 2)
        {
            foreach (DataRow dr in dtPhase2Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }
        else if (iPhaseLevel == 3)
        {
            foreach (DataRow dr in dtPhase3Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }

        rpAchievements.DataSource = dtUserDisplayAchievements;
        rpAchievements.DataBind();
    }

    protected void popCompletedMessage(int iMissionID)
    {
        clsMissions = new clsMissions(iMissionID);
        clsAchievements clsAchievements = new clsAchievements(clsMissions.iAchievementID);

        string strHTMLImageProduct = "";

        try
        {
            string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Achievements\\" + clsAchievements.strPathToImages);

            //### Get the first image from the list
            strHTMLImageProduct = strImagesProduct[0].Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);
        }
        catch { }

        StringBuilder strbCurrentMission = new StringBuilder();

        //strbCurrentMission.AppendLine("<span style='text-align:center;color:#3c3c3c;'>You have earned the following badge \"" + clsMissions.strTitle + "\" and " + clsMissions.iBonusPoints + " points.</span><br /><br />");
        //strbCurrentMission.AppendLine("<br /><img src='images/imgStarsUpper.png' /><br /><br /><br />");
        //strbCurrentMission.AppendLine("<div class='roundedCornerBox'><img src='" + strHTMLImageProduct + "' style='text-align:center;' width='120px' height='auto'/><br /><br />" + clsMissions.strTitle + "</div><br />");
        //strbCurrentMission.AppendLine("<center><div class='buttonStyle' style='padding-top: 5px;'><a href='Accelerate-Home.aspx' style='color: #black;'><div class='deleteButton' style='height:22px; width:100px;'>Continue</div></div></center><br />");
        //strbCurrentMission.AppendLine("<br /><img src='images/imgStarsLower.png' /><br /><br /><br />");
        //strbCurrentMission.AppendLine("</div>");

        strbCurrentMission.AppendLine("<img src='img/imgCongratulations.png' style='text-align:center' />");
        strbCurrentMission.AppendLine("<br /><br /><br />");
        strbCurrentMission.AppendLine("<p style = 'color:black;'><b>You have earned the badge \"" + clsMissions.strTitle + "\" and earned 300 points </b></p>");
        strbCurrentMission.AppendLine("<br /><br />");
        strbCurrentMission.AppendLine("<img src='" + strHTMLImageProduct + "' style='width:25%;' />");
        strbCurrentMission.AppendLine("<br /><br />");
        strbCurrentMission.AppendLine("<div style='font-size:18px; color:black;'><b>" + clsMissions.strTitle + "</b></div>");

        litCurrentMission.Text = strbCurrentMission.ToString();
        // < asp:Button ID = "btnContinue" runat = "server" Text = "Continue" CssClass = "btnStart" Width = "100px" OnClick = "btnContinue_Click" />
    }

    protected void saveMissionAndLoadNext(int iCurrentMissionID)
    {
        string strReportName = "Completed Mission";
        clsMissions clsCurrentMissions = new clsMissions(iCurrentMissionID);
        StringBuilder strbMailBuilder = new StringBuilder();

        bool bAlreadyExists = clsCommonFunctions.DoesRecordExist("tblNotifications", "iMissionID='" + iCurrentMissionID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

        if (bAlreadyExists)
        {
            clsNotifications clsNotifications = new clsNotifications();

            //### Add / Update
            clsNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsNotifications.iAddedBy = -10;
            clsNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsNotifications.iGroupID = Convert.ToInt32(clsAccountUsers.iGroupID);
            clsNotifications.iMissionID = iCurrentMissionID;
            clsNotifications.strMessage = clsAccountUsers.strFirstName + " has just completed the mission: " + clsCurrentMissions.strTitle + "";

            clsNotifications.Update();

            clsNotifications clsBadgeNotifications = new clsNotifications();

            //### Add / Update
            clsBadgeNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsBadgeNotifications.iAddedBy = -10;
            clsBadgeNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsBadgeNotifications.iMissionID = iCurrentMissionID;

            clsAchievements clsAchievements = new clsAchievements(clsCurrentMissions.iAchievementID);

            clsBadgeNotifications.strMessage = clsAccountUsers.strFirstName + " has just earned a new badge: " + clsAchievements.strTitle + "";

            clsBadgeNotifications.Update();

            //clsAccountUsers.iCurrentPoints = clsAccountUsers.iCurrentPoints + clsCurrentMissions.iBonusPoints;
            clsAccountUsers.Update();

            Session["clsAccountUsers"] = clsAccountUsers;

            //bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMissions.iAchievementID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

            bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMissions.iAchievementID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID);

            strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
            strbMailBuilder.AppendLine("<table width='100%' bgcolor='#efefef' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
            strbMailBuilder.AppendLine("<tbody>");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td>");
            strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
            strbMailBuilder.AppendLine("<tbody>");
            strbMailBuilder.AppendLine("<!-- Spacing -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td height='50'></td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End of Spacing -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td>");
            strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
            strbMailBuilder.AppendLine("<tbody>");

            strbMailBuilder.AppendLine("<!-- Content -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td width='60'></td>");
            strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, Arial;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
            strbMailBuilder.AppendLine("Dear " + clsAccountUsers.strFirstName + "<br /><br />");
            strbMailBuilder.AppendLine("You have just completed: " + clsAchievements.strTitle + "<br /><br />");
            strbMailBuilder.AppendLine("<br/>");
            strbMailBuilder.AppendLine("Please Login to the system to complete all other missions<br/>");
            strbMailBuilder.AppendLine("" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "/Accelerate-Home.aspx");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("<td width='60'></td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End of Content -->");

            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("<td width='20'></td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- Spacing -->");
            strbMailBuilder.AppendLine("<tr>");
            strbMailBuilder.AppendLine("<td height='50'></td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("<!-- End of Spacing -->");
            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            strbMailBuilder.AppendLine("</td>");
            strbMailBuilder.AppendLine("</tr>");
            strbMailBuilder.AppendLine("</tbody>");
            strbMailBuilder.AppendLine("</table>");
            strbMailBuilder.AppendLine("<!-- End of textbanner -->");


            //clsEmailSettings clsEmailSettings = new clsEmailSettings(true, clsAccountUsers.iAccountUserID);

            // if (clsEmailSettings.bEmailMissions)
            //{
            if (bUserAchievementExists)
            {
                clsUserAchievements clsUserAchievements = new clsUserAchievements();
                clsUserAchievements.iAccountUserID = clsAccountUsers.iAccountUserID;
                clsUserAchievements.iAchievementID = clsCurrentMissions.iAchievementID;
                clsUserAchievements.Update();

                Attachment[] empty = new Attachment[] { };

                try
                {
                    //missionCompletionEmail.SendMail("noreply@softservedigital.co.za", clsAccountUsers.strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "", "", empty, true);
                    //missionCompletionEmail.SendMail("no-reply@raonboarding.co.za", clsAccountUsers.strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "", "", empty, true);

                }
                catch { }
            }
            // }
        }
    }

    protected void popStyleSheet(int iCustomColourID)
    {
        clsCustomColours clsCustomColours = new clsCustomColours(iCustomColourID);

        //litStyleSheet.Text = "<link href='" + clsCustomColours.strLink + "' rel='stylesheet' />";
    }
}