﻿<%@ Page Title="Accelerate Onboarding :: Video Missions" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" CodeFile="Accelerate-Video-Missions.aspx.cs" Inherits="Accelerate_Video_Missions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <!-- get jQuery from the google apis -->
    <script type="text/javascript" src="js/jquery-1.10.1.js"></script>

    <!-- THE PREVIEW STYLE SHEETS, NO NEED TO LOAD IN YOUR DOM -->
    <link rel="stylesheet" type="text/css" href="css/noneed.css" media="screen" />

    <%--<link href="css/animate.css" rel="stylesheet" type="text/css" />--%>

    <!-- CSS STYLE-->
    <link rel="stylesheet" type="text/css" href="css/visualizationStyle.css" media="screen" />

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="css/extralayers.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />

    <!-- GOOGLE FONTS -->
    <%--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css' />--%>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--	<!-- HEADER -->
	<header>
		<section class="container">
			<article class="logo-container"><a href="http://themes.themepunch.com/?theme=revolution_jq"><div class="logo"></div></a></article>
			<div class="button-holder"><a href="http://codecanyon.net/item/slider-revolution-responsive-jquery-plugin/2580848" target="_blank" class="button"><strong>BUY NOW</strong></a></div>
			<div style="clear:both"></div>
		</section>
	</header> <!-- END OF HEADER -->--%>

    <article class="boxedcontainer">

        <!--
	    #################################
		    - THEMEPUNCH BANNER -
	    #################################
	    -->
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul style="position:relative;">
                    <!-- SLIDE  -->
                    <li data-transition="slidedown" data-slotamount="1" data-masterspeed="2000" style="margin-right: 50px !important; padding-right: 50px !important">
                        <!-- MAIN IMAGE -->
                        <img src="images/dummy.png" alt="slidebg2" data-lazyload="images/slidebg2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"/>
                        <!-- LAYERS -->

                        <div class="tp-caption black_heavy_70 customin randomrotateout tp-resizeme rs-parallaxlevel-5"
                            data-x="615"
                            data-y="40"
                            data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                            data-speed="500"
                            data-start="1400"
                            data-easing="Power3.easeInOut"
                            data-splitin="chars"
                            data-splitout="none"
                            data-elementdelay="0.2"
                            data-endelementdelay="0.2"
                            data-endspeed="600"
                            style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Video
	
                        </div>
                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption customin randomrotateout rs-parallaxlevel-7"
                            data-x="734"
                            data-y="98"
                            data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="300"
                            data-start="1900"
                            data-easing="Power3.easeInOut"
                            data-elementdelay="0.1"
                            data-endelementdelay="0.1"
                            data-endspeed="600"
                            style="z-index: 7;">
                            <img src="images/dummy.png" alt="" data-lazyload="images/largegreen.png">
                        </div>

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption light_heavy_70 customin randomrotateout tp-resizeme rs-parallaxlevel-6"
                            data-x="748"
                            data-y="106"
                            data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="300"
                            data-start="2200"
                            data-easing="Power3.easeInOut"
                            data-splitin="chars"
                            data-splitout="none"
                            data-elementdelay="0.1"
                            data-endelementdelay="0.1"
                            data-endspeed="600"
                            style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">Missions
                        </div>


                        <!-- BULLETS START -->
                        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->

                        <asp:Literal ID="litMissionVideoBullets" runat="server"></asp:Literal>

                        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                        <!-- BULLETS END -->

                        </li>

                        <!-- SLIDES START  -->
                        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->

                        <asp:Literal ID="litMissionPages" runat="server"></asp:Literal>

                        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                        <!-- SLIDES END -->
                        <%--                        <asp:Literal runat="server" ID="litMissionVideoSlides" EnableViewState="true" Visible="true"></asp:Literal>--%>
                </ul>
                <%--<div class="tp-bannertimer"></div>--%>	<%--</div>--%>
            </div>
        </div>
    </article>
    <script type="text/javascript">

        jQuery(document).ready(function () {

            jQuery('.tp-banner').show().revolution(
            {
                dottedOverlay: "none",
                delay: 3000,
                startwidth: 1170,
                startheight: 700,
                hideThumbs: 0,

                thumbWidth: 100,
                thumbHeight: 50,
                thumbAmount: 5,

                navigationType: "bullet",
                //navigationArrows: "solo",
                navigationStyle: "preview1",

                touchenabled: "off",
                onHoverStop: "off",

                swipe_velocity: -1,
                swipe_min_touches: -1,
                swipe_max_touches: -1,
                drag_block_vertical: false,

                parallax: "mouse",
                parallaxBgFreeze: "on",
                parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

                keyboardNavigation: "off",

                //navigationHAlign: "center",
                //navigationVAlign: "bottom",
                //navigationHOffset: 0,
                //navigationVOffset: 20,

                //soloArrowLeftHalign: "left",
                //soloArrowLeftValign: "center",
                //soloArrowLeftHOffset: 20,
                //soloArrowLeftVOffset: 0,

                soloArrowRightHalign: "right",
                soloArrowRightValign: "center",
                soloArrowRightHOffset: 0,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: "on",
                fullScreen: "off",

                spinner: "spinner4",

                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: 1,

                shuffle: "off",

                autoHeight: "off",
                forceFullWidth: "off",

                hideThumbsOnMobile: "off",
                hideNavDelayOnMobile: 3000,
                hideBulletsOnMobile: "off",
                hideArrowsOnMobile: "off",
                hideThumbsUnderResolution: 0,

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                videoJsPath: "rs-plugin/videojs/",
                fullScreenOffsetContainer: ""
            });

        });	//ready

    </script>

    <script type="text/javascript">

        var revapi;

        jQuery(document).ready(function () {

            revapi = jQuery('.tp-banner').revolution(
             {
                 delay: 9000,
                 startwidth: 1170,
                 startheight: 500,
                 hideThumbs: 10,
                 navigationType: "thumb",
                 navigationHAlign: "center",
                 navigationVAlign: "bottom",
                 navigationHOffset: 0,
                 navigationVOffset: -80,
             });


            //########################################
            //	-	API HANDLING	-
            //########################################

            // listen for slide change event

            revapi.bind("revolution.slide.onchange", function (e, data) {
                jQuery('#callbackinfo').html('Last Event: Slide Changed to ' + data.slideIndex).addClass("changecolor");
                setTimeout(function () {
                    jQuery('#callbackinfo').removeClass("changecolor");
                }, 500)
            });

            revapi.bind("revolution.slide.onpause", function (e, data) {
                jQuery('#callbackinfo').html('Last Event: Timer Pause ').addClass("changecolor");
                setTimeout(function () {
                    jQuery('#callbackinfo').removeClass("changecolor");
                }, 500)
                ;
            });

            revapi.bind("revolution.slide.onresume", function (e, data) {
                jQuery('#callbackinfo').html('Last Event: Timer Resume ').addClass("changecolor");
                setTimeout(function () {
                    jQuery('#callbackinfo').removeClass("changecolor");
                }, 500)
                ;
            });

            revapi.bind("revolution.slide.onvideoplay", function (e, data) {
                jQuery('#videoinfo').html('YouTube / Vimeo Video Playing ').addClass("changecolor");
                setTimeout(function () {
                    jQuery('#videoinfo').removeClass("changecolor");
                }, 500);
            });

            revapi.bind("revolution.slide.onvideostop", function (e, data) {
                jQuery('#videoinfo').html('YouTube / Vimeo Video Stopped').addClass("changecolor");
                setTimeout(function () {
                    jQuery('#videoinfo').removeClass("changecolor");
                }, 500);
            });

            revapi.bind("revolution.slide.onstop", function (e, data) {
                jQuery('#stopinfo').html('Slider Stopped ').addClass("changecolor");
                setTimeout(function () {
                    jQuery('#stopinfo').removeClass("changecolor");
                }, 500);
            });

            revapi.bind("revolution.slide.onbeforeswap", function (e) {
                jQuery('#otherevents').html('Slider going to swap').addClass("changecolor");
                setTimeout(function () {
                    jQuery('#otherevents').removeClass("changecolor");
                }, 500);
            });

            //revapi.bind("revolution.slide.onafterswap", function (e) {
            //    jQuery('#otherevents').html('Slider is ready with swap').addClass("changecolor");
            //    setTimeout(function () {
            //        jQuery('#otherevents').removeClass("changecolor");
            //    }, 500);
            //});

            //revapi.bind("revolution.slide.onloaded", function (e) {
            //    jQuery('#loadevent').html('Slider is Loaded').addClass("changecolor");
            //    alert("Slider loaded")
            //    setTimeout(function () {
            //        jQuery('#loadevent').removeClass("changecolor");
            //    }, 500);
            //});

            // bind to button click
            jQuery("input").click(apiHandler)

            function apiHandler(e) {
                switch (e.currentTarget.id) {
                    case "pause":
                        revapi.revpause();
                        break;
                    case "resume":
                        revapi.revresume()
                        break;
                    case "prev":
                        revapi.revprev()
                        break;
                    case "next":
                        revapi.revnext()
                        break;
                    case "showStart1":
                        revapi.revshowslide(1);
                        break;
                    case "showStart2":
                        revapi.revshowslide(1);
                        break;
                    case "showStart3":
                        revapi.revshowslide(1);
                        break;
                    case "showStart4":
                        revapi.revshowslide(1);
                        break;
                    case "showStart5":
                        revapi.revshowslide(1);
                        break;
                    case "bullet1":
                        revapi.revshowslide(2);
                        break;
                    case "bullet2":
                        revapi.revshowslide(3);
                        break;
                    case "bullet3":
                        revapi.revshowslide(4);
                        break;
                    case "bullet4":
                        revapi.revshowslide(5);
                        break;
                    case "bullet5":
                        revapi.revshowslide(6);
                        break;
                    case "bullet6":
                        revapi.revshowslide(7);
                        break;
                    case "bullet7":
                        revapi.revshowslide(8);
                        break;
                    case "bullet8":
                        revapi.revshowslide(9);
                        break;
                    case "bullet9":
                        revapi.revshowslide(10);
                        break;
                    case "bullet10":
                        revapi.revshowslide(11);
                        break;
                    case "bullet11":
                        revapi.revshowslide(12);
                        break;
                    case "length":
                        alert(revapi.revmaxslide());
                        break;
                    case "current":
                        alert(revapi.revcurrentslide());
                        break;
                    case "lastslide":
                        alert(revapi.revlastslide());
                        break;
                    case "redraw":
                        revapi.revredraw();
                        break;
                }
                return false;
            }

            jQuery("div").click(apiHandler)

            function apiHandler(e) {
                switch (e.currentTarget.id) {
                    case "pause":
                        revapi.revpause();
                        break;
                    case "resume":
                        revapi.revresume()
                        break;
                    case "prev":
                        revapi.revprev()
                        break;
                    case "next":
                        revapi.revnext()
                        break;
                    case "showStart1":
                        revapi.revshowslide(1);
                        break;
                    case "showStart2":
                        revapi.revshowslide(1);
                        break;
                    case "showStart3":
                        revapi.revshowslide(1);
                        break;
                    case "showStart4":
                        revapi.revshowslide(1);
                        break;
                    case "showStart5":
                        revapi.revshowslide(1);
                        break;
                    case "bullet1":
                        revapi.revshowslide(2);
                        break;
                    case "bullet2":
                        revapi.revshowslide(3);
                        break;
                    case "bullet3":
                        revapi.revshowslide(4);
                        break;
                    case "bullet4":
                        revapi.revshowslide(5);
                        break;
                    case "bullet5":
                        revapi.revshowslide(6);
                        break;
                    case "bullet6":
                        revapi.revshowslide(7);
                        break;
                    case "bullet7":
                        revapi.revshowslide(8);
                        break;
                    case "bullet8":
                        revapi.revshowslide(9);
                        break;
                    case "bullet9":
                        revapi.revshowslide(10);
                        break;
                    case "bullet10":
                        revapi.revshowslide(11);
                        break;
                    case "bullet11":
                        revapi.revshowslide(12);
                        break;
                    case "length":
                        alert(revapi.revmaxslide());
                        break;
                    case "current":
                        alert(revapi.revcurrentslide());
                        break;
                    case "lastslide":
                        alert(revapi.revlastslide());
                        break;
                    case "redraw":
                        revapi.revredraw();
                        break;
                    case "btnGoToMission1":
                        return true;
                        break;

                }
                //return false;
            }

        });	//ready

    </script>
</asp:Content>

