﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using System.Drawing;
using System.Net.Mail;

public partial class Accelerate_Home : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsAccountUserTrackings clsAccountUserTracking;
    clsGroups clsGroup;

    string cacheGroup = "";
    string cacheGroupPhase1 = "";
    string cacheGroupPhase2 = "";
    string cacheGroupPhase3 = "";

    DataTable dtPhase1Missions;
    DataTable dtPhase2Missions;
    DataTable dtPhase3Missions;
    int iNextMissionID = 0;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all sesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        //try
        //{
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
            clsAccountUserTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);

            if (clsAccountUsers.bHasCompletedProfileMission == false)
                Response.Redirect("Accelerate-Welcome.aspx");

            //### Caching Group
            cacheGroup = "cacheGroup" + clsAccountUsers.iGroupID.ToString();
            if (Cache[cacheGroup] == null)
            {
                clsGroup = new clsGroups(clsAccountUsers.iGroupID);
                Cache.Add(cacheGroup, clsGroup, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(2, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
            }
            else
            {
                clsGroup = (clsGroups)Cache[cacheGroup];
            }

            //## Caching GroupPhases
            cacheGroupPhase1 = cacheGroup + "Phase1";
            cacheGroupPhase2 = cacheGroup + "Phase2";
            cacheGroupPhase3 = cacheGroup + "Phase3";

            getInitialUserData();
        //}
        //catch(Exception ex)
        //{
        //    Response.Redirect("Accelerate-Error404.aspx");
        //}
        if (!IsPostBack)
        {
            try
            {
                popGroupMembers();
                checkCompletedPhasesUserTracking();
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
        }

        if (clsAccountUserTracking.bIsPhase3Complete == true)
        {
            StringBuilder strbMissionsComplete = new StringBuilder();
            strbMissionsComplete.AppendLine("<div class='missionContainer'>");
            strbMissionsComplete.AppendLine("<h4 class='missionHeading black'><strong>MISSIONS COMPLETE</strong></h4>");
            strbMissionsComplete.AppendLine("<p class='currentMissionText'>Congratulations! All missions have been completed.</p>");
            strbMissionsComplete.AppendLine("</div>");

            litCurrentMission.Text = strbMissionsComplete.ToString();
        }
        else
        {
            try
            {
                if (iNextMissionID != 0)
                    popCurrentMissionInfo(iNextMissionID);
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
        }

        litPoints.Text = clsAccountUsers.iCurrentPoints.ToString();
    }

    protected void btnPhase1_Click(object sender, EventArgs e)
    {
        try
        {
            popUserPhaseAchievements(1);
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    protected void btnPhase2_Click(object sender, EventArgs e)
    {
        try
        {
            popUserPhaseAchievements(2);
        }
        catch (Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    protected void btnPhase3_Click(object sender, EventArgs e)
    {
        try
        {
            popUserPhaseAchievements(3);
        }
        catch (Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    //### Getting initial data for all miisions in all phases to be cached.
    protected void getInitialUserData()
    {
        if (Cache[cacheGroupPhase1] == null)
        {
            dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTracking.iPhase1ID, "");
            Cache.Add(cacheGroupPhase1, dtPhase1Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(2, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase1Missions = (DataTable)Cache[cacheGroupPhase1];
        }

        if (Cache[cacheGroupPhase2] == null)
        {
            dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTracking.iPhase2ID, "");
            Cache.Add(cacheGroupPhase2, dtPhase2Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(2, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase2Missions = (DataTable)Cache[cacheGroupPhase2];
        }

        if (Cache[cacheGroupPhase3] == null)
        {
            dtPhase3Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTracking.iPhase3ID, "");
            Cache.Add(cacheGroupPhase3, dtPhase3Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(2, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase3Missions = (DataTable)Cache[cacheGroupPhase3];
        }
    }

    //###
    protected void checkCompletedPhasesUserTracking()
    {
        clsPhases clsPhase1 = new clsPhases(clsAccountUserTracking.iPhase1ID);
        litPhase1Text.Text = clsPhase1.strTitle;
        clsPhases clsPhase2 = new clsPhases(clsAccountUserTracking.iPhase2ID);
        litPhase2Text.Text = clsPhase2.strTitle;
        clsPhases clsPhase3 = new clsPhases(clsAccountUserTracking.iPhase3ID);
        litPhase3Text.Text = clsPhase3.strTitle;

        int iCurrentPhaseLevel = clsAccountUserTracking.iCurrentPhaseLevel;

        if (iCurrentPhaseLevel == 1)
        {
            popUserPhaseAchievements(1);
            btnPhase2.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
        }
        else if (iCurrentPhaseLevel == 2)
        {
            btnPhase2.Enabled = true;
            btnPhase2.Text = "<div class='showcaseSection Blue2'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
            popUserPhaseAchievements(2);
        }
        else if (iCurrentPhaseLevel == 3)
        {
            btnPhase2.Enabled = true;
            btnPhase3.Enabled = true;
            btnPhase2.Text = "<div class='showcaseSection Blue2'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Blue3'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
            popUserPhaseAchievements(3);
        }
    }

    #endregion

    #region POPULATION METHODS

    //### popAchievementsByPhase OPTIMISE
    protected void popUserPhaseAchievements(int iPhaseLevel)
    {
        DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsRecords(clsAccountUsers.iAccountUserID);

        DataTable dtUserDisplayAchievements = new DataTable();
        dtUserDisplayAchievements.Clear();
        dtUserDisplayAchievements.Columns.Add("iMissionID");
        dtUserDisplayAchievements.Columns.Add("strLink");
        dtUserDisplayAchievements.Columns.Add("strTitle");
        dtUserDisplayAchievements.Columns.Add("strNewRow");
        dtUserDisplayAchievements.Columns.Add("FullPathForImage");

        List<int> lstAchievements = new List<int>();
        List<int> lstMissions = new List<int>();
        //T
        List<int> lstNextMission = new List<int>();

        int iCount = 1;
        int iAchieveTotalCount = 0;

        if (iPhaseLevel == 1)
        {
            foreach (DataRow dr in dtPhase1Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }
            //T
            lstNextMission = lstMissions;

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }
        else if (iPhaseLevel == 2)
        {
            foreach (DataRow dr in dtPhase2Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }
        else if (iPhaseLevel == 3)
        {
            foreach (DataRow dr in dtPhase3Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }

        rpAchievements.DataSource = dtUserDisplayAchievements;
        rpAchievements.DataBind();
    }

    /// <summary>
    /// POP Mission Questions DYNAMICALLY
    /// </summary>
    /// <param name="iCurrentMissionID"></param>
    ///
    protected void popCurrentMissionInfo(int iCurrentMissionID)
    {
        string strCurrentMission;
        clsMissions clsMissions = new clsMissions(iCurrentMissionID);
        StringBuilder strbCurrentMission = new StringBuilder();
        clsAchievements currentAchievements = new clsAchievements(clsMissions.iAchievementID);

        string strFullPathForMissionImage = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);

        strbCurrentMission.AppendLine("<div class='well-short' style='background: #80c342; padding-bottom: 12px;'>");
        strbCurrentMission.AppendLine("<div class='' style='color: #000; font-weight:bold; font-size:13px;'>" + clsMissions.strTitle + "</div>");
        strbCurrentMission.AppendLine("<br />");
        strbCurrentMission.AppendLine("<div class='row-fluid'>");
        strbCurrentMission.AppendLine("<div class='span1'><center>");
        strbCurrentMission.AppendLine("<img onerror=\"this.src='images/imgAchievementEmpty.png'\" src='" + strFullPathForMissionImage + "' align='center' />");
        strbCurrentMission.AppendLine("</center></div>");
        strbCurrentMission.AppendLine("<div class='span9' style='font-size: 12px; color: #000;'>");
        strbCurrentMission.AppendLine(clsMissions.strDescription);
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("<div class='span2' style='padding-left:15px;'>");
        strbCurrentMission.AppendLine("<span style='font-size: 3em; color: #FFF; line-height: 1em; font-weight: 100;'>" + clsMissions.iBonusPoints + "</span><br />");
        strbCurrentMission.AppendLine("<span style='font-size: 1.5em; color: #FFF; line-height: 1em; font-weight: 600;'>MISSION</span><br />");
        strbCurrentMission.AppendLine("<span style='font-size: 1.5em; color: #FFF; line-height: 1em; font-weight: 600;'>POINTS</span><br />");
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("<br/ ><br/ ><a href='Accelerate-Current-Mission.aspx?iMissionID=" + clsMissions.iMissionID + "' class='Green3 btn'>Start Mission</a>");

        strCurrentMission = strbCurrentMission.ToString();
        litCurrentMission.Text = strCurrentMission;
    }

    protected void popGroupMembers()
    {
        string strFirstName;
        int iAccountMemberID;
        int iCount = 0;
        int iLevelID = 0;
        string strAccountMemberMain = "";
        string strPathToImage = "";
        StringBuilder sbAccountMember = new StringBuilder();

        if (clsGroup.iLevelID != 0)
        {
            iLevelID = clsGroup.iLevelID;
        }

        DataTable dtGroupList = clsGroups.GetGroupsList("iLevelID =" + iLevelID, "");
        foreach (DataRow dtrGroup in dtGroupList.Rows)
        {
            DataTable dtAccountMemberMain = clsAccountUsers.GetAccountUsersList("iGroupID=" + Convert.ToInt32(dtrGroup["iGroupID"]).ToString(), "strFirstName ASC");
            dtAccountMemberMain.Columns.Add("FullPathForImage");
            foreach (DataRow dtrAccountMember in dtAccountMemberMain.Rows)
            {
                ++iCount;
                iAccountMemberID = Convert.ToInt32(dtrAccountMember["iAccountUserID"].ToString());
                strFirstName = dtrAccountMember["strFirstName"].ToString();

                if ((dtrAccountMember["strMasterImage"].ToString() == "") || (dtrAccountMember["strMasterImage"] == null))
                {
                    sbAccountMember.AppendLine("<a style='display:inline-block; margin: 2px' href='Accelerate-User-Profile.aspx?iAccountUserID=" + iAccountMemberID + "'><img onerror=\"this.src='images/no-image.png'\" src='images/no-image.png'  title='" + strFirstName + "' alt='" + strFirstName + "' width='62px' height='62px' /></a>");
                }
                else
                {
                    strPathToImage = "AccountUsers/" + dtrAccountMember["strPathToImages"] + "/" + Path.GetFileNameWithoutExtension(dtrAccountMember["strMasterImage"].ToString()) + "" + Path.GetExtension(dtrAccountMember["strMasterImage"].ToString());
                    sbAccountMember.AppendLine("<a style='display:inline-block; margin: 2px' href='Accelerate-User-Profile.aspx?iAccountUserID=" + iAccountMemberID + "'><img onerror=\"this.src='images/no-image.png'\" src='" + strPathToImage + "'  title='" + strFirstName + "' alt='" + strFirstName + "' width='62px' height='62px' /></a>");
                }
            }
        }
        strAccountMemberMain = sbAccountMember.ToString();
        litGroupMemberMain.Text = strAccountMemberMain.ToString();
    }

    #endregion
}